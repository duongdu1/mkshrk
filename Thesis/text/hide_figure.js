#!/usr/bin/env node

const fs = require('fs')
const [ ,, mode, file ] = process.argv

if((mode != 'hide' && mode != 'show') || !file){
    console.log(`usage: hide_figure <hide|show> <file>`)
    process.exit(1)
}

let body = fs.readFileSync(file, 'utf8')

if(mode == 'hide')
    body = body.replace(
        /(\\begin{figure}[\s\S\n]*?\\end{figure})|(^\\missingfigure.*?$)/gm,
        passage => passage.split('\n').map(x => `%HIDE_FIGURE_${x}`).join('\n')
    )
else if(mode == 'show')
    body = body.replace(/%HIDE_FIGURE_/g, '')

fs.writeFileSync(file, body)

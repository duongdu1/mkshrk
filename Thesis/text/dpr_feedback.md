# DPR Feedback

Práce je psána čtivě, text je pro čtenáře srozumitelný. K textu mám následující podněty:

### Poděkování
- je nepsaným zvykem poděkovat vedoucímu práce, pak až můžete děkovat komukoliv

### Abstrakt
- formulace – druhá věta v čj je nestandardně koncipovaná, spíše tedy... V teoretické části práce byly popsány principy a historie pos-apokalistického žánru a dále byly analyzovány technologie využité následně v praktické část práce. (publikace vynechte, ty budete používat v teoretické části práce)
- dále místo Výstupem práce je... V práci byla vytvořena počítačová hra...
- dále možná místo ... uživatelů, kteří si hru vyzkoušeli... kteří hru testovali...

### Kapitola 1 (- 2 body)
- obrázky – za jejich popisy neuvádějte tečky
- str. 4 – pokud vkládáte pojem do uvozovek, bude do nich těsně přiléhat, mezi pojmem a uvozovkami není mezera
- kapitola 1.2.1 – pokud máte v kapitole podkapitoly, je nutné je očíslovat (zvažte ale, zda má cenu vytvářet podkapitolu kvůli jednomu či dvěma odstavcům)
- celá tato kapitola je čerpaná z literatury, ale odkazy do literatury zde nejsou, doplňte

###  Kapitola 2 (- 1 bod)
- mezi citovaný text a odkazy do literatury vkládejte mezeru
- str. 8 – váš dotaz – video z YouTube citovat můžete, pokud není jiný primárnější zdroj
- str. 10 – pokud chcete uvést přímou citaci, nevkládejte do ní výpustku, pak už nejde o přímou citaci

### Kapitola 3
- obrázky – opět odstraňte tečky za popisky obrázků
- opět vkládejte mezi citovaný text a odkaz mezeru
- tato kapitola je na kapitolu prvního řádu poměrně krátká, je to analytická část, stejně jako kapitola 4, proto doporučuji analytické části sloučit do jedné kapitoly

### Kapitola 4
- nekombinujte psaní formou autorského singuláru a neosobní způsob vyjadřování
- kapitola 4.1.1 – opět chybějící číslování podkapitol (očíslujte či neuvádějte jako samostatné podkapitoly)
- opět mezi citovaný text a odkazy vložte mezeru

### Kapitola 5
- opět mezi citovaný text a odkazy vložte mezeru
- figure 5.1 – není to obrázek, ale tabulka, obrázky a tabulky číslujeme a popisujeme zvlášť + tabulky popisujte nad tabulky

### Kapitola 6
- opět nekombinujte psaní formou autorského singuláru a neosobní způsob vyjadřování

### Kapitola 7
- na kapitolu 1. řádu příliš krátká

### Kapitol 8
- kapitola 8.1.1 – do přímé citace nevkládejte trojtečku

### Závěr
- nejpve zopakujte cíle práce, pak až jak jste je splnil

### Literatura (- 1 bod)
- zdroj 1 – v názvu zdroje máte místo pomlčky spojovník
- zdro 4, 29 – místo zkratky n.d. vložte datum vzniku či aktualizace
- zdroj 5 – uveďte dle ČSN ISO 690
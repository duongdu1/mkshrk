\chapter{Creation of Assets}
\label{chapter:asset-creation}
\begin{chapterabstract}
In this chapter, the process that goes into creating assets in Blender is going to be described, alongside their subsequent export and import to Unity. Most of the work remains consistent for all the models, so it will be only described once on a selected asset, and the ones where the process differs will be further elaborated on. Some assets have been obtained from third parties, so their sources will be documented here.
\end{chapterabstract}
% ------------------------------------------------------------------------------------------------ %

The assets inside this thesis are both hand-made by the author or borrowed from PolyHaven \cite{polyhaven}, which are licensed under the CC0 license. The models that the author made follow the same workflow, with minor adjustments to accommodate the specific needs of some models. The general workflow starts from modeling, texturing, and baking, all done in Blender and then prepared to be imported to Unity. Models borrowed from third parties occasionally required additional attention for them to be brought over to Unity, described in Section~\ref{section:third-party-models}.

The modeling process is the process of creating the shape of the model and usually starts with obtaining references. Some models (e.g., water bottles) used references from sites such as \href{https://www.dimensions.com/}{Dimensions.com}, which contains the dimensions and orthographic views of the subject. A handful of models were designed by the author just by their visual look (e.g., the cabin). After that, \hlcyan{the process continues by creating the model with the box modeling technique}, which involves building the complex model by composing and modifying primitives (such as cubes and planes). This method is universal for all models; however, for models that have less of a rigid shape\footnote{An example of a non-rigid shape could be a plastic bag.} an additional method called sculpting can be employed, which allows the artist to add more details to the mesh itself as if they were drawing on it and not by manually moving each of the faces, edges, or vertices around. However, this technique often results in meshes with very high polygon counts, which are unsuitable for video games and, hence, are later reduced during the baking process.

Texturing gives the model its visual look and also describes its physical material. Within the thesis, most of the textures are sourced from CC0-licensed asset libraries such as PolyHaven \cite{polyhaven}, AmbientCG \cite{ambientcg}, and Textures.com \cite{texturescom}. In Blender, one can make procedurally generated textures using Shader Nodes, which have been used in this thesis for various purposes, such as mixing two materials based on a mask or a given criteria, or adding scratch marks and wear and tear look to the models. These procedural textures are not easily transferable to different programs, meaning they must be baked into a regular texture that can be used anywhere.

Models and textures are baked to precompute details into more optimized meshes and textures. This is done to increase the rendering performance at the possible cost of reducing the visual fidelity. The loss of quality can be useful for objects that are barely visible to the player, such as faraway objects. Baking of textures usually means creating different texture maps that describe the model's color, metalness, roughness, and normals. While baking the normals, one can also bake a model using a simplified model with a lower polygon count than the original but roughly looking resembling the original shape. By doing so, the baked normals will make the simplified model shade similarly to the original model with a smaller performance impact.

Exporting is the process of converting the model from Blender to be usable in Unity. This process consists of saving and converting the model to the FBX format\footnote{Proprietary format by Autodesk for storing 3D models and animations.} in Blender, packing the textures, and then importing them into Unity. Additional precautions must be taken while exporting a model from Blender; otherwise, once the model is imported into Unity, it can have unexpected rotation and scaling issues. The rotation can be partially remedied using Unity's option to ``Bake Axis Conversion''; however, that will not solve the object's scale always being multiplied by $100$ to be correct. During the exporting process, an experimental option of ``Apply Transform'' is used; however, due to the limitation of the Blender's built-in FBX exporter, all models can be nested utmost to a depth of two\footnote{This limitation was already reported to the Blender's issue tracking site, but was closed due to it being an experimental feature \cite{blenderBugFBX1}.}.

Texture packing or channel packing is the process of storing multiple textures in an image's individual RGBA\footnote{An acronym for Red, Green, Blue, Alpha} channels. The textures that are being packed have to all share the exact image dimensions and describe a single numerical value per pixel. In Unity's URP and HDRP default shaders, textures usually expect what the author has dubbed the ``MADS'' arrangement \cite{litShaderURP}, which correlates the channels to the maps as described in Figure~\ref{fig:channel-mapping}, with a practical example shown in Figure~\ref{fig:texture-packing}. Packing is done to reduce the memory bandwidth.

\hlcyan{Some textures for objects such as magazines and scattered papers were created by capturing local newspapers and newsletters using a camera. The organizations featured in the papers are not affiliated with this thesis and are solely used for convenience.}

\begin{table}[ht]
  \centering

  \caption{Texture packing arrangement used in this thesis. The default column describes what the channel can be replaced with in case the assigned map is not available.}
  \label{fig:channel-mapping}

  \begin{tabular}{|c|c|c|}
    \hline
    \textbf{Channel} & \textbf{Assigned map} & \textbf{Default} \\
    \hline
    \hline
    Red & Metalness & Black \\ \hline
    Blue & Ambient Occlusion & White \\ \hline
    Green & Detail Map & Black \\ \hline
    Alpha & Smoothness\footnote{In some workflows this property is called ``Roughness'' and is calculated as $1-\text{roughness}$} & White \\ \hline
    \hline
  \end{tabular}
\end{table}

\begin{figure}[ht]
  \centering
  \includegraphics[width=.7\textwidth]{assets/packed-textures.pdf}
  \caption{Different texture maps being combined into a single texture.}
  \label{fig:texture-packing}
\end{figure}

\section{Hands}

The model of the hands was inspired by the gardening gloves the author had in their family's garden. According to \hlcyan{the author}, it is quite suitable for a post-apocalypse situation, given that these gloves can be obtained quite easily from gardens or sheds and are durable enough to protect the hand from sharp objects that they may wish to pick up.

These hands also serve as the player's avatar, similarly to how Half-Life:~Alyx portrays the player as described in Section~\ref{section:hl-alyx}. This has been done to avoid disorienting the player with limbs that most devices are not capable of tracking, such as arms and feet. While it is possible to extrapolate the tracking data available from hands and apply it to arms, it will not cover all possible positions in which the arms and hands can be. It will often cause a mismatch between virtual and real hands, as demonstrated in Figure~\ref{fig:ik-mismatch}.

\begin{figure}
  \centering
  \includegraphics[width=.4\textwidth]{assets/ik-issue1.png}
  \includegraphics[width=.4\textwidth]{assets/ik-issue2.png}
  \caption{Mismatch between virtual and real arms, with the only tracking data used being the headset's and controller's position and rotation. The controller's position and rotation remain unchanged between the poses in the pictures. The male model obtained from Blender Studio Sculpt Base library \cite{blender-studio-base}, and the controllers from Meta Quest Developer Center \cite{quest-models}.}
  \label{fig:ik-mismatch}
\end{figure}

The creation process of the hands differs from the other models by the fact it is one of the only organic models explicitly created for this thesis. Instead of box modeling the hand, an existing model from the Blender Sculpt Base library \cite{blender-studio-base} is used. These models, as the name suggests, are designed for sculpting purposes, and their topology is not optimal for this use case since it is expected for the hand's fingers to bend. To remedy this, a process called ``retopology'' is used.

Retopology is the process of modifying the topology of an existing model while retaining its general shape. This can be done for reasons such as easier manipulation of the model itself \cite{blender-retopo}, adjusting the polygon count to be lower or higher, or, in this case, to ensure the model bends correctly. To do this, the Shrinkwrap modifier in Blender is used. It allows for one model to be cast onto a different one, which is used here to create a new model with proper topology, and by casting it onto the original model, it copies its shape. The result of the retopology can be seen in Figure~\ref{fig:hand-retopology}

\begin{figure}[h]
  \centering
  \includegraphics[width=.7\textwidth]{assets/hand-topology.png}
  \caption{The original hand model from Blender Sculpt Bases on the left and the retopologized version used in the thesis on the right. The most notable change is the increased density of polygons around the joints on the retopologized version.}
  \label{fig:hand-retopology}
\end{figure}

As the hand is required to be animated by the controller's input, it needs to be skinned and weight-painted. To create the bone structure, a Rigify\footnote{An addon shipped with Blender that allows simple creation of many different rigs.} human rig was used. Bones for its limbs, such as legs and arms, were not needed and were removed, so only the hand and finger bones were kept. These provided rigs need to have their bones' position and rotation adjusted to match the model at hand, where the model's topology has aided as it was possible to easily determine the center section of a finger. The automatic weight method from Blender has been used to add weights to the vertices of the model. However, as it did not provide satisfactory results when the thumb was bending, making the hand's thenar bend inwards, a manual adjustment of these areas was necessary.

\pagebreak
Importing the model into Unity follows the same procedures outlined above. An additional script was required to drive the rotation of the finger joints in harmony with the player's input. The script operates in two modes: either it can curl all of the fingers equally or drive each finger using a single value provided by the controller. The value itself is limited from $0.0$ to $1.0$ inclusive, and they are multiplied against the amount of rotation each of the finger joints receives. Even though these rotations should be up to $90$ degrees on the given axis, they have to be ``calibrated'' by trying them out in-game with different parameters and manually determining what is correct. The way the finger curls in relation to the driving value is simplified in 2D in Figure~\ref{fig:finger-curl}.



\begin{figure}[ht]
  \centering
  \begin{tabular}{ccc}
    \includegraphics[width=.4\textwidth]{assets/finger-0.png} & \includegraphics[width=.2\textwidth]{assets/finger-50.png} & \includegraphics[width=.2\textwidth]{assets/finger-100.png} \\
    $\begin{aligned}
      \text{curl} &= 0.0 \\
      \alpha &= 180^\circ
    \end{aligned}$ & $\begin{aligned}
      \text{curl} &= 0.5 \\
      \alpha &= 135^\circ
    \end{aligned}$ & $\begin{aligned}
      \text{curl} &= 1.0 \\
      \alpha &= 90^\circ
    \end{aligned}$

    
    % $\text{curl} = 0.0$  & $\text{curl} = 0.5$  & $\text{curl} = 1.0$ \\
    % $\alpha = 180^\circ$ & $\alpha = 135^\circ$ & $\alpha = 90^\circ$ \\
  \end{tabular}
  \caption{Simplified model of a finger's skeleton and how it curls given a factor. The angles of the joints (indicated by the pink overlay) equate to $\alpha = 180^\circ - \text{curl} \cdot 90^\circ$, where $\text{curl}$ indicates the factor of how curled the finger should be.}
  \label{fig:finger-curl}
\end{figure}

As it has been established in Section~\ref{section:openxr}, controllers following the OpenXR standard have three interaction zones. The thumb is curled when the buttons or touch is detected on top of the controller. The index finger is curled in accordance with how deep the trigger is held down. The grip curls the middle finger, ring finger, and little finger in a similar fashion as the trigger does.

\section{Third party models}\label{section:third-party-models}

Many of the assets in this thesis are borrowed from the public CC0 licensed asset library called PolyHaven \cite{polyhaven}, which includes textures, skies, and models. These models are often designed to be used in animations and still renders, not necessarily for real-time purposes such as games, meaning that the models can have a considerably high polygon count. Two methods were utilized in this thesis for the polygon count reduction: manual labor and using a Decimate modifier. 

To perform the manual labor, the model needs to have a suitable topology first, from which it is possible to remove edge loops around places where the player would not see and notice the more crudely defined curves. An important note is that removing loops is only possible if they are not acting as a seam to the UV map. If it is, the UV map must be edited or the entire texture rerendered. This method, while providing theoretically the best results in terms of similarity to the original model and flexibility, allowing the artist to redistribute the quality density based on its importance, is very time-consuming. Therefore, the second method was used more often.

Using the Decimate modifier in Blender allows the model to reduce the poly count quickly, however, with little to no attention to its topological structure. The modifier itself has three modes it can work in:

\begin{description}
  \item[Collapse:] ``Merges vertices together progressively, taking the shape of the mesh into account.'' \cite{blender-decimate} Using this method often results in stretched and invalid UV maps. It decimates to approximately $triangleCount \cdot ratio$, where $ratio$ is specified by the user.
  \item[Un-Subdivide:] Reverses the Subsurface Subdivision modifier, basically attempts to find a grid of quads\footnote{Polygons made out of four vertices} and merge it into one. It requires the model to be subdivided and unmodified after that, which was not applicable in most cases in this thesis, given that their models usually come unsubdivided with the presumption that the software would do it for the user.
  \item[Planar:] Attempts to merge faces under a certain angle threshold, making them flat. This method also allows the user to limit which faces are to be considered based on their UV seams, making it possible to keep the map intact.
\end{description}

Given the advantages of the Planar method, allowing the existing UV map to be kept, it was used extensively in this thesis. There are, however, some models that the player would be able to inspect up close, so the details were kept intact using an LOD\footnote{Level of Detail} approach. This entails keeping multiple versions of the model in varying poly-count and quality, rendering only one of them depending on how much of it is visible on screen, where distance is often the most significant contributing factor. Doing so allows the most detailed models to be seen up close while displaying a lower quality version from afar that is visually indistinguishable due to the size it is rendered on screen.
\chapter{Terminology}
\begin{chapterabstract}
  \hlcyan{This chapter briefly explains the terminologies used in this thesis, including the basic concepts of 3D graphics and rendering. The individual properties of materials in physically based rendering systems used in this thesis and the concept of virtual reality are also explained.}
\end{chapterabstract}

\section{Basic concepts of 3D}
\label{section:basic-3d}

The most fundamental concept in 3D graphics is an ordered set of three dimensions. This set is often called a ``vector'' and can carry different meanings depending on the context (e.g., position, scale, direction, \dots). The individual values in the set are often referred to by the letters $x$, $y$, and $z$, respectively. A point in a three-dimensional world would be specified using a single vector, determining its position or, in other words, its offset from the world's zero point\footnote{Also known as the world origin.} \cite{3dgamedesign}. Points that are connected to each other are called polygons, and three connected points would then form a triangle, which is the most primitive shape that is visible to the human in computer graphics, with most graphics hardware being optimized for handling triangles. While it is possible to form a polygon using more points, it can have different representations based on how the application draws the given polygon \cite{realtimerendering}. Figure~\ref{fig:quad-undeterministic} shows an example of this behavior. The way the vertices are connected with each other forms a topology, which may not be as important for static models but is crucial for models that are supposed to be bent or deformed (e.g., body parts).

\begin{figure}[ht]
  \centering
  \includegraphics[width=.75\textwidth]{assets/same_set_of_points.png}
  \caption{Same set of four points forming a polygon; however, each calculated in a different order. A light source is added to help demonstrate the shape difference.}
  \label{fig:quad-undeterministic}
\end{figure}

Models are built from a mesh, materials, and textures \cite{unityDocs-models}. Meshes are made of multiple polygons, most often triangles, connected to each other to convey a more complex shape. Materials determine the appearance of either parts or the entire model and can utilize textures as a data source. Textures are then applied to the mesh accordingly with the materials, conveying its visual appearance in terms of color or any additional detail that would not be computationally feasible using polygons \cite{3dgamedesign}. Applying a texture, usually a 2D image, onto a 3D mesh is often done using UV maps\footnote{Similar to how 3D vectors use the letters $x$, $y$, $z$, UV maps use the letters $u$ and $v$. In some applications, they are referred to as $s$ and $t$ coordinates.}, which specify the coordinates of the individual points of a polygon on a 2D plane. They are also commonly referred to as texture coordinates \cite{3dgamedesign}. The process of creating these UV maps is called ``UV unwrapping''. To simplify the thought process, one can think of it as deconstructing a papercraft model, going from the final model and unwrapping it onto a paper. For the 3D software to know how to unwrap the model, edges have to be marked as ``seams'', which in papercraft terminology would be where it would need to be cut to flatten it with the least distortion or stretching \cite{blender-unwrap, blender-seams}.

\pagebreak
A three-dimensional world is often called a Scene, which is described using objects. Each of these objects holds information about their position, rotation, and scale and can be either nothing\footnote{Often used to group objects to transform them together or to serve as bones in rigs.}, models, or lights. Objects can be parented to another object, which would make changing the parent's transformation affect its children, but not the other way around. These relationships are often described in scene graphs. \cite{realtimerendering}

\begin{figure}[ht]
  \centering
  \begin{minipage}[tc]{.4\textwidth}
    \dirtree{%
      .1 SceneRoot.
      .2 barrel\_03.
      .2 small\_wooden\_table\_01.
      .3 food\_apple\_01.
      .3 marble\_bust\_02.
    }
  \end{minipage}
  \begin{minipage}[tc]{.4\textwidth}
    \includegraphics[width=\textwidth]{assets/scene-graph.png}
  \end{minipage}

  \caption{Example of a simple scene graph. Moving the table would affect the apple and the bust; however, moving the barrel would not change any other object in the scene.}
\end{figure}

For a model to be set in motion, one can update the transformation values in the scene graph over time. Doing so would only allow the model to be moved, rotated, or scaled with no deformation to its shape as if it were rigid. For cases where we wish to deform the model in a hierarchical way, the model needs to have bones and a numerical value called ``weight'' for its vertices. This method is often referred to as ``skinning,'' with the process of adding weights called ``weight painting'' \cite{blender-weight}. Bones can be considered standard empty objects that store the basic three transformations, and each vertex can have a weight value correlating to the said bone. The weight determines how much the vertex would change in relation to the change in the bone's transformation. \cite{3dgamedesign}
%This technique is usually used for organic models such as human bodies and their limbs.
%; however, it is also useful for mechanical models such as robotic hands, as a properly prepared rig allows one to define how the bones relate to each other. These models are often called ``skinned meshes'' since it can be thought of as the model being a skin case for the bone inside them.

\section{3D Rendering}

In computer graphics, scenes, models, and objects are typically stored as data, which can be difficult for humans to visualize. A visual representation of the scene is created by a process called rendering. In this thesis, two rendering techniques were used depending on the use case: rasterization and path tracing.
% Scenes, models, and objects in computer graphics are often stored as data, which are most likely hard to visualize for humans. To create a visual form of the scene is called rendering. 

\subsection{Rasterization}

Rasterization is a very fast type of rendering, usually done on a GPU using graphics API such as OpenGL, DirectX, Vulkan, or Metal. It is often used in game engines or workflows where real-time previews are necessary. While it is fast, it may not be physically accurate, as in most cases, they are approximating the lights' behavior. On input, the rasterizer receives the geometry assembled from primitives, such as the triangles mentioned above, and returns a fragment in a pixel grid. \cite{cgtutorial} % On input, the rasterizer receives the geometry and appropriate scene data, such as information about light sources in the scene, and returns a fragment in a pixel grid.

A fragment is a set of values of the geometry at a given position on the raster, meaning that it has no visual appearance. For that, special programs called \textit{pixel shaders}\footnote{Also known as Fragment Shaders in some APIs} are used to give the fragment a color, making it a pixel on the screen. \cite{realtimerendering}

When rendering opaque objects, they can be rendered in any order since each of the fragments usually holds a depth value, which is the distance of the geometry at that point to the camera. This method is called the ``Z-Buffer Algorithm'' since the depth values are often referred to as Z values and are stored in a buffer. If two fragments are drawn at the same pixel, the one closer to the camera wins and is displayed instead of the other one \cite{graphicsshader-theoryandpractice}. This behavior is referred to as ``over-draw'' \cite{realtimerendering} and can be modified, commonly to make an object visible even if it is occluded~\cite{unityDocs-ztest}. \linebreak In the case of transparent objects, however, they must be rendered after opaque objects and then in order from the outermost transparent object to the closest one \cite{3dgamedesign}. This approach is called the ``Painter's algorithm'', as it performs similarly to how an artist would paint with perspective in mind on a canvas.

Shading and lighting are usually also done using pixel shaders, and there are two approaches\footnote{Commonly referred to as rendering paths} to this called Forward and Deferred. Both of them are used in the industry; however, each of them has a set of advantages and disadvantages, both in terms of performance and abilities, and are usually chosen based on the use case or the scene \cite{unityDocs-renderpath}.
% \subsubsection*{Forward}

\textit{Forward} shading works by going through each of the fragments and lights in the scene to calculate the final appearance and can be done using a single pixel shader. This approach can be very computationally demanding in scenarios where many dynamic lights need to be calculated in a dense environment since computing time might have been spent on lighting operations on fragments later drawn over by a different fragment.

% \subsubsection*{Deferred}

\textit{Deferred} shading works by initially calculating various information about the scene into ``\textit{G-Buffers}''\footnote{Various buffers containing information of the rasterized scene.}, which are then used in a second stage, which performs the lighting calculation. In this case, all of the lighting that is being calculated is visible and does not go unused. The downside, however, is that the G-Buffers can be very large in terms of memory and are unable to render transparent objects, among other limitations. In Unity, for example, transparent objects are rendered using the Forward method after the Deferred method has finished \cite{realtimerendering,unityDocs-deferredShading}.

\pagebreak
\subsection{Path Tracing}

Path tracing attempts to render the scene by faithfully simulating the light rays in the scene as they are being emitted, reflected, and scattered to find their path from the camera to the light source \cite{pbrbook}. This method is very performance-demanding; however, the quality tradeoff for it is often worth it. It is often performed on the CPU; however, it has also recently been possible to use GPU for such tasks. Nowadays, it is even possible to render scenes in real-time with acceptable quality thanks to advancements in GPU technology.

This method is not used to display the final game in this thesis since it would be a tremendous task to have it work fast enough for virtual reality. It is, however, used to prepare textures for 3D models using a process called ``baking,'' which is the process of calculating time-consuming tasks (e.g., lighting) and storing its results in a texture. Doing so allows the models to be viewed with better lighting in real time.

\subsection{Physically Based Rendering}

Physically based rendering, in a sense, are guidelines that help the artist describe the materials in the scene and how they should interact with light and its surrounding environment \cite{adobe-pbr}. It is commonly used in computer graphics, both in video games and movie industries, to the point that it has become the de-facto standard in the industry \cite{pbr-guide}, allowing these materials to be exported from one software and imported to another with little to no additional work.
A material can be defined as a set of properties that dictate how the light interacts with the surface on which it is used. These properties can be encoded in either a numerical value, meaning that it applies to the entire material, or using a map, which would only affect specific areas of the material.
% A material can be defined as a set of properties of a certain dimension, where its complexity scales with the number of dimensions. The most complex ones even have the ability to dictate how the light should scatter within the material itself, its refraction and translucency properties, and such. These properties can be encoded in either a numerical value, meaning that it applies to the entire material, or using a map, which would only affect specific areas of the material.

Two workflows are commonly used: the \textit{specular workflow} and the \textit{metallic workflow}. The former is more flexible in terms of artistic freedom, allowing the artist to change the color of the specular lighting, which may lead to light interactions that would not be possible in real life \cite{adobe-pbr-guide-2}. The latter dictates the color of the specular lighting using a metallic map, which, as the name suggests, determines which portion of the material is made out of metals, which is more useful, as it will follow more faithfully how light behaves in the real world for most common materials. For the entirety of the thesis, the metallic workflow is used, as it is the default in both Unity and Blender \cite{litShaderURP,blender-principled-bsdf}.

For most materials in this thesis, only a subset of properties are used to describe them in a believable way:

\begin{description}
  \item[Albedo:] Describes the color of the material. In most cases, it should be a texture with no shadows and shading for the best results, as it is the renderer's job to provide them. Including shadows in the albedo can cause overlapping shadows or misdirected light, resulting in physically inaccurate renditions. \cite{pbr-guide}
  \item[Roughness:] Determines how rough the surface is in the material, which can make a material have a glossy or matte appearance. In some software, a value called ``Smoothness'' or ``Gloss'' is used instead, which is just the inverted value of roughness. \cite{pbr-poliigon}
  \item[Metallic:] Describes which parts of the material are made out of metal. Usually, the map or values should be set either to $1.0$ for metallic or $0.0$ for dielectric, with values hardly ever being anything in between. This is because a material described should have a clear-cut distinction whether it is metal or not. \cite{pbr-guide}
  \item[Normal Map:] It is a three-dimensional map representing the offset direction of the surface normal on which it is being projected \cite{pbr-poliigon}. This map allows additional details to be added that would have been hard to portray or expensive in terms of performance in a mesh using polygons \cite{unityDocs-normmap}.
  \item[Emission:] Determines which parts of the material emit light and with what color and intensity \cite{pbr-poliigon}. Since most materials do not emit light, most materials have this set to pure black, meaning it does not emit any light.
\end{description}

Many other maps can, for example, describe the material's transmission properties down to their IOR\footnote{Index of Refraction} values, or subsurface scattering maps that determine which colors of the incoming light are being scattered as it penetrates the surface; however, they are most commonly used in specific materials such as glasses and skin. \cite{blender-principled-bsdf} % These properties can be missing in game engines due to the computational power required to render them, which would not be feasible in real time.

These maps can be created procedurally using specialized software such as Adobe Substance Painter or Blender or can be approximated by utilizing the photogrammetry techniques on a material in real life. Procedurally generated materials are often specific to the given software or computationally expensive to calculate all the time and need to be baked into a texture for the maps to be used in other software. The process and workflows describing baking are further elaborated in Chapter~\ref{chapter:asset-creation}.

\section{Virtual Reality}

Virtual Reality (VR) is the concept of creating a world in a virtual space, which is often viewed using specialized equipment called virtual reality glasses or headsets. The most basic ones contain one or multiple screens, which are used to display the virtual world to the user, and a gyroscopic sensor, tracking the user's head rotation. These headsets are called 3DOF\footnote{Degrees of freedom} headsets since they allow the user to track their head's rotation only. The more advanced ones include more sensors (e.g., accelerometers or light sensors), which track the headset's position and are called 6DOF headsets. Most VR headsets nowadays include controllers, which can be either 3DOF or 6DOF, similar to the headsets themselves. They are used to interact with the virtual world in a greater capacity, usually to give the user virtual hands. \cite{vrbook}

% The author of this thesis uses a Meta\footnote{Formerly Oculus} Quest 2 headset, which is a 6DOF headset with two 6DOF controllers for each hand to develop the game.

The most common issue people have with VR headsets, especially newcomers to the technology, is the feeling of discomfort after using them, such as nausea and motion sickness \cite{vrbook}. \linebreak Motion sickness generally happens when the movement perceived by the player's eyes differs from what is actually happening around them. This mismatch is often caused by insufficient framerate, meaning that the eye would have jittery visuals or, at times, by design (e.g., fast-paced action games).
% This mismatch can be caused most often either by design (e.g., fast-paced games) or due to lower framerate, meaning that the eye would have jittery visuals.

An avatar is used to represent the user in the virtual space. In virtual reality, avatars are essential, as without them, users would have trouble orienting themselves in the virtual world. These avatars can be as simple as floating models of hands or controllers or as complex with full-fledged characters, which may be bipedal or even \hlcyan{quadruped} if one so desires, as there are no theoretical limits, only practical constraints to how one would be controlling the avatar. Thanks to the limitless potential, it is an excellent way for people to personalize themselves, be unique from others, and become who they wish to be.
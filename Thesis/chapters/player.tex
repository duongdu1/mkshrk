\chapter{Player controls}
\label{chapter:player-controls}
\begin{chapterabstract}
  \hlmagenta{This chapter explains the thought process behind the player's appearance, the control scheme, how it is implemented in the game, and the gameplay mechanics of storing items in a backpack.}
\end{chapterabstract}

The movement of the player is mostly inspired by the game Half-Life:~Alyx and its continuous hand control mode described in Section~\ref{section:hl-alyx}. The differences between their control scheme and this thesis' were the removal of the ability to climb over obstacles and the ability to turn using the joystick. The reason why the turning system was removed is to enhance the immersion, forcing the player to rotate physically in the real world.

For the player, there is a new space the author has dubbed the Player Space, which contains the tracked objects representing the headset and controllers. This entire space is moved using the joystick and \texttt{CharacterController}. This, however, means that the camera/headset will not match with the location of the character controller at all times. Because of this, the \texttt{UpdateCharacterCenter} function in the script responsible for the player's movement correctly updates the character controller's position and height to the headset's current position. Furthermore, when moving, the game checks the slope on which the player is located and adjusts the speed of the player, with steeper slopes causing the player to be slower to simulate fatigue on hills. If, however, the slope is higher than a predefined value, the player will not be able to move upwards, only to the side or downwards. The logic of the player controls is contained within the \texttt{PlayerControl.cs} file.

In contrast to the game that inspired the author, where the player's vision is obscured if they physically move their head into a colliding object, leaving a small hole that the player needs to move through to resume the game (as described in Section~\ref{section:hl-alyx}), this thesis' game moves the entire player space back to resolve the collision.

The hands are also inspired by the aforementioned game. However, in the Half-Life:~Alyx when the player picks up an item, the fingers are wrapping around the model, but in this thesis, the hand forms a fist, while the object is only simply parented to the hand, which results in held objects seemingly floating in front of the hand.

The controllers, which represent the hands, are each tracked individually to an object which the author refers to as ``the ground truth transform'' and does not interact in any way with the simulated physics world and is not visible to the player\footnote{Exception being adding visualization to this object for debugging and development purposes.}. The model of the hand, which is physically simulated with proper collision setup, is constantly following the ground truth transform using a \texttt{ConfigurableJoint}. The usage of joints allows the developer to set the maximum amount of force it can apply to the model in order to move it to the correct position, which makes the hand occasionally lag behind the ground truth position during quick hand~motions. Doing~so~will,~however,~provide more predictable and physically correct results as other forces would push the hand back during a collision. This further comes into play when the player wishes to grab objects around the world, as they would be unable to lift heavy objects or objects that are bound to an immovable prop.

Grabbing is done using FixedJoints, which determine the location where the player has grabbed the object and acts as the point from which it applies forces to keep up with the grabbed hand. This means the hand model is connected to the ground truth position using a \texttt{ConfigurableJoint} to match the expected hand position and is also connected to the grabbed object using a \texttt{FixedJoint}. Joining these objects in this way allows the object to be held by multiple hands, thus permitting the player to lift heavier objects. Diagrams of physical interactions with hands and objects can be seen in Figure~\ref{fig:hand-gt-grab}. If the \texttt{FixedJoint} requires too much force to move the grabbed object, the joint is destroyed, and the hand no longer holds onto the object, causing it to drop.

Objects the player can grab contain a \texttt{Grabbable} component or its derived class, which handles the interaction between the hand and the object. By deriving a class based on it, it is possible to pass additional controller inputs to it, which can be used to control held objects that would have been inconvenient physically as the controls would be too small and require high precision from the user (e.g., turning a knob on a radio to change frequency). When a grab action is requested, the game picks up the nearest object to the hand. As this may be unclear to the player, the game highlights the object they are about to interact with using a purple highlight, as shown in Figure~\ref{fig:object-highlight}.

\begin{figure}[]
  \centering
  \includegraphics[width=.4\textwidth]{assets/gtgrab1.png}
  \includegraphics[width=.4\textwidth]{assets/gtgrab2.png}
  \caption{A diagram of how the hands are implemented in the physics simulation within this thesis. The red-green axis gizmo represents the ground-truth position of the controllers. The visual hands are shown by the blue and red circles, connected using fixed joints to the held object, represented by the gray box. The visual hands have forces applied using configurable joints, represented by the pink vectors, to make it move towards the ground-truth position. On the left, the hands have been moved from the position during the initial grab; on the right, the object has moved up to respond to this change.}
  \label{fig:hand-gt-grab}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.7\textwidth]{assets/pickup-highlight.png}
  \caption{A book that the player would grab, being highlighted using a purple outline.}
  \label{fig:object-highlight}
\end{figure}

In addition to grabbing physics objects, the other way the player can interact with the world is by using what the author has labeled as ``gestures,'' which are areas that perform an action if a hand is within their boundaries and has performed a grab. If they reach the bounds of any gesture, a short haptics feedback is sent to the controller to signal the player they may grab to perform the action. This is used, for example, for the flashlight on the player's head, where the gesture zone is located right above their head. Reaching into it and invoking it will cause the flashlight to turn on or off.

\section{Backpack}
\label{section:backpack}

One of the key points of the game is to collect items and to be evaluated based on which items the player has grabbed and stashed away in their backpack\footnote{In the project files it has been dubbed the \texttt{kaban}, which is a romanized Japanese word for backpack}. Because dragging it across the world would be tedious and not enjoyable, the player can grab the backpack's handle, which will cause the backpack to be technically hidden. This is to simulate it being slung over the player's back. To make it appear again, the player would have to reach over their shoulder, where the gesture zone for making the backpack is, and grab it, which would make the backpack appear near them. The player needs to ensure they have slung it back on their back; otherwise, attempting to grab it over the shoulder would yield nothing, as it was left somewhere in the world.

The backpack essentially works as a LIFO\footnote{Last in, First out} structure. To add items to it, the player needs to toss it into its open mouth. To retrieve an item, the player needs to reach into the backpack, and depending on how early on they have stashed it, the deeper they need to put their hands in and grab it. To demonstrate, an example diagram can be seen in Figure~\ref{fig:backpack-lifo}. This also means that items that are too big to fit in will not be able to be stashed, as the player would not be able to put them physically into the backpack. Additionally, objects can be marked as unable to be stored regardless of their size. The backpack itself also has a limit on how much it can carry, with each object having its own weight property. If an object is dropped into it and accepting it would exceed the backpack's limit, it is not stashed and is forcefully pushed out of it. It is important to note that while it is called weight, it does not refer to the object's weight but to the volume it would take in the backpack. The limit of the backpack is set to $40$, with the units being in liters. All the items have a property describing the approximate volume they would take in the same units.

\begin{figure}[h]
  \centering
  \includegraphics[width=.85\textwidth]{assets/lifo-backpack.png}
  \caption{Diagram of the backpack. In the first image, there are two items in it, with a blue item the player wishes to stash. In the second image, the player has already deposited the blue item which is on top of the other items in the backpack, and wants to take out the red item at the bottom, so the player has to reach deep into the backpack to select it, with the red line visualizing how deep the hand is considered to be. After grabbing it, the remaining items are shifted down, and the player holds the red item.}
  \label{fig:backpack-lifo}
\end{figure}

The backpack's contents are then checked by the component managing the given scenario, as each scenario can have different goals and require different evaluation methods, described in Chapter~\ref{chapter:scenarios}.
\chapter{Analysis of existing media} % maybe the works
\begin{chapterabstract}
  This chapter will present a brief analysis of four modern publications that feature the theme of either post-apocalypse or survival. The subjects of this analysis are the Fallout franchise, Half-Life: Alyx, Introverts Till the End and Girls' Last Tour. The author has also taken inspiration from each during the creation of the scenarios in the thesis.
\end{chapterabstract}

\section{Fallout franchise}
\label{section:fallout}

Fallout is the long-running series of games set in a nuclear post-apocalypse, initially created by Interplay Entertainment, with later series done by Bethesda Game Studios and additionally by Obsidian Entertainment, who helped to produce the game Fallout:~New~Vegas.
% which is a company founded by the previous employees of the aforementioned Interplay Entertainment and

The games that the author had played were Fallout:~New~Vegas \cite{falloutnv} and Fallout~4 \cite{fallout4}; however, they were inspired mainly by the latter since the former focused mostly on the political side of the post-nuclear world, while Fallout~4 had more emphasis on scavaging and collecting materials for crafting purposes. 

Exploring the open world, commonly referred to as the Wasteland, can lead the player to various locations, which can be filled with enemies, destroyed buildings, caves, and many other points of interest. In these areas, there are usually a lot of items scattered around, which the player can pick up. Some may be on the ground lying around, on the table, or even stored in shelves, boxes, and cupboards. Across the environment, the player may also stumble upon many radio stations, which can be just transmitting music, instructions, or directions towards a certain place, or even distress signals from individuals or various groups.

The player themself is not limited by the number of items they wish to carry but by the weight limit. It is a soft limit, meaning that the game allows them to carry over the limit; however, they will be handicapped with slower movement and the inability to sprint. Each item also has a ``Value'' stat, which is the value of the item in caps, the game's universe's currency. Fallout~4 has a simplified crafting and scavaging mechanic, where instead of a crafting recipe requiring a specific item, it requires general materials (e.g., metal, wood, oil, screw, ...) that can be obtained by decomposing found items. An example of such an item can be seen in Figure~\ref{fig:fallout4-item}.

\begin{figure}[]
  \centering
  \includegraphics[width=.55\textwidth]{assets/fallout-4-item.png}
  \caption{Example of an item called ``Gas Canister'', which can be decomposed into 3 Steel and one Oil, weighs 3 units, and could be sold under 10 caps.}
  \label{fig:fallout4-item}
\end{figure}

In the Wasteland, during one of the quests, the player will be given a chance to visit a place called the ``Old Corner Bookstore'', which was used further as an early inspiration for the building's layout of the library in the scenario that will be described in Section~\ref{section:scenario-library}. When the player enters the building, they are met with a front hall, rows of bookshelves, a reception desk, and stairs that lead to the second floor of the bookstore. Both of these examples can be seen in Figure~\ref{fig:fallout4-bookstore}.
% Mainly the front area right after entering the building, the player would be greeted with a front hall and using the stairs in the corner, they would be able to visit the second floor of the bookstore.

\section{Half-Life: Alyx}
\label{section:hl-alyx}

Half-Life:~Alyx \cite{halflifealyx} is a prequel to the popular video game franchise Half-Life by Valve, released in 2020. The game is set in a dystopian world, where an alien race called the Combine has enslaved humanity. It is a virtual reality game that has won the 2020 Game Awards in the ``Best VR/AR'' category \cite{thegameawardsGameAwards2020} and is shipped with Valve's VR headset (called Valve Index) \cite{halflifealyx-index}. % The game requires a 6DOF headset and controllers to be played.

The main inspiration the author had taken from this media was the control scheme of the player in the environment, specifically the continuous motion mode controls. The continuous mode essentially moves the player through the world without teleporting or sudden changes in velocity. The other modes left are ``Blink'' and ``Shift'', which teleport the player with different ways of transitions. The thesis will only work with continuous locomotion for full immersion with the cost of possibly causing motion sickness.

Continuous mode allows the player to move themself using the controller's joystick, which can move the player in the direction of either the hand or the head, depending on the player's preference. During the move, the collision model of the player moves with them, with gravity's force being applied to it. It will also collide with objects more crudely. When the player has stopped using the joystick, the collision is more lenient, as the player's collision shape is now only around the head and not the entire body. This allows the player to lean over tables, look out of the windows, peek into shelves, and many more actions. Figure~\ref{fig:alyx2}  demonstrates this behavior. In cases where the head collides with anything, the player's vision is obscured with only a small window, into which they are required to move their head and body in order to resume the game. The game also permits the player to teleport if they hold the joystick back on their primary hand, aim at the desired location they wish to move to, and release the joystick.

Turning the view around to look behind or adjusting the player's orientation can be done by physically rotating themselves in real life or flicking the joystick horizontally. Similar to how the movement is handled, as mentioned above, the rotation can be instantaneous or gradual, depending on the player's preference. The latter is, however, more disorienting and prone to cause motion sickness.

As mentioned before, the force of gravity is only applied to the player when their collision shape is in the air or when they are moving around using the joystick. Because of that, it is possible to physically move your real body off a virtual ledge. To prevent this, the game has a certain leniency when the joystick is untouched. However, if the head moves too far from the collision shape of the body, the game forces the collision shape of the body to be updated to be below the head, after which, if the player's head were indeed located off a ledge, they would fall. A diagram showcasing this behavior is shown in Figure~\ref{fig:alyx1}.

Within the game, there are various levels where one has to go up a ladder, and the game allows multiple ways to perform the ascension. The player can climb manually step by step by grabbing the ladder and pulling downwards, effectively lifting themselves off the ground. Once the player has been lifted a predefined amount, the game teleports them to the top platform. Another way to ascend is to aim the controller at the ladder and hold down the teleport button, and after some delay, releasing the button will teleport them to the top as well. The same methods are applied when the player needs to descend on a ladder. In most cases, the former is physically impossible, as the location of the ladder in the physical world would have been in the ground and unreachable by the player.

Another point of inspiration was how the game handled the player's hands, as they are part of the physics world, and items such as buttons and levers have to be physically pushed or pulled to be activated. The hands also act as the player's avatar, representing them in the virtual world.

\begin{figure}[h]
  \centering
  \includegraphics[width=.45\textwidth]{assets/alyx-analysis-2.png}
  \caption{An example of the head having separate collision bodies from the player controller, allowing the player to lean over a wall. The capsule represents the player controller, while the tracked head is a red circle. The gizmo axis represents the origin of the tracked player space.}
  \label{fig:alyx2}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=.65\textwidth]{assets/alyx-analysis-1.png}
  \caption{An example of the controller character updating to match the tracked head position. The capsule represents the player controller, while the tracked head is a red circle. The gizmo axis represents the origin of the tracked player space. On the left, the distance between the head and the controller is still within the threshold. When this threshold is crossed, the player controller is updated by moving its center and adjusting its height. After this update is done, the player will fall.}
  \label{fig:alyx1}
\end{figure}

\section{Introverts Till the End}
\label{section:itte}

Introverts Till the End \cite{introvertsFF-ao3} is a fanfiction\footnote{Literary work that's based on a franchise and has been made by its supporters} by an author who goes by their pen-name ``tripsout2''. The author writes about characters from a group of online streamers who use animated characters to represent themselves\footnote{Commonly referred to as ``Virtual YouTubers''} called Hololive, specifically the Hololive~English~Myth group and its five members, and included them in an alternative universe located in a zombie post-apocalypse of unknown cause.

The story is set in winter and is told from the perspective of the protagonist, Amelia Watson, with the premise that she is a lone wolf surviving the apocalypse with her dog and not knowing any of the other girls. The first few chapters are about how she met the other members who travel together. Each encounter with them is set in a different location, circumstance, and activity over which they bond. After that, she develops trust with the traveling group and provides food to them, even killing a deer for them.

Two chapters, specifically the second and fifth, have been used as inspirations for various aspects of this thesis' scenario designs, which are further elaborated in Section~\ref{section:scenario-library} and Section~\ref{section:scenario-cave}.

In the second chapter, the narrator describes that winter is beginning, and Amelia requires warmth to survive. \hlmagenta{In the library, the protagonist encounters Ninomae Ina'nis, a member of the traveling group who arrived before her. They both considered using the books as fuel instead of gathering firewood, as it was the faster option.} The paragraph in question can be found in Figure~\ref{cite:ina-library}.

\begin{figure}[ht]
  \begin{framed}
    \begin{quote}
      ``\textit{She wasn't about to light a campfire every night, but she still needed warmth. Warmth required wood. Unless she wanted to go out and hack at trees all day, her best bet was to find something small. She's also lazy, so she makes a path for the library two blocks away with full intentions to burn literature.}''
    \end{quote}
  \end{framed}
  \caption{Paragraph from the second chapter of Introverts Till the End, mentioning the character's method of obtaining fuel for a campfire \cite{introvertsFF-ao3}.}
  \label{cite:ina-library}
\end{figure}

In the fifth chapter, Amelia ventures to a river near the town to gather water with her dog. While the story is set in the winter, the river's stream is not frozen due to its strong flow. The method of gathering water she used was scooping it from the river using buckets she had brought with her. However, while she was gathering water, unbeknownst to her, her dog had run away, and after a quick investigation, she noticed that the dog's paw prints in the snow ended at the river bank. Amelia then, without thinking, jumps into the river in an attempt to find her dog, who she presumed has been swept away in the strong stream. It is implied that the flow is strong enough to make a person drift away, as seen in Figure~\ref{cite:river-bubba}.

\begin{figure}[ht]
  \begin{framed}
    \begin{quote}
      ``\textit{Winter makes it difficult. The river doesn't freeze because the waters move so quickly. [\dots] She can't find where she left her bucket. She has no idea how far she drifted downstream.}''
    \end{quote}
  \end{framed}
  \caption{Selected sections from the fifth chapter of Introverts Till the End, describing the river's flow \cite{introvertsFF-ao3}.}
  \label{cite:river-bubba}
\end{figure}

\section{Girls' Last Tour}
\label{section:glt}

\textit{Girls' Last Tour}\footnote{Also known as ``\textit{Shōjo Shūmatsu Ryokō}''} \cite{GirlsLastTour} is a Japanese comic series written and illustrated by \textit{Tsukumizu}. The story is set in a post-apocalyptic environment of unknown cause; however, there are strong inclinations of war being one of the factors leading to the end of civilization, with only a handful of people being alive and scattered far from each other. The series follows two female protagonists as they explore the wasteland, occasionally stumbling upon a few survivors.

% "The series was later adapted to be an animated series spanning 12 episodes in late 2017."
% The above was extracted as being unnecessary

While the author of this thesis has read both the comic series and watched the animated adaptation, all references going forward will be related to the latter. The notable sections for this thesis are located in episodes 3 and 5 of the animated series. 

In episode 3, the two main characters are in a scenario where they need to cross a large hole between two sidewalks, with no way of walking around it or a bridge in sight. The protagonists also notice that there is a still smoking cigarette butt on the floor and footprints in the snow, suggesting that someone other than them is in the vicinity. As they look around for any threats, bracing and arming themselves for potential conflict, an explosion suddenly occurs on one of the ground floors of a tall building behind them, causing it to fall over, forming an impromptu bridge between two sides. After that, they meet a survivor coming out of the dust clouds caused by the collapse, who had gathered the explosives and caused it intentionally to form the bridge, as shown in the Figure~\ref{fig:glt-bridge}. 

In episode 5, rain suddenly occurs during their exploration in the wasteland, forcing them to take cover in a cave made of ruins of what seems like a building. The environment resembles a forest; however, all the trees are steel beams stuck in the ground. After they arrive at the cave and settle, one of the protagonists orders the other to use buckets to collect the water dripping from the holes of the imperfect roof. The cave and the surroundings can be seen in the Figure~\ref{fig:glt-cave}.

\begin{figure}[ht]
  \centering
  \begin{minipage}[t]{.45\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{assets/glt-bridge.jpg}
    \captionof{figure}{A blown up and collapsed building that bridges two sidewalks together.}
    \label{fig:glt-bridge}
  \end{minipage}%
  \hspace{.025\textwidth}
  \begin{minipage}[t]{.45\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{assets/glt-cave.jpg}
    \captionof{figure}{The cave the protagonists settled in during the rain, surrounded by steel beams protruding from the ground.}
    \label{fig:glt-cave}
  \end{minipage}
\end{figure}
\chapter{Sound effects}
\begin{chapterabstract}
This chapter describes the sound effects, their source, and how they are invoked within the game.
\end{chapterabstract}

One of the most important parts of an immersive game is sound effects that are played based on the actions of the player and the environment. It can tell the player a lot of information about the surroundings without being directly told, for example, something occurring behind closed doors, what they are standing on, and much more.

The sound effects used in the thesis are borrowed from the game Half-Life~2 \cite{halflife2} due to its wide library of effects used and easily recognizable for those who have played any of the games in the series. \hlmagenta{The author themself recorded a couple of sounds that were used in the game, one being an ambient sound track obtained by going out and recording audio in times and places with no people or vehicles nearby, and the other a radio transmission used in the scenario described in Section~{\ref{section:scenario-cave}}.} % The author themself recorded ambient sounds by going out and recording audio in times and places with no people or vehicles nearby.

Unity allows the developer to tweak how the audio is being perceived using various effects. The most commonly used effect in this thesis is the inclusion of reverb to simulate different environments, for example, the echo in the forest or the reverb in tightly closed spaces. This is done to simulate how the audio would have bounced physically in the world. This is noticeable when the player is walking through the city streets and then entering a building, where the reverb and echo sound different. It is possible to have a more physically accurate physical simulation of the audio using third-party solutions such as ``Steam Audio''; however, due to its overwhelming complexity, it was decided that it was not suitable for the thesis at the moment.

In the thesis, sound effects are grouped into ``Sound pools''. This is done to provide the sound clips to components in a consistent way and to easily pick the clips randomly. These pools are further utilized in audio emitting systems such as footsteps, impact sounds, and certain interaction sounds.

Most, if not all, components and objects that emit sound utilize Unity's Audio Mixer function. These mixers function similarly to their real-world counterparts, allowing the volume adjustment to a group of audio sources or adding effects to them. This thesis uses it to tweak the volume globally to avoid going to extremes: being too loud or too quiet. To ease access to these mixers, it is possible to access them using the \texttt{AudioManager} class, which, alongside functions to play a sound effect anywhere in the world, gives access to the master mixer\footnote{The master mixer controls all of the other mixers.}, ambiance mixer, footstep mixer, and impact mixer.

\section{Footsteps}

Footsteps are emitted from the player's feet and differ based on the surface on which the player is walking. The game keeps track of the distance the player has walked, and once it has reached a certain distance corresponding to the distance of a human step, a random footstep audio clip is played beneath them, and the distance counter is reset to zero. If the player is walking up or down a slope, the counter's increment rate is higher to make the footsteps occur more often. The footstep audio clips are also grouped into sound pools based on the material. If the ground has no audio material defined, the concrete material is used as a fallback to prevent the illusion of the player being silent while walking.

The limitation of this is that the level designer, in this case, the author of the thesis, has to specify the material of the ground or any surface the player could step on manually. This means that if a ground is made out of multiple materials, only one audio pool of footstep sounds can be used unless it is separated into multiple colliders. There was an option to do this by checking the triangle of the mesh it has collided on; however, this is more computationally expensive and would require the mesh to be set as readable during its import, which also increases the memory usage \cite{unityDocs-readableMesh}. 

\section{Interaction sounds}

Interaction sounds include both sounds emitted by the player interacting with items, such as buttons, and sounds emitted by physically simulated objects in collisions. The system in place has a similar problem as the footstep system, as for every object desired to have sounds being played during its impact, a component has to be added to them since Unity does not have a way to detect collisions globally and only on a per object basis.

The audio clips are now gathered in two sets of audio pools, one for ``soft'' impact and the other for ``hard'' impact. Depending on the velocity of the impact, a different sound is played, with soft audio clips being played at lower velocities. Additionally, the volume of the audio clip is also further scaled by the velocity to convey the strength it delivered upon impact. % The different ranges can be visualized as in Figure~\ref{fig:softhardaudio}.

% \begin{figure}[h]
%   \missingfigure{Add image/formula defining the ranges for soft/hard sound effects}
%   \caption{tehepero}
%   \label{fig:softhardaudio}
% \end{figure}

There are some objects that perform sound effects based on the state they are in or the player's actions. In the simplest cases, since they already need to perform an action upon their interaction, a sound effect is added to be played after the said action is performed. This applies to the buttons when pressed, to doors when opened or closed, or to certain windows that are allowed to be broken.

Some objects that do not necessarily perform anything utilize various monitoring scripts, such as \texttt{MonitorDistance}, which is a script to detect when an object has moved away too far from or into its initial position and to invoke events and actions based on it. This was used in the case of the cash register's sliding drawer when a sound effect would be played if the drawer had fully retracted back.

However, there are objects that play sound effects not necessarily due to the player's interaction but due to physical properties being applied to it. The difference between these and former sound effects is that it does not utilize the sound pools but play a single looping audio track. To achieve this, a script called \texttt{SoundOnVelocity} is used, which monitors either positional or angular velocity. If the magnitude of the monitored velocity is higher than a certain threshold, then the audio track is played with its volume adjusted to the magnitude of the velocity. The use of threshold limiting prevents the audio from being played when the object is barely moved, and the volume modulation conveys the strength of the force that was applied. The adjustment is also smoothed out over time to prevent stuttery-sounding audio. In this game, this was used to add sound effects to both the regular doors and shutter doors, with the former utilizing the angular velocity of the rotating door and the latter the velocity of the sliding vertical door.

% For objects that are interactable such are buttons and door handles, they already emit an event to perform their desired action, so they just have an additional sound play function in the event stack. There are however objects that emit sound that are not captured by such events, such are creaking doors or cash registers. For these objects, a script is actively monitoring their velocity/torque and adjusting a looping audio clip's volume. Furthermore, it also emits sound when the object reaches a certain position, which is used for a door's click when it is closed or when the roller doors slam down to the ground.

\section{Ambient sounds}

By design, the game is devoid of any background music and uses ambient audio tracks, some of which were recorded by the author. These tracks mostly consist of sounds of chirping birds, wind whistling, tree rustling, and miscellaneous distant sounds.

However, if the game were to treat these tracks as background music, then it would play regardless of the player's location. This was especially evident in areas where the player is able to get into a building and would still hear the wind and trees inside. To remedy this, a script called \texttt{AmbienceManager} is used, which is also responsible for playing the ambient tracks. In addition to that, it allows the level designer to define volume boxes in the world to determine where the volume should be reduced. The volume is then smoothed over time to avoid stutter and sudden changes in audio. These box volumes also allow changing the ambiance tracks, which are used, for example, to play wind rustling through trees in the forest and not in the city.

To simplify the defining process of these boxes, the component itself adds controls into the editor that allow intuitive resizing and moving of existing boxes. An example of different boxes with their controls can be seen in the example scenario in Figure~{\ref{fig:ambientexample}}.

\begin{figure}[h]
  \centering
  \includegraphics[width=.9\textwidth]{assets/ambience-zones.png}
  \caption{Example of ambience zones in the \textit{library} scenario. The purple zone covers the entire library, where the outside ambiance is quieter, and the green zone inside the library makes it even more quieter. The teal zone on the right covers the forest and switches the ambient track into a leaves rustling one.}
  \label{fig:ambientexample}
\end{figure}
\chapter{User tests}
% \begin{chapterabstract}
% \end{chapterabstract}

The game was tested by the author and three other people who have varying levels of knowledge and experience in VR gaming and gaming in general. These tests were conducted on the developer's machine and equipment with the specifications listed in Table~{\ref{table:testspecs}}. A third-party program called ALVR {\cite{alvr}} was used to stream the VR content from the computer to the headset wirelessly. Because of this, however, all test subjects were affected by a latency issue. The results of their playtesting sessions were used to adjust various gameplay elements, visuals, and pay-off matrices.

\begin{table}[h]
  \centering
  \caption{Computer and headset specifications used during the playtesting}
  \label{table:testspecs}
  \begin{tabular}{|l|r|}
    \hline
    \textbf{CPU} & AMD Ryzen 7 6800HS \\ \hline
    \textbf{GPU} & NVIDIA GeForce RTX 3060 Mobile \\ \hline
    \textbf{RAM} & 16 GB \\ \hline
    \textbf{Headset} & Meta Quest 2 \\ \hline
  \end{tabular}
\end{table}

Before the testing had begun, test subjects were informed about the use of the controllers, safety precautions about the space around them, and an option to pause or quit playtesting if they had become motion sick or unwell. \hlmagenta{They were not told anything about the scenarios or game controls.} Furthermore, they were asked for consent about having their gameplay footage recorded alongside their voice, as they were encouraged to vocalize their thought process out loud. After this, they were equipped with the headset, and started testing the gameplay.

The player starts in a square room, where they can find three buttons with labels indicating the scenario it would start upon pressing it and the goals of said scenario. On the right side of the room, they are informed about how to control their character and how to interact with the world. The room itself can be seen in Figure~{\ref{fig:mainhub}}. After the end of each scenario, the player would appear back in this room, however, with the new addition of a wall of text on the left side of the room, showing the result of the scenario, details about the actions recorded, and the calculated score of the evaluation if they had succeeded. If they had not, they would be met with the reason why they had failed. The thought and methods applied during the calculation of this score are already described in the evaluation sections of individual scenarios in Chapter~{\ref{chapter:scenarios}}.

\begin{figure}[h]
  \centering
  \includegraphics[width=.31\textwidth]{assets/scenario-result.png}
  \includegraphics[width=.31\textwidth]{assets/main-hub.png}
  \includegraphics[width=.31\textwidth]{assets/instructions.png}
  \caption{In the center is the main hub, where the player can select which scenario they want to try. Instructions on how to control the game are on the right, and the result of the last scenario is on the left if they have just returned after finishing one.}
  \label{fig:mainhub}
\end{figure}

Before the test begins, the author ensures that the headsets are correctly calibrated to the environment, meaning that the floor the test subject would be standing on would match the virtual one in the game. Each of the selected test subjects will be testing all of the scenarios in the order listed in this thesis. During the test, the author is in the same room with the test subject and silently monitors the state of the game while keeping them safe by checking the physical boundaries to ensure they would not get hurt while in VR. Only questions unrelated to the game or the scenario would be answered, such as the technical aspects or potential issues that may arise. After experiencing each scenario at least once, individuals can redo previous scenarios with the new knowledge gained from previous attempts. The results from these test sessions consist of the recordings of what they have perceived in the headset with their thoughts voiced out. These were viewed by the author, who then decided whether adjustments are necessary and possible.

% First test subject

\section{First test subject}

The first test subject had a lot of experience with various gaming and game mechanics but had little to none with virtual reality. As they were the first play tester, they discovered a lot of oversights and issues in various aspects of the game that were hard to spot by the author, given their pre-existing experience with the game.

Right in the main hub, they noted that the instructions written out were sometimes confusing and were initially lacking essential information. They had also noted that reading it was uncomfortable, as the main hub was a small room, and the text was spread out over the entire wall, making it very big and close to the player. This issue has been remedied by pushing both the instructions and the result screen back; however, an obstacle was added to keep the player within the bounds of the original room.

In terms of the controls, they had difficulty understanding how to put the backpack down from their back. As noted in Section~{\ref{section:backpack}}, a gesture of grabbing the air above the shoulder would make the backpack appear. \hlmagenta{During the play session, the gesture was for the player to grab the air above their shoulder using the opposite hand, such as using their right hand to grab the left shoulder and vice versa.} This turned out to be very confusing for them, and as such, it has been changed so the gesture can be invoked by either hand over either shoulder.

Their experience with the item grabbing was seemingly frustrating. The observer noticed that nearby objects were flung due to the rapid transition of the hands from a fist to a fully extended position. To address this issue, the author has limited the extension of the hand to a partial curl, assuming that the player has a firm grip on the controller.
% As the author watched closely, they noted that it seemed to be an issue of the hands going from a fist to fully extended so quickly that it flung the nearby objects. To remedy this, as the player is expected to have a firm grip on the controller, the author has limited the hand from being fully extended to only partially curled.

During the first scenario (the library), the subject had missed the backpack entirely and was exploring the world. They were left wondering how they were supposed to stash the items. The author acknowledges that the backpack's initial position was quite far and in a location that would cause it to blend with the background visually, so it was moved closer to the player's initial position. While exploring, they also often forgot if they had the backpack with them on their back or not since there was no clear indication. In the library, while they had discovered the employee-only door and even the employee card for it, they failed to connect those two entities and discarded the card.

\pagebreak
In the second scenario, the subject has spent a long time attempting to break the window to the store, not once noticing the hole in the fence that'd allow them to progress. A visual indicator in terms of a decal on the floor at the broken fence was added to make it more visible. They have also gotten disoriented in the dark room, even with the flashlight. In an attempt to lessen the issue, a different light mapping system was used that simulated secondary light bounces from real-time sources, making the flashlight illuminate even the surrounding walls and making the scene, in general, brighter and more visible. They had also noticed the noticeboard but did not notice the warning and locked themself in the basement.

How to exit the scenarios had to be told to the subject, as it was not clear, given that it appeared in all of the scenarios as a prop near them that seemingly served no purpose other than being a background element. To help the future players, a small text indicating that the prop they are about to grab would exit them out of the scenario was added, which only appears if they are in its close vicinity.

The score calculation had to be adjusted in regards to the time being too big of a factor, heavily punishing the player in not time-limited scenarios for taking their time exploring. For these reasons, the time in these scenarios is further divided by a constant, so the players are still rewarded for completing the scenario quickly but not punished too harshly when they take their time.

% Second test subject

\section{Second test subject}

The second test subject had no experience in virtual reality and had, in comparison to the first test subject, less gaming experience. Thanks to that, the feedback provided valuable insight into how the controls are to a complete newcomer to virtual reality. This also meant that, as a player, they had read the instructions more carefully; however, that did not stop them from asking to be reminded about the backpack controls later. They remarked that the wall of text, while understandable, could be better if it had a dedicated tutorial level where the players could try the mechanics out. However, due to the project being in very late stages of development, it could not be implemented.

During the library scenario, even with the adjustments done, they walked by the backpack and did not take it with them. At the start, they wandered blindly, testing out the grabbing mechanics with the books in the library. After a while, they noticed the employee-only room and recognized that it was currently inaccessible but could be unlocked using the card reader next to the door. Initially, they found the newspaper decals outside, which were not interactable, so when they found the key card in the drawer, they assumed that it was also just for decoration. After being informed that it could be grabbed, they managed to enter the room. After reaching the table inside it, they noticed the map, figured out the layout of the scenario, and started heading towards the forest. Once they had approached it, they noticed that their body temperature was very low and decided against going further, so they turned around and exited the scenario. They, however, did not make it and succumbed to hypothermia.

The second scenario was the store. Right from the beginning, they had attempted to break in using the baseball bat found near the windows. Later, they noticed the hole in the fence relatively quickly after the marks were added to the floor. In the dark room, they seemed less disoriented than the last test subject, as the room was now better illuminated. By pure coincidence, they had stumbled upon the hidden stash of water bottles near the collapsed metal shelves and had stashed some away. On their way back, they had found the noticeboard and the warning on it, and because of that, they had avoided going to the basement and attempted to exit the scenario. However, the testing session was cut short since they were getting very motion-sick.

\pagebreak

They noted that during the second attempt at a scenario, given that they knew what the game was about and how to control it, it was much easier to focus on the scenarios themselves. For example, in the beginning, they had issues distinguishing between the trigger and the grip button. However, during the second attempt, they had already grown accustomed to the control scheme and were grabbing items without any significant issues.

% Third test subject
\section{Third test subject}

The third and last test subject had the most experience in virtual reality out of all the subjects, and because of that, they had the least amount of trouble traversing the world and using the menu. There was, however, a brief moment where the backpack instructions regarding putting it on and off their back had to be explained again at the beginning. They noted that even with the instructions on how to put the backpack on the player's back, the gesture required was not very intuitive.

The first scenario they tried was the store instead of the library since they had accidentally pressed the wrong button. In there, they had trouble seeing the hole in the fence even with the marks on the ground and had only noticed once they were hinted that the fence played an integral part in the solution. Out of all of the test subjects, they understood that the roller door could be propped up by the barrels found underneath the docking area and were utilizing both hands to do so. While they did not find the hidden stash of bottles, unlike the second test subject, they went to the store and gathered some food resources. Curiously enough, they had gathered many boxes of salt. After that, they went back to the employee lounge and failed to acknowledge the noticeboard and its warning, heading straight to the basement, where they had found the body and the sleeping bag. While carrying the sleeping bag back up, they noticed that the door handle was missing, and the scenario ended there.

The next scenario they attempted was the library. This testing was relatively short. They had entered the library with the backpack with them and started stashing the books, as they had immediately noted that books could be used as fire fuel. While doing so, they used a technique: instead of grabbing the books individually, they placed the backpack near the shelves and just pushed the books down into the backpack. They were also the first test subject who had managed to fill the backpack to its fullest. However, they were confused about why the books were launched from the backpack, which occurs when the new item would overstep the backpack's capacity. After that, they left the library and headed towards the forest, where they had gathered some water bottles until their backpack was full. During the collection of the bottles, they inquired if the water bottles were filled with water or not. However, they concluded that because they were in the trash can, they were empty. Because of the full backpack, they had decided to exit the scenario.

They had briefly also attempted the cave scenario. In the first moments, they had noticed the bucket and the map on the table and grew vary of the minefield nearby. They were, however, disoriented and did not know which way the minefield was. When interacting with the bucket, they accidentally dumped all the water out and figured out that they could use the river to fill it back up. However, when trying to do so, they dropped the bucket into the river and attempted to retrieve it, which caused them to end the scenario as they drifted away, ending the scenario.

% Common remarks
\section{Summary of testing}

From all of the three test subjects, there were a few remarks all of them had regarding the controls. One of them is that the instructions were not the clearest during the first playthrough, but after learning and trying the game mechanics out firsthand, they understood the point of the game. The third subject also noted that the latency introduced due to how the game was running on the computer and wirelessly transmitted to the headset greatly contributed to the motion sickness, affecting all test subjects. This could not be solved at the time of testing due to a lack of equipment on the author's side.

The backpack, the key part of the game, was commonly ignored on the first playthrough; however, they have always remembered to take it with them on subsequent attempts. At times, however, they seemed to forget if they were carrying the backpack on their backs or not.

For the library scenario, only one of them found their way into the employee-only room and ventured to the forest; however, none of them had found nor attempted to find the wooden cabin in the forest. The subject who got closest to the forest decided against it because it was dangerous for them. All of them had also gone up to the second floor in an attempt to search the lockers, which caused them to lose a lot of time. On that floor, all of them had also asked a sincere question: whether this game had horror elements.

In the store scenario, they all attempted to break the glass once they had found the baseball bat. However, none of them immediately realized it was impossible to break it completely. They also attempted to pull open the door to no avail. All of them managed to get into the store with or without help, with one of them even propping the shutter door to prevent it from falling onto the player. Only two subjects had noticed the noticeboard; however, only one read and heeded the warning. Within the store, all of them thought the counter gate was static and crawled underneath it.

The cave scenario was the least tested, and no one had completed it, as it was the last scenario, with most of them having become motion sick and two of them dying in a river. They had also noted that the sleeping bag was unclear and that it was the exit to the scenario.

The scores of the individual testing sessions of the subjects are summarized in Table~{\ref{table:summary-testing}}, alongside the time they had finished and a short commentary, if applicable.

\begin{table}[h]
  \centering
  \caption{Summary of testing sessions with the subjects and their obtained scores, sorted by the scenario.}
  \label{table:summary-testing}

  \begin{tabular}{|c|c|c|c|l|}
    \hline \textbf{Test subject} & \textbf{Scenario} & \textbf{Time [s]} & \textbf{Score} & \textbf{Comment} \\ \hline
    First  & Library & $920.61$  & $1932$  & {}            \\
    Second & Library & $1015.59$ & $0$           & {Hypothermia} \\
    Third  & Library & $379.48$  & $10168$ & {}            \\ \hline
    
    First  & Store   & $1848.17$ & $0$           & {Locked themself in the basement} \\ 
    Second & Store   & $1232.38$ & $22225$       & {} \\
    Third  & Store   & $1024.04$ & $0$           & {Locked themself in the basement} \\ \hline

    First  & Cave    & $146.26$  & $0$           & {Drifted away in a river}    \\
    Second & Cave    & --          & --            & {Too motion sick to attempt} \\
    Third  & Cave    & $128.19$  & $0$           & {Drifted away in a river}    \\
    \hline
  \end{tabular}
\end{table}

\hlmagenta{During test sessions, if players left the backpack in the world and exited the scenario, they would not receive a score of 0 for the items, as this was added after the author observed that players commonly forgot to retrieve their backpacks. If it had been added before, all scores would have been zeroed.}
% It is important to note that during their test sessions, leaving the backpack in the world and exiting the scenario would yield the player score of 0 for the items. It was added after the author witnessed the players commonly forgetting it. All scores would have been zeroed if it had been added before.

% \hl{The author feels disappointed that the cave scenario could not be tested as thoroughly and witnessed by more people, as it had a few new features and mechanics that the subjects did not find.}
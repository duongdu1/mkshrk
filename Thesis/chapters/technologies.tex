\chapter{Technologies used}
\begin{chapterabstract}
  % Within this chapter, I will be going through the technologies I have used for this thesis, mainly the game engine Unity and its physics engine, the 3D modeling suite Blender, and OpenXR API.
  This chapter is dedicated to explaining the technologies used in this thesis, mainly the game engine Unity and its rendering pipelines and physics engine, the 3D modeling suite Blender, and the OpenXR API.
\end{chapterabstract}

\section{Unity}

Unity, developed by Unity Technologies, is a multi-purpose 3D and 2D engine that has found itself in the gaming, movie, medical, and many other industries. The engine was chosen for its modularity, simplicity, and great support for XR-related applications. The editor is also readily available on Windows, Linux, and even macOS.

The version and edition used in the thesis is 2022.3.4f1 Personal, due to it being the LTS\footnote{Long-Term Support} version at the time of writing and includes packages that had proper feature maturity for this project. The Personal edition is also the only Free Tier available without going through any sort of additional verification.

In Unity, Scenes are used to group GameObjects, which are objects that are defined by their transformations (position, rotation, scale)\footnote{Same concept as scenes in section \ref{section:basic-3d}}. Their purpose and behavior are then further defined by Components, which can be either written by the user using the scripting language C\#, or one of the Unity's built-in components \cite{unityDocs-gameobjects,unityDocs-components}. These components can serve to play sounds, move objects in accordance with the user's input, render a model, and for many other purposes.

Unity provides the user the ability to choose different rendering pipelines, which dictate how objects are going to be drawn on the screen. By default, it provides these three rendering pipelines \cite{unityDocs-intro-rpp}, all of which differ from each other to some degree:
% Unity, by default, provides the developer with these three rendering pipelines, all of which differ from each other to some degree:

\begin{description}
  \item[Built-in Rendering Pipeline:]
    It is the default pipeline Unity would use on a completely blank project. While it is the most mature out of the three, it lacks proper visual shader editor support \cite{unityDocs-shaderGraph-changelog-gettingStarted} and proper decal support. It is possible to supplement them using third-party packages.
  \item[Universal Rendering Pipeline (URP):] 
    It was previously known as the Lightweight Render Pipeline\footnote{LWRP}. It is very similar to the built-in rendering pipeline, but it is still in development, and thus, a few features that would be there are not available here. However, it supports the visual shader editor, decals, and a few other features, which will be discussed further in the subsection~\ref{section:urp}.
  \item[High-Definition Rendering Pipeline (HDRP):]
    It is the most performance-demanding rendering pipeline with great visual fidelity and the most advanced of the three, being tailored for photo-realistic purposes (e.g., movies, architectonic previews, games). It is not suitable for this thesis, given that the performance cost of rendering in VR would be too high for most devices.
\end{description}

In this thesis, the author has chosen the Universal Rendering Pipeline, especially since the aforementioned version has finally received long-awaited features that outweigh the reason to stay on the built-in pipeline. As mentioned before, this is going to be further elaborated in the subsection~\ref{section:urp} below.

\subsection{Universal Rendering Pipeline}\label{section:urp}

Universal Rendering Pipeline (URP, previously known as LWRP) is directed to be the successor to the built-in render pipeline \cite{unity-srp-blog}. The pipeline was chosen based on its features and performance, which made it suitable for this thesis.

As mentioned above, while comparing it with the current built-in render pipeline, there are some features that URP has that the Built-in does not and vice-versa. While the differences in features used to be much greater, at the time of writing, the only feature that was missing and would have been helpful for this project is the Auto-Exposure post-processing effect\footnote{An effect that is performed after the image is rendered, but before it is shown to the user.} \cite{unityDocs-rp-compare}. This effect simulates how the human eye adapts to different light sources of varying brightness by adjusting the eye's iris and changing the amount of light entering the eye. It mimics this behavior by adjusting the exposure of the camera in a similar fashion, and it allows the developer to add lights into their scenes without worrying about dark areas being pitch black or bright areas being pure white \cite{unityDocs-autoexposure}.

The main advantages that decided the use of this pipeline were mainly the \textit{Decal system}, \textit{Shader Graphs}, and the \textit{Forward+ rendering path}., all of which are elaborated below.

\subsubsection*{Decal system}

Decals are essentially textures being overlaid over a model. There are different ways of achieving it. For example, a flat surface applied onto a wall is one method; however, it is limited to only flat surfaces. The system that both URP and HDRP have allows the user to apply decals to virtually any surface of any shape.

While it is possible to create decals on complex shapes in the built-in render pipeline as well, it does not have the same configurability, flexibility, and user-friendly experience as it does on URP and HDRP. For example, in URP and HDRP, limiting which objects the decal would be applied to is done by specifying the rendering layer that the object belongs to \cite{unityDocs-urp-decals}. However, in the built-in rendering pipeline, this would have to be done by changing the Layer of the entire object, which may affect other parts of the engine, most often the physics engine, which also utilizes said layers \cite{unityDocs-projector}.

\subsubsection*{Shader Graph}

The Shader Graph is available in the current version for all rendering pipelines, including the built-in one \cite{unityDocs-shaderGraph-changelog-12}; however, it is included only for compatibility reasons \cite{unityDocs-shaderGraph-changelog-gettingStarted}. It allows the developer to build shaders using various nodes as an alternative to writing shaders in shading languages such as HLSL\footnote{High Level Shading Language}, the language Unity uses.

Apart from simple color modulation and arithmetic color effects, it also supplies the user with various types of noise generators and even parallax mapping effects without any coding work from the developer's side. \cite{unityDocs-shaderGraph-procedural-14} %It also allows the shader to be rendered in batches and not on a per-instance basis automatically. \todo{CITATION NEEDED.}

\begin{figure}[]
  \centering
  \includegraphics[width=1\textwidth]{assets/crt-shadergraph.png}

  \caption{Example of a shader graph for an old-school monitor screen}
  \label{fig:shadergraph-example}
\end{figure}

\subsubsection*{Lighting}

Lighting in Unity utilizes the light components, which determine what type of light it is, if it should cast shadows, what color, and how bright the emitted light should be, etc. Furthermore, lights can be either dynamic or static, which have to be set in the editor.

Dynamic lights are, by nature, light objects that are going to change their transformation or their light settings. It is calculated while the game is running and will require additional computing power. Because it is done in real-time, it lacks the ability to simulate secondary bounces\footnote{It is possible to use Enlighten Real Time Global Illumination, which can simulate this effect, however, at the cost of performance and still requires additional data to be baked.}. \cite{unityDocs-realtime-lights} 

Static lights are lights that are immutable in both their transformation and settings. These lights have to go through a process called lightmap baking in Unity, which is precalculating bounces of light rays throughout the scene against other static objects. The advantage of this process is significantly lower performance requirements and more physically accurate results. However, it lacks the ability to be changed in real time, and the baking process can take a long time, depending on the scene's complexity. \cite{unityDocs-baked-lights}

The lightmap baking, however, only gives the appearance to the static objects, while dynamic objects would not benefit from this baking. For this purpose, light probes are used, which are points in space that store the lighting information as if a sphere was being lit in its place \cite{unityDocs-lightprobes}. Usually, a scene would contain light probes at positions where dynamic objects could get and places where the lighting could change drastically. The placement of such probes is crucial, as incorrect placement can easily result in lights leaking through walls since an object would gather probes from both sides of the wall, which can be seen in an example scene in Figure~\ref{fig:lightprobe-leak}. The object would then choose the nearest light probes based on their origin and calculate the average of the lighting information from said probes.

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{assets/light-leak2.png}
  \caption{An example of a light leak due to incorrect placement of light probes. The individual spheres represent the light probes, which are static, along with the environment, including the room itself. The cube is a dynamic object and colored white; however, it samples more probes that are inside the lit room instead of the outside shaded area, causing it to appear glowing.}
  \label{fig:lightprobe-leak}
\end{figure}

Additionally, Reflection Probes are used to bake in the reflection of objects by storing a full cubical view of the probe in its place \cite{unityDocs-refprobes}. This stage of baking has to be done after the other baking process. Unity also supports Box Reflection Probes, which attempt to project the baked reflection probe in a cube instead of a sphere. It is especially useful in areas of a box shape, as the reflections will be more visually pleasing and accurate regarding its walls. \cite{unityDocs-refprobes-advanced}

\subsubsection*{Forward+}

Forward+ is a rendering path extending the Forward rendering path. It features the ability to have multiple lights in the scene without significantly reducing the performance. This is achieved by effectively culling the lights that do not contribute to the shaded pixel, thus reducing the performance cost of rendering lights that are not visible to the camera. It, however, has the additional hardware requirement of compute shaders, which should have been available to all devices that are supposed to run this thesis at a reasonable performance. The culling of lights is done in tiles instead of per-pixel \cite{forwardplus}.

The Universal Rendering Pipeline, since version 14 shipped with Unity 2022 versions, has implemented the Forward+ rendering path. Apart from the flexibility of having many lights in the scene, objects can also blend between two or more reflection probes \cite{unityDocs-urp-forwardplus}. This rendering path is not fully supported in VR and results in artifacts, especially near light falloffs \cite{unityForums-fp-urp}. An example of such artifact can be seen in Figure~\ref{fig:forwardpartifact}. Although it may be distracting, this thesis does not use a lot of dynamic light sources and relies mostly on baked lighting. Forward+ in this thesis is mostly for its ability to use more than one reflection probe per object.

\begin{figure}
  \centering
  \includegraphics[width=0.6\textwidth]{assets/forwardp-artifact.png}
  \caption{An example of such artifacts of Forward+ in VR, where jagged edges around the window are visible.}
  \label{fig:forwardpartifact}
\end{figure}

\subsection{Physics System}

Unity internally uses the NVIDIA PhysX engine for physics interactions in 3D and 2D \cite{unityDocs-physics}. This system is used not only for simulating the physics of various rigid bodies in the scene but also for the player's movement and interaction with the physical world. It could be labeled the ``core of this project'' for this thesis.

To ensure an object is considered in the physics engine, a kinematic RigidBody component or a collider of any type has to be added to it. However, doing so will make the system consider it a static object. To make it properly dynamic\footnote{It is possible to update the object's transformation; however, that may result in unstable physics simulation.}, a non-kinematic RigidBody component must be added. \cite{unityDocs-rigidbodies}

\pagebreak
Every object in the scene can have a Collider, which tells the physics engine the shape of the given object. Usually, it is a simplified model of the displayed one for performance reasons. By default, it is possible to choose one of these colliders \cite{unityDocs-collider-shapes}:

\begin{description}
  \item[BoxCollider]
    A box-shaped collider is defined by a center position offset and dimensions of said box.
  \item[SphereCollider]
    A spherical collider is defined by a center position offset and radius of the sphere.
  \item[CapsuleCollider]
    Capsule\footnote{Could be thought of as two spheres with a cylinder connecting them} collider, defined by a center position offset, radius, and height of the capsule.
  \item[TerrainCollider]
    Special collider used only on Terrain objects.
  \item[WheelCollider]
    Special collider for vehicles and their wheels.
  \item[MeshCollider]
    Allows the collider to take the shape of any mesh. For non-kinematic rigid bodies, it is required to be a convex shape with a maximum of 256 polygons. Static mesh colliders or kinematic rigid bodies do not have this limitation.
\end{description}

There are cases where the built-in colliders are not good enough to describe the shape of our rigid body mesh (e.g., trash can), for which Unity allows the creation of ``compound colliders'' made of multiple colliders belonging to the same hierarchy \cite{unityDocs-colliders}. An example of such a structure can be seen in Figure~\ref{fig:compound-collider}.

\begin{figure}[h]
  \centering
  \begin{minipage}[c]{.5\textwidth}
    \dirtree{%
      .1 simpleChair \DTcomment{RigidBody, MeshRenderer}.
      .2 leg.0 \DTcomment{BoxCollider}.
      .2 leg.1 \DTcomment{BoxCollider}.
      .2 leg.2 \DTcomment{BoxCollider}.
      .2 leg.3 \DTcomment{BoxCollider}.
      .2 seat \DTcomment{BoxCollider}.
      .2 backrest \DTcomment{BoxCollider}.
    }
  \end{minipage}
  \begin{minipage}[c]{.275\textwidth}
    \includegraphics[width=\textwidth]{assets/chair-compound-example.jpg}
  \end{minipage}

  \caption{Example of a compound collider made out of box colliders, with \texttt{simpleChair} being the visible mesh and the colliders being appended to objects that are parented to the said chair}
  \label{fig:compound-collider}
\end{figure}

Interaction with the rigid bodies is done using forces, which can be applied from code, with an example snippet shown in Code listing~\ref{fig:snippet-force}. Another way to do so would be using joints, which are components that can be used to define the relationship between different rigid bodies. The number of joints one object can have is not limited. It is even possible to use different types of joints on the same object simultaneously. There are various types of joints that Unity provides~\cite{unityDocs-joints}:

\pagebreak
\begin{description}
  \item[FixedJoint] Allows one joint to stick to another, acting as if they belong to each other.
  \item[HingeJoint] Allows one object to be connected to another using a hinge. 
  \item[SpringJoint] Allows one object to be connected to another using a spring.
  \item[CharacterJoint] Allows to simulate ball and socket joints, commonly used for ragdoll characters on their knees and shoulders.
  \item[ConfigurableJoint] Allows the developer to adjust the joint in any degree of freedom.
\end{description}

\lstdefinestyle{single-code}{
  float=tp,
  floatplacement=tbp,
}
\begin{lstlisting}[label={fig:snippet-force},style=single-code,caption={Component that applies a constant force to a given object as long it is active.}]
  using UnityEngine;
  
  private new Rigidbody rigidbody;
  void FixedUpdate(){
    rigidbody.AddForce(new Vector3(0.0f, 1.0f, 0.0f) * Time.fixedDeltaTime);
  }
\end{lstlisting}

For the player to traverse the physical world, there is a special component for this specific case called the \texttt{CharacterController}, which implements the ``collide and slide'' technique often used in games. This component also applies gravity and handles collision detection \cite{nvidia-charcontr,unityDocs-charcontr}.

The ``collide and slide'' algorithm describes the behavior of a situation where the player wants to move in a way that would result in a collision with an obstacle, in which the sliding comes into play, and the player would slide to the side instead of stopping them. A simple diagram of how this algorithm works is demonstrated in the Figure~\ref{fig:collide-and-slide}.

 \begin{figure}[ht]
   \centering
   \includegraphics[width=0.5\textwidth]{assets/colandslide.png}
   \caption{An example of the collide and slide algorithm, where the green circle represents the player and the faded circle the resulting position. The arrows represent the various vectors related, with blue being the vector the player wishes to move with, red being the normal vector of the collision the player would encounter, and purple being the resulting move the player would perform.}
   \label{fig:collide-and-slide}
 \end{figure}

\section{OpenXR}
\label{section:openxr}

OpenXR is an open standard for creating games and applications for both virtual and augmented reality. OpenXR is developed by the Khronos Group \cite{openxr}, which has a rich history of standards that are still used to this day (e.g., OpenGL and Vulkan rendering APIs) \cite{khoronos-standards}. The specifications of the standard not only include technical information about how the individual components need to interact with each other, but it also contains the standard of what controls the controllers should have and even the facial structure for the potential facial and eye tracking features, among other topics.

While it is possible to use SteamVR, a higher level of abstraction, since most headsets nowadays have a way to bridge their API and this one together, it is an additional dependency that is not required for this use-case. Additionally, OpenXR was chosen over SteamVR since it allows the game to be played on many VR/XR headsets and even on different platforms, not limited to PC, with little to no additional work by the developer or the player.

Most controllers provided with an OpenXR-compliant VR headset usually consist of three touch zones: the top, the trigger, and the grip of the controller \cite{openxr-interaction-profile-paths}. The top of the controller usually consists of buttons and a joystick, so it is possible to detect when the thumb is located here either by detecting when the components are pressed or possibly when touched\footnote{According to the OpenXR specification, controllers are not obligated to report touch.}. The trigger is where the index finger is located, and it is possible to detect how deep the trigger is being pressed. The last zone is the grip, which is located below the trigger, from which it is possible to determine how firm the grip is. \cite{openxr-subpaths} % On some headset's controllers, it is implemented similarly to the trigger (eg. Meta Quest 2), however, some have it implemented using pressure sensors.

\section{Blender}

Blender is a free and open-source 3D manipulation tool licensed under the GPL license, officially available for Windows, Mac OS, and Linux \cite{blender-about}. It allows the user to create 3D models, texture them, and render them into images or animation. The user can also create physics simulations of either rigid or soft bodies or even liquid physics simulations \cite{blender-features}. The version the author used was 3.6 since it was the current LTS version at the time of writing. For this thesis, it was used to create models and textures as assets for the Unity game and to optimize models that were not in an ideal state. 

% \begin{figure}[ht]
%   \_missingfigure{Blender Default UI}
%   \caption{Blender's default user interface, with the default scene open, consisting of a cube, a point light, and a camera.}
%   \label{fig:blender-ui}
% \end{figure}

The classic installation of Blender comes with two rendering engines: EEVEE\footnote{Extra Easy Virtual Environment Engine} and Cycles, which are rasterization and path-tracing engines, respectively \cite{blender-rendering-eevee,blender-rendering-intro}. The former requires a GPU and is ideal for use cases, where one wishes to quickly preview how the model would look or how it would look in a game, given that it renders in a similar way as most game engines. The latter can be run without a GPU; however, having a compatible GPU can significantly reduce its processing time \cite{blender-rendering-cycles-gpu}. It is mainly used for scenarios where photo-realism is desired as it simulates individual light rays and their bounces in the scene and to bake textures for models \cite{blender-rendering-cycles-baking}. Both of them define the materials using Shader Nodes and allow a wide range of customization to be done to the final look. These nodes are then interconnected into a tree structure, which is called a Shader Node Tree \cite{blender-shader-editor}.

For the PBR workflow found in many industries, the Principled BSDF\footnote{Bidirectional Scattering Distribution Function. A mathematical formula describing light's behavior.} is often used \cite{blender-principled-bsdf}. As mentioned before, Blender, and subsequently this node, follow the Metallic workflow. If, however, the artist demands, a Specular BSDF node is provided for the Specular workflow \cite{blender-specular-bsdf}. If they wish to deviate from the standard and create their materials, it is possible to do so by combining different BSDF nodes, such as the Diffuse BSDF, Glossy BSDF, and others. Within these Shader Nodes, the artist can even create their textures using mathematical operations and different types of noise generators (e.g., White Noise or Voronoi) to provide them with some entropy. These textures are commonly referred to as procedural textures and need to be baked in order to bring them into different software \cite{blender-rendering-cycles-baking}.

Modeling can be done by the traditional box modeling method or by sculpting, both of which are considered ``destructive workflows,'' meaning that it is not trivial to return to the previous iterations of the model after an edit has been made. In Blender, \textit{modifiers} can be added to models, which, as their name suggests, modify the model in a non-destructive way. It allows the artist to change its parameters later on or even remove it \cite{blender-modifiers-intro}. Out of the box, the artist would be supplied with modifiers such as Bevel, Subsurface Subdivision, and many others. If they want to create their own, then Geometry Nodes, modifiers the user can customize, can be utilized \cite{blender-geo-intro}. These nodes, similar to Shader Nodes, modify the geometry with whatever parameters and methods the artist desires, and their usage can be from quickly instancing multiple objects in a set pattern to creating full-fledged buildings. While this is great for editing as it gives the artist freedom and flexibility, it cannot be used in other software directly and needs to be converted into a classic mesh by applying the modifiers, akin to the Shader Nodes.

A quick side note that most production pipelines in the video game industry often use Autodesk FBX, which is the proprietary format for storing 3D models and animations, and due to Blender's license, they are unable to use the official FBX SDK\footnote{Software Development Kit -- It allows the software to be built upon or to be integrated as part of other.} in Blender. The format has been reverse-engineered to work around this issue, and an import/export addon was made for~it.~\cite{blender-fbx-blog} While its features are stable and sufficient enough for most cases, there are caveats in certain areas that require artists to work around said issues. One of those issues, specifically regarding the export, is further mentioned in Chapter~\ref{chapter:asset-creation}.
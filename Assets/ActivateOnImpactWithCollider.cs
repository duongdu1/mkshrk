using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

public class ActivateOnImpactWithCollider : MonoBehaviour
{
    public bool triggered;
    public bool triggerOnce;
    public UnityEvent action;

    private HashSet<Collider> _colliders = new HashSet<Collider>();
    public List<Collider> colliders = new List<Collider>();
    public bool addParentsOfColliders = false;
    public bool addChildrenOfColliders = false;

    public float yThreshold = 0.02f;

    void Awake(){
        _colliders.Concat(colliders);
        if(addParentsOfColliders || addChildrenOfColliders){
            foreach(Collider col in colliders){
                if(addParentsOfColliders)
                    foreach(Collider newc in col.GetComponentsInParent<Collider>()){
                        _colliders.Add(newc);
                    }
                if(addChildrenOfColliders)
                    foreach(Collider newc in col.GetComponentsInChildren<Collider>()){
                        _colliders.Add(newc);
                    }
            }
        }
        Debug.Log(_colliders.Count);
    }

    void OnCollisionEnter(Collision col){
        if((!triggerOnce || !triggered) && !_colliders.Contains(col.collider) && (col.GetContact(0).point.y - transform.position.y <= yThreshold)){
            action.Invoke();
            triggered = true;
        }
    }
}

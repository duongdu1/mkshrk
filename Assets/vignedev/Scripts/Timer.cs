using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour{
    [SerializeField] float _timer = 0.0f;

    void Update(){
        _timer = Time.realtimeSinceStartup;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class UpdateLineRendererTransforms : MonoBehaviour{
    LineRenderer liner;

    public bool useFixedUpdates = false;
    public bool useThisAsRoot = true;
    public List<Transform> transforms = new List<Transform>();

    void Awake() {
        if(!liner) liner = GetComponent<LineRenderer>();

        UpdatePositions();
    }

    void Update() { if(useFixedUpdates) UpdatePositions(); }
    void FixedUpdate() { if(!useFixedUpdates) UpdatePositions(); }

    void UpdatePositions(){
        List<Vector3> positions = new List<Vector3>();;
        if(useThisAsRoot)
            positions.Add(transform.position);

        foreach(Transform t in transforms)
            positions.Add(t.position);

        liner.SetPositions(positions.ToArray());
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchBucketRefiller : MonoBehaviour{
    void OnTriggerStay(Collider col){
        BucketWaterLevelController level = col.gameObject.GetComponentInParent<BucketWaterLevelController>();
        if(!level) return;
        level.fillRate = 1.0f;
    }
}

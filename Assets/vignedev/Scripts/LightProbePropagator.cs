#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.IMGUI.Controls;
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(LightProbeGroup))]
class LightProbePropagator : MonoBehaviour {
	static Vector3Int[] directions = new Vector3Int[] {
		Vector3Int.back,
		Vector3Int.down,
		Vector3Int.left,
		Vector3Int.forward,
		Vector3Int.up,
		Vector3Int.right,
	};

	// map = [Vector3Int * cubeSize] = map
	public Dictionary<Vector3Int, int> map = new Dictionary<Vector3Int, int>();
	public float cubeSize;
	public int initialHealth = 2;

	public int stepLimit = 100;

	public Bounds boundary = new Bounds(Vector3.zero, Vector3.one);

	public List<Vector3> probes = new List<Vector3>();
	LightProbeGroup lpg;

	public bool raincastMode = false;

	void Awake(){
		lpg = GetComponent<LightProbeGroup>();
	}

	Collider[] tempColliders = new Collider[24];
	bool CheckPosition(Vector3Int pos){
		return CheckPosition(pos.x, pos.y, pos.z);
	}
	bool CheckPosition(int x, int y, int z){
		Vector3Int mapKey = new Vector3Int(x, y, z);

		return Physics.CheckBox(
			new Vector3(x, y, z) * cubeSize + (transform.position),
			Vector3.one * (cubeSize / 2.0f),
			Quaternion.identity
		);
	}

	public void Propagate(){
		#if UNITY_EDITOR
		map.Clear(); probes.Clear();
		
		if(raincastMode) RaincastPropagation();
		else NaiveTraversePropagation();

		lpg.probePositions = probes.ToArray();
		#else
			Debug.LogWarning("Propagate was called outside of editor, why is that?");
		#endif
	}

	#if UNITY_EDITOR
	private void NaiveTraversePropagation(){
		Queue<Vector3Int> queue = new Queue<Vector3Int>();
		queue.Enqueue(Vector3Int.zero);

		while(queue.Count != 0){
			Vector3Int index = queue.Dequeue();

			// new node, create and set initial
			if(!map.ContainsKey(index)) map[index] = initialHealth;
			else if (map[index] == 0) continue;
			else continue;

			if(probes.Count >= stepLimit) break;

			bool IsObstructed = CheckPosition(index);
			bool AddSurrounding = false;

			// if it is not obstructed, create a new probe and add surrounding
			if(!IsObstructed){
				probes.Add(((Vector3)index) * cubeSize);
				AddSurrounding = true;
			}else{ // obstructed, reduce health and continue
				map[index] -= 1;
				if(map[index] > 0) AddSurrounding = true;
			}

			// Add surrounding if they have not been calculated yet
			if(AddSurrounding){
				foreach(var direction in directions){
					Vector3Int newIndex = index + direction;
					if(map.ContainsKey(newIndex)){
						continue; // ignore already passed
					}
					if(!boundary.Contains(((Vector3)newIndex) * cubeSize)) continue; // out of bounds, ignore

					queue.Enqueue(newIndex);
				}
			}
		}
	}

	private void RaincastPropagation(){
		if(cubeSize == 0){
			Debug.LogError("cubeSize cannot be 0!");
			return;
		}
		int count_x = (int)(boundary.extents.x / cubeSize);
		int count_y = (int)(boundary.extents.z / cubeSize);

		float width = (boundary.extents.x / count_x);
		float height = (boundary.extents.z / count_y);

		Debug.Log($"{width} x {height}");

		Vector3 origin = boundary.center + transform.up * boundary.extents.y;
		Vector3 offset_x = transform.right * width;
		Vector3 offset_y = transform.forward * height;

		for(int x = 0; x < count_x; ++x){
			for(int y = 0; y < count_y; ++y){
				if(x == 0 && y == 0){
					probes.Add(origin);
				}else{
					CastFromTop(origin + x * offset_x + y * offset_y);
					if(x != 0) CastFromTop(origin + -x * offset_x + y * offset_y);
					if(y != 0) CastFromTop(origin + x * offset_x + -y * offset_y);
					if(x != 0 && y != 0) CastFromTop(origin + -x * offset_x + -y * offset_y);
				}
			}
		}

		void CastFromTop(Vector3 pos){
			probes.Add(pos);

			Vector3 wpos = transform.TransformPoint(pos);
			Vector3 offset = Vector3.down * cubeSize;
			
			RaycastHit hit;
			if(Physics.Raycast(wpos, -transform.up, out hit, Mathf.Infinity)){
				int count = Mathf.CeilToInt(hit.distance / cubeSize);
				for(int i = 1; i < count; ++i){
					probes.Add(pos + offset * i);
				}
			}
		}
	}
	#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(LightProbePropagator))]
public class LightProbePropagatorEditor : Editor {
	private BoxBoundsHandle handles = new BoxBoundsHandle();

	protected virtual void OnSceneGUI(){
		LightProbePropagator source = (LightProbePropagator)target;

		handles.center = source.boundary.center + source.transform.position;
		handles.size = source.boundary.size;
		

		EditorGUI.BeginChangeCheck();
		handles.DrawHandle();
		if (EditorGUI.EndChangeCheck()){
            Undo.RecordObject(source, "Probe Boundary Changed");

            source.boundary.center = handles.center - source.transform.position;
            source.boundary.size = handles.size;
        }
	}

	override public void OnInspectorGUI(){
		DrawDefaultInspector();
		LightProbePropagator source = (LightProbePropagator)target;

		if(GUILayout.Button("Propagate")){
			source.Propagate();
		}
	}
}
#endif
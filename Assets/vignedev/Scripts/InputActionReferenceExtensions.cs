using UnityEngine.InputSystem;

public static class InputActionReferenceExtensions {
    public static bool IsReadable(this InputActionReference actionReference){
        return actionReference != null &&
            actionReference.action != null &&
            actionReference.action.enabled &&
            actionReference.action.activeControl != null;
    }

    public static TValue ReadOrDefault<TValue>(this InputActionReference actionReference, TValue def) where TValue : struct {
        return actionReference.IsReadable() ? actionReference.action.ReadValue<TValue>() : def;
    }
}
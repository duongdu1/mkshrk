using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Audio;

public class AudioManager : SingletonBehaviour<AudioManager>{
    public enum MixerOutput {
        General,
        Ambience,
        Footstep,
        Impact
    };

    private AudioManager Initialize(){
        _masterMixer = Addressables.LoadAssetAsync<AudioMixerGroup>("Assets/vignedev/Audio/Mixers/MasterMixer.mixer[Master]").WaitForCompletion();
        _ambienceMixer = Addressables.LoadAssetAsync<AudioMixerGroup>("Assets/vignedev/Audio/Mixers/MasterMixer.mixer[Ambience]").WaitForCompletion();
        _footstepMixer = Addressables.LoadAssetAsync<AudioMixerGroup>("Assets/vignedev/Audio/Mixers/MasterMixer.mixer[Footstep]").WaitForCompletion();
        _impactMixer = Addressables.LoadAssetAsync<AudioMixerGroup>("Assets/vignedev/Audio/Mixers/MasterMixer.mixer[Impact]").WaitForCompletion();
        return this;
    }

    [SerializeField] AudioMixerGroup _masterMixer;
    public AudioMixerGroup masterMixer => _masterMixer;
    [SerializeField] AudioMixerGroup _ambienceMixer;
    public AudioMixerGroup ambienceMixer => _ambienceMixer;
    [SerializeField] AudioMixerGroup _footstepMixer;
    public AudioMixerGroup footstepMixer => _footstepMixer;
    [SerializeField] AudioMixerGroup _impactMixer;
    public AudioMixerGroup impactMixer => _impactMixer;

    private AudioMixerGroup GetMixerGroup(MixerOutput value){
        switch(value){
            default:
            case MixerOutput.General:
                return _masterMixer;
            case MixerOutput.Ambience:
                return _ambienceMixer;
            case MixerOutput.Footstep:
                return _footstepMixer;
            case MixerOutput.Impact:
                return _impactMixer;
        }
    }

    public void PlayClipAtPoint(AudioClip clip, Vector3 position, float volume = 1.0f, float maxDistance = 500.0f){
        PlayClipAtPoint(clip, position, MixerOutput.General, volume, maxDistance);
    }
    public void PlayClipAtPoint(AudioClip clip, Vector3 position, MixerOutput mixer, float volume = 1.0f, float maxDistance = 500.0f){
        GameObject tempObject = new GameObject("Temporary AudioSource");
        tempObject.transform.position = position;
        AudioSource source = tempObject.AddComponent<AudioSource>();
        source.clip = clip;
        source.volume = volume;
        source.outputAudioMixerGroup = GetMixerGroup(mixer);
        source.spatialBlend = 1.0f;
        source.maxDistance = maxDistance;

        source.Play();
        StartCoroutine(DestroyOnAudioStop(source));
    }

    public void PlayRandomClip(AudioClip[] pool, Vector3 position, float volume = 1.0f, float maxDistance = 500.0f){
        PlayRandomClip(pool, position, MixerOutput.General, volume, maxDistance);
    }
    public void PlayRandomClip(AudioClip[] pool, Vector3 position, MixerOutput mixer, float volume = 1.0f, float maxDistance = 500.0f){
        PlayClipAtPoint(pool[Random.Range(0, pool.Length)], position, mixer, volume, maxDistance);
    }

    public IEnumerator DestroyOnAudioStop(AudioSource target){
        yield return new WaitUntil(() => !target || target.isPlaying == false);
        if(target && target.gameObject) Destroy(target.gameObject);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysVisibleBoxCollider : MonoBehaviour{
    [SerializeField] Color color = new Color(0, 1, 0, 0.5f);
    [SerializeField] bool children = false;

    void Awake(){
        Debug.LogWarning("AlwaysVisibleBoxCollider is enabled, it should be removed if not needed in runtime!", this);
    }

    void OnDrawGizmos(){
        Gizmos.color = color;

        BoxCollider[] colliders = children ? GetComponentsInChildren<BoxCollider>() : GetComponents<BoxCollider>();
		foreach(BoxCollider col in colliders){
            Gizmos.matrix = col.transform.localToWorldMatrix;
            Gizmos.DrawCube(col.center, col.size);
        }
    }
}
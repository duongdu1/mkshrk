using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEventExpose : MonoBehaviour{
    [SerializeField] UnityEvent onTriggerEnter;
    [SerializeField] UnityEvent onTriggerExit;

    public float delay = 0.0f;
    public bool wasTriggered;
    public bool triggerOnce;

    public List<GameObject> ignoreList = new List<GameObject>();

    IEnumerator DelayedTrigger(UnityEvent action){
        yield return new WaitForSeconds(delay);
        action.Invoke();
    }

    void Trigger(UnityEvent action){
        if(Mathf.Abs(delay) <= Mathf.Epsilon) {action.Invoke(); wasTriggered = true;}
        else StartCoroutine(DelayedTrigger(action));
    }

    void OnTriggerEnter(Collider col){
        if(((triggerOnce && !wasTriggered) || !triggerOnce) && !ignoreList.Contains(col.gameObject)){
            wasTriggered = true;
            Trigger(onTriggerEnter);
        }
    }
    void OnTriggerExit(Collider col){
        if(((triggerOnce && !wasTriggered) || !triggerOnce) && !ignoreList.Contains(col.gameObject)){
            wasTriggered = true;
            Trigger(onTriggerExit);
        }
    }
}

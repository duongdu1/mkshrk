using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.OpenXR;
using System.IO;
using System;
using UnityEngine.SceneManagement;

public class DebugCameraView : MonoBehaviour{
    public float mouseSensitivity = 1.0f;
    public float moveSpeed = 1.0f;
    public float speedMultiplier = 2.0f;
    public float speedDivisor = 4.0f;
    public float scrollSpeed = 1.0f;
    public Vector2 fovLimit = new Vector2(10f, 160f);
    new Camera camera;

    public GameObject flashlightObject;
    public int supersize = 1;

    void Awake(){
        if(!camera) camera = GetComponent<Camera>();
    }

    void Start(){
        // Disable myself if this is a playable scene
        if(GameManager.instance.player){
#if !UNITY_EDITOR_LINUX
            if(OpenXRRuntime.name == "Unity Mock Runtime"){
                GameManager.instance.player.gameObject.SetActive(false);
                gameObject.SetActive(true);
            }else{
                GameManager.instance.player.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
#else
            GameManager.instance.player.gameObject.SetActive(false);
            gameObject.SetActive(true);
#endif
        }
    }

    void Update(){
        Cursor.lockState = (Mouse.current.rightButton.isPressed) ? CursorLockMode.Locked : CursorLockMode.None;

        if(Mouse.current.rightButton.isPressed){
            Vector3 angles = transform.eulerAngles;
            Vector2 delta = Mouse.current.delta.ReadValue() * Time.deltaTime * mouseSensitivity;
            float new_angle = angles.x - delta.y;
            angles.y += delta.x;
            angles.z = 0;

            if(!(new_angle >= 90f && new_angle <= 270)) angles.x = new_angle;

            transform.eulerAngles = angles;

            Vector3 movement = new Vector3();
            if(Keyboard.current.wKey.isPressed) movement.z += 1;
            if(Keyboard.current.aKey.isPressed) movement.x -= 1;
            if(Keyboard.current.sKey.isPressed) movement.z -= 1;
            if(Keyboard.current.dKey.isPressed) movement.x += 1;
            if(Keyboard.current.qKey.isPressed) movement.y -= 1;
            if(Keyboard.current.eKey.isPressed) movement.y += 1;

            if(Keyboard.current.shiftKey.isPressed) movement *= speedMultiplier;
            else if(Keyboard.current.altKey.isPressed) movement /= speedDivisor;
            transform.position += transform.TransformDirection(movement * moveSpeed * Time.deltaTime);

            Vector2 scroll = Mouse.current.scroll.ReadValue();
            camera.fieldOfView = Mathf.Clamp(camera.fieldOfView + scroll.y * scrollSpeed * Time.deltaTime, fovLimit.x, fovLimit.y);
        }

        if(Keyboard.current.f2Key.wasPressedThisFrame){
            string filepath = Path.Join(Path.GetTempPath(), $"{SceneManager.GetActiveScene().name}_{DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff")}.png");
            ScreenCapture.CaptureScreenshot(filepath, supersize);
            Debug.Log(filepath);
        }

        if(Keyboard.current.fKey.wasPressedThisFrame){
            flashlightObject.SetActive(!flashlightObject.activeSelf);
        }
    }
}

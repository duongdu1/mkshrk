using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class RandomizeMaterialColor : MonoBehaviour{
    new Renderer renderer;

    public string fieldName;

    void Awake(){
        if(!renderer) renderer = GetComponent<Renderer>();
    }

    void Start(){
        renderer.material.color = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 1.0f, 1.0f);
    }
}

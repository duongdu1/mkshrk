using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR.OpenXR;

public class MoveUsingGamepad : MonoBehaviour {
    [SerializeField] TrackedPoseDriver driver;

    void Awake(){
        if(!driver) driver = GetComponent<TrackedPoseDriver>();
#if UNITY_EDITOR_LINUX
    driver.enabled = false;
#else
    if(OpenXRRuntime.name != "Unity Mock Runtime") enabled = false;
    else driver.enabled = false;
#endif
    }

    Vector2 GetLookVector(){
        if(Gamepad.current != null) return Gamepad.current.rightStick.value;
        return Mouse.current.rightButton.isPressed ? Mouse.current.delta.value : Vector2.zero;
    }

    Vector2 GetPhysicalOffsetVector() {
        if(Gamepad.current != null) return Gamepad.current.dpad.value;

        return new Vector2(
            - Keyboard.current.leftArrowKey.value + Keyboard.current.rightArrowKey.value,
            + Keyboard.current.upArrowKey.value - Keyboard.current.downArrowKey.value
        );
    }

    void Update() {
        Vector2 rightStick = GetLookVector();
        Vector2 dpad = GetPhysicalOffsetVector();
        float leftTrigger = Gamepad.current?.leftTrigger.value ?? Keyboard.current.qKey.value;
        float rightTrigger = Gamepad.current?.rightTrigger.value ?? Keyboard.current.eKey.value;
        
        Vector3 copy = transform.localEulerAngles;
        copy.x -= rightStick.y;
        copy.y += rightStick.x;
        copy.z = 0;
        transform.localEulerAngles = copy;

        // dpad.x, -leftTrigger + rightTrigger, dpad.y
        Vector3 flat_forward = transform.forward;
        flat_forward.Scale(new Vector3(1.0f, 0.0f, 1.0f));
        flat_forward.Normalize();

        Vector3 flat_right = transform.right;
        flat_right.Scale(new Vector3(1.0f, 0.0f, 1.0f));
        flat_right.Normalize();

        transform.Translate((flat_forward * dpad.y + flat_right * dpad.x + Vector3.up * (-leftTrigger + rightTrigger)) * Time.deltaTime, Space.World);
    }
}

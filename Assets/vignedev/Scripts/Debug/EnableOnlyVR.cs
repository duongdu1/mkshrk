using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnableOnlyVR : MonoBehaviour{
    void Start(){
        gameObject.SetActive(UnityEngine.XR.XRSettings.isDeviceActive);
    }
}

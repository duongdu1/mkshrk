using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MoveUsingAxis2DInput : MonoBehaviour{
    public InputActionReference action;
    public InputActionReference zoomAction;
    public Vector3 speed = Vector3.one;
    public Space space;

    void Update(){
        Vector2 axis = action.ReadOrDefault<Vector2>(Vector2.zero);
        Vector3 realSpeed = speed * Time.deltaTime;
        transform.Translate(axis.x * realSpeed.x, axis.y * realSpeed.y, realSpeed.z * zoomAction.ReadOrDefault<float>(0.0f), space);
        // Vector3 copy = transform.localEulerAngles; copy.z = 0;
        // copy.x += axis.y * speed.y; copy.y += axis.x * speed.x; copy.z = 0;
        // transform.localEulerAngles = copy;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateUsingAxis2DInput : MonoBehaviour{
    public InputActionReference action;
    public Vector2 speed = Vector2.one;
    public Space space;

    void Update(){
        Vector2 axis = action.ReadOrDefault<Vector2>(Vector2.zero);
        Vector2 realSpeed = speed * Time.deltaTime;
        transform.Rotate(axis.y * realSpeed.y, axis.x * realSpeed.x, 0, space);
        // Vector3 copy = transform.localEulerAngles; copy.z = 0;
        // copy.x += axis.y * speed.y; copy.y += axis.x * speed.x; copy.z = 0;
        // transform.localEulerAngles = copy;
    }
}

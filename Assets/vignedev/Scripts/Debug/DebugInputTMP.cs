using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using System;
using System.Text;

public class DebugInputTMP : MonoBehaviour{
    [Serializable]
    class StringAndInputAction {
        public string name;
        public InputActionReference action;
    }

    TMP_Text text;
    StringBuilder builder = new StringBuilder();

    [SerializeField] StringAndInputAction[] items;

    void Awake(){
        text = GetComponent<TMP_Text>();
    }

    void Update(){
        builder.Clear();
        foreach(var item in items){
            builder.AppendFormat("{0} {1}\n", item.action.ReadOrDefault<float>(0.0f).ToString("N2"), item.name);
        }
        text.text = builder.ToString();
    }
}

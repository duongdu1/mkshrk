using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using TMPro;
using System.Runtime.CompilerServices;

public partial class WorldSpaceIMGUI : MonoBehaviour {
    // Singleton structure (TODO: maybe create a generization of it?)
    private static WorldSpaceIMGUI _instance;
    private static WorldSpaceIMGUI instance {
        get {
            if(_instance == null){
                _instance = FindObjectOfType<WorldSpaceIMGUI>();
            }
            if(_instance == null){
                GameObject holder = new GameObject("WorldSpaceIMGUI");
                _instance = holder.AddComponent<WorldSpaceIMGUI>();
            }
            return _instance;
        }
    }

    // Key is the "hash ID" and its corresponding text object
    Dictionary<int, (Canvas, TMP_Text)> items = new Dictionary<int, (Canvas, TMP_Text)>();
    // List of IDs to be removed at the end of LateUpdate
    HashSet<int> inactiveItems = new HashSet<int>();
    // Prefab of the expected template
    [SerializeField] GameObject template;

    // Attempt to fetch the prefab from the marked addressable
    void Awake() {
        if(template == null){
            var loader = Addressables.LoadAssetAsync<GameObject>("Assets/vignedev/Prefabs/BillboardIMGUI.prefab");
            template = loader.WaitForCompletion();
        }
    }

    private (Canvas, TMP_Text) NewTuple(){
        GameObject obj = Instantiate(template, Vector3.zero, Quaternion.identity);
        Canvas canvas = obj.GetComponent<Canvas>();
        TMP_Text text = obj.GetComponentInChildren<TMP_Text>();
        return (canvas, text);
    }

    private int GetCombinedHashCode(string source, int line){
        return source.GetHashCode() + line.GetHashCode();
    }

    private (Canvas, TMP_Text) GetOrCreateText(int hashCode){
        (Canvas, TMP_Text) pair;
        if(items.TryGetValue(hashCode, out pair)){
            inactiveItems.Remove(hashCode);
        }else{
            pair = NewTuple();
            items[hashCode] = pair;
        }
        return pair;
    }

    // Creates a text that is drawn at a specific location
    private void CreateText(Vector3 position, string message, int size = 4, [CallerFilePath] string sourceFile = "", [CallerLineNumber] int sourceLine = 0){
        int hashCode = GetCombinedHashCode(sourceFile, sourceLine);
        var (canvas, text) = GetOrCreateText(hashCode);

        canvas.transform.position = position;
        text.text = message;
        text.fontSize = size;
    }

    // Creates a text that is at a following a specific transform
    private void CreateText(Transform parent, string message, int size = 4, [CallerFilePath] string sourceFile = "", [CallerLineNumber] int sourceLine = 0){
        int hashCode = GetCombinedHashCode(sourceFile, sourceLine);
        var (canvas, text) = GetOrCreateText(hashCode);

        canvas.transform.SetParent(parent, false);
        text.text = message;
        text.fontSize = size;
    }

    // Cleanup for inactive items
    private void LateUpdate() {
        // Remove items from the inactiveItems list
        foreach(int hashCode in inactiveItems){
            var (canvas, text) = items[hashCode];
            items.Remove(hashCode);
            DestroyImmediate(canvas.gameObject);
        }

        // Clean slate, repopulate it with items' keys
        inactiveItems.Clear();
        inactiveItems.UnionWith(items.Keys);
    }
}

// Static mirrors
public partial class WorldSpaceIMGUI : MonoBehaviour {
    public static void Text(Vector3 position, string message, string index = "", int size = 4, [CallerFilePath] string sourceFile = "", [CallerLineNumber] int sourceLine = 0){
        instance.CreateText(position, message, size, sourceFile + index, sourceLine);
    }
    public static void Text(Transform parent, string message, string index = "", int size = 4, [CallerFilePath] string sourceFile = "", [CallerLineNumber] int sourceLine = 0){
        instance.CreateText(parent, message, size, sourceFile + index, sourceLine);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DebugPhysicsGrab : MonoBehaviour{
    [SerializeField] Rigidbody anchor;

    enum JointType {
        Spring,
        Fixed,
        Configurable
    }

    [SerializeField] Joint joint;
    [SerializeField] JointType type = JointType.Spring;

    [SerializeField] float speed = 0.01f;
    [SerializeField] float spring = 1000f;
    [SerializeField] float damper = 2000f;

    JointDrive positionDrive;
    JointDrive angularDrive;
    [SerializeField] FollowingJoint.JointDriveSerializable positionDriveSettings = new FollowingJoint.JointDriveSerializable() {
        positionSpring = 3_000f,
        positionDamper = 0.0f,
        maximumForce = Mathf.Infinity
    };
    [SerializeField] FollowingJoint.JointDriveSerializable angularDriveSettings = new FollowingJoint.JointDriveSerializable() {
        positionSpring = 3_000f,
        positionDamper = 0.0f,
        maximumForce = Mathf.Infinity
    };

    bool updatePosition = false;
    Vector3 newPositionPassthrough;
    [SerializeField] GameObject visualization;
    [SerializeField] Vector3 visScale = Vector3.one;

    public LayerMask layerMask;

    void Start(){
        anchor = new GameObject("Anchor").AddComponent<Rigidbody>();    
        anchor.isKinematic = true;

        if(visualization){
            GameObject vis = Instantiate(visualization, anchor.position, anchor.rotation, anchor.transform);
            vis.transform.localScale = visScale;
        }

        anchor.gameObject.SetActive(false);
    }

    public void UpdateJoint(){
        positionDrive.positionSpring = positionDriveSettings.positionSpring;
        positionDrive.positionDamper = positionDriveSettings.positionDamper;
        positionDrive.maximumForce = positionDriveSettings.maximumForce; 

        angularDrive.positionSpring = angularDriveSettings.positionSpring;
        angularDrive.positionDamper = angularDriveSettings.positionDamper;
        angularDrive.maximumForce = angularDriveSettings.maximumForce;

        ((ConfigurableJoint)joint).angularXDrive = ((ConfigurableJoint)joint).angularYZDrive = ((ConfigurableJoint)joint).slerpDrive = angularDrive;
        ((ConfigurableJoint)joint).xDrive = ((ConfigurableJoint)joint).yDrive = ((ConfigurableJoint)joint).zDrive = positionDrive;

    }

    Vector3 offsetX = Vector3.zero;
    Vector3 offsetY = Vector3.zero;

    void Update(){
        if(Mouse.current.leftButton.wasPressedThisFrame){
            Camera camera = Camera.main;
            RaycastHit hit;
            if(Physics.Raycast(camera.ScreenPointToRay(Mouse.current.position.ReadValue()), out hit, Mathf.Infinity, layerMask)){
                newPositionPassthrough = hit.point;
                anchor.transform.position = newPositionPassthrough;
                Rigidbody target = hit.collider.GetComponentInParent<Rigidbody>();
                if(!target) return;
                Grabbable grabbable = target.GetComponentInParent<Grabbable>();

                if(joint) Destroy(joint);
                if(type == JointType.Spring){
                    joint = anchor.gameObject.AddComponent<SpringJoint>();
                    ((SpringJoint)joint).spring = spring;
                    ((SpringJoint)joint).damper = damper;
                }else if(type == JointType.Fixed){
                    joint = anchor.gameObject.AddComponent<FixedJoint>();
                }else if(type == JointType.Configurable){
                    joint = anchor.gameObject.AddComponent<ConfigurableJoint>();
                    UpdateJoint();
                }

                joint.connectedBody = target;
                joint.autoConfigureConnectedAnchor = true;
                updatePosition = true;

                Vector3 midScreen = new Vector3(camera.pixelWidth / 2.0f, camera.pixelWidth / 2.0f, hit.distance);
                Vector3 origin = camera.ScreenToWorldPoint(midScreen);

                offsetX = (camera.ScreenToWorldPoint(midScreen + new Vector3(1.0f, 0.0f, 0.0f)) - origin) * camera.pixelWidth;
                offsetY = (camera.ScreenToWorldPoint(midScreen + new Vector3(0.0f, 1.0f, 0.0f)) - origin) * camera.pixelHeight;

                // speed = hit.distance;
                if(grabbable) grabbable._OnHandGrab(null, null);

                anchor.gameObject.SetActive(true);
            }
        }

        if(Mouse.current.leftButton.wasReleasedThisFrame){
            if(joint){
                Grabbable grabbable = joint.connectedBody.GetComponentInParent<Grabbable>();
                if(grabbable) grabbable._OnHandRelease(null);
                Destroy(joint);
                anchor.gameObject.SetActive(false);
            }
            joint = null;
        }
    }

    void FixedUpdate(){
        if(updatePosition){
            anchor.MovePosition(newPositionPassthrough);
            
            updatePosition = false;
        }
        if(joint){
            Camera cam = Camera.main;
            Vector3 delta = Mouse.current.delta.value;
            delta.z = Mouse.current.scroll.value.y;

            if(Keyboard.current.eKey.isPressed){
                anchor.MoveRotation(anchor.rotation *
                    Quaternion.AngleAxis(delta.y * Time.deltaTime * 10.0f, anchor.transform.InverseTransformDirection(transform.right)) *
                    Quaternion.AngleAxis(-delta.x * Time.deltaTime * 10.0f, anchor.transform.InverseTransformDirection(transform.up)) *
                    Quaternion.AngleAxis(delta.z  * Time.deltaTime * 10.0f, anchor.transform.InverseTransformDirection(transform.forward))
                );
            }else{
                // Vector3 cameraRelative = cam.transform.right * delta.x + cam.transform.up * delta.y + cam.transform.forward * delta.z * 0.25f;
                Vector3 cameraRelative = offsetX * delta.x + offsetY * delta.y + cam.transform.forward * delta.z;
                anchor.MovePosition(anchor.position + cameraRelative * Time.deltaTime * speed);
            }
        }

        if(Gamepad.current != null){
            Vector2 rotation = Gamepad.current.leftStick.value;
            float rotationZ = -Gamepad.current.leftTrigger.value + Gamepad.current.leftShoulder.value;

            anchor.MoveRotation(anchor.rotation *
                Quaternion.AngleAxis(rotation.y * Time.deltaTime * 500.0f, anchor.transform.InverseTransformDirection(transform.right)) *
                Quaternion.AngleAxis(rotation.x * Time.deltaTime * 500.0f, anchor.transform.InverseTransformDirection(transform.up)) *
                Quaternion.AngleAxis(rotationZ  * Time.deltaTime * 500.0f, anchor.transform.InverseTransformDirection(transform.forward))
            );
        }
    }
}

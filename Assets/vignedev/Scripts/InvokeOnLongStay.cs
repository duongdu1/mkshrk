using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(InvokeOnLongStay))]
class InvokeOnLongStayEditor : Editor {
    public override void OnInspectorGUI(){
        base.OnInspectorGUI();
        var x = (InvokeOnLongStay)target;
        GUILayout.Label(x.value.ToString());
    }
}
#endif

public class InvokeOnLongStay : MonoBehaviour{
    private float _value = 0.0f;
    public float value => _value;
    public float duration = 1.0f;

    public bool wasInvoked = false;
    public bool oneShot = true;
    private bool hasReset = true;

    public GameObject target;
    public UnityEvent action;
    public UnityEvent onReset;

    void FixedUpdate() {
        if(hasReset && ((!wasInvoked && oneShot) || (!oneShot)) && _value >= duration){
            action.Invoke();
            wasInvoked = true;
            hasReset = false;
        }
    }

    // void Update(){
    //       WorldSpaceIMGUI.Text(transform, value.ToString());
    // }

    void OnTriggerStay(Collider col){
        if(col.gameObject == target) _value += Time.fixedDeltaTime;
    }

    void OnTriggerExit(Collider col){
        if(col.gameObject == target) {
            _value = 0.0f;
            hasReset = true;
            onReset.Invoke();
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class MaterialColorOverride : MonoBehaviour{
    new MeshRenderer renderer;
    
    [Serializable] struct ColorOverride { public string field; public Color color; };
    [SerializeField] List<ColorOverride> colorOverrides = new List<ColorOverride>();

    void Awake() {
        renderer = GetComponent<MeshRenderer>();
    }

    void Start(){
        Material material = renderer.material;
        foreach(var item in colorOverrides){
            material.SetColor(item.field, item.color);
        }
    }
}

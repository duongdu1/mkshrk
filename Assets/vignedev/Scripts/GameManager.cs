using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using EasyButtons;

#if UNITY_EDITOR
using UnityEditor;
using EasyButtons.Editor;
[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor {
    StringBuilder builder = new StringBuilder();
    private ButtonsDrawer drawer;

    void OnEnable(){
        drawer = new ButtonsDrawer(target);
    }

    public override void OnInspectorGUI(){
        DrawDefaultInspector();
        drawer.DrawButtons(targets);

        GameManager gm = (GameManager)target;

        if(gm.lastScenarioResult == null){
            GUILayout.Label("Last Scenario Result is null");
        }else{
            var result = gm.lastScenarioResult.Value;
            builder.Clear();
            
            builder.AppendLine($"result.name = {result.name}");
            builder.AppendLine($"result.goal = {result.goal}");
            builder.AppendLine($"result.duration = {result.duration}");
            builder.AppendLine($"result.alive = {result.alive} ({result.deathCause})");

            builder.AppendLine("");
            builder.AppendLine($"result.flags[{result.flags.Count}]");
            int i = 0;
            foreach(var pair in result.flags)
                builder.AppendLine($"    [{i++}] {pair.Key} | {pair.Value}");

            builder.AppendLine($"result.values[{result.values.Count}]");
            i = 0;
            foreach(var pair in result.values)
                builder.AppendLine($"    [{i++}] {pair.Key} | {pair.Value}");

            builder.AppendLine($"result.items[{result.items.Count}]");
            i = 0;
            foreach(var pair in result.items)
                builder.AppendLine($"    [{i++}] {pair.Key} | {pair.Value}");

            builder.AppendLine($"result.properties[{result.properties.Count}]");
            i = 0;
            foreach(var pair in result.properties)
                builder.AppendLine($"    [{i++}] {Enum.GetName(typeof(ItemProperty), pair.Key)} | {pair.Value}");

            builder.AppendLine("");
            builder.AppendLine($"result.score = {result.score}");

            GUILayout.Label(builder.ToString());
        }
    }
}
#endif

/*
This class is to maintate the state of the game within the current session, mainly to aliviate
the need of PlayerPrefs and to prevent the possibility of PlayerPrefs states not being properly
reset.

It will also serve as a singleton with easy access to certain parts
*/
public class GameManager : SingletonBehaviour<GameManager>{
    // common colors
    public static Color transparent_black = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    public static Color transparent_white = new Color(1.0f, 1.0f, 1.0f, 0.0f);
    public static Color transparent_red   = new Color(1.0f, 0.0f, 0.0f, 0.0f);

    // for retrieving player info, note that player can be null
    private PlayerController _player;
    public PlayerController player { get {
        if(_player == null) _player = GameObject.FindGameObjectWithTag("Player")?.GetComponent<PlayerController>();
        if(_player) return _player;
        return _player = FindObjectOfType<PlayerController>();
    } }

    public string nextScene = null;

    // calls from the singleton
    private FullScreenPassRendererFeature _screenBlend;
    public FullScreenPassRendererFeature screenBlend => _screenBlend;
    void Initialize() {
        var passes = UniversalRenderPipelineUtils.GetRendererFeatureList<FullScreenPassRendererFeature>();
        foreach(var pass in passes){
            if(pass.passMaterial.shader.name == "Shader Graphs/BlendOver"){
                _screenBlend = pass;
                break;
            }
        }

        // vig: before this one to ensure visibility on startup
        //      now that menubeta is handling it using the worldloader
        // StartCoroutine(ScreenFade(transparent_black, 1.0f));
    }

    private bool _isLoading = false;
    public bool isLoading => _isLoading;

    public bool paused = false;

    // vig: Unity for some weird design choice kills all coroutines, even if they are
    //      within a component that is set to not be destroyed across scenes.
    //      Can't find any valid statement in the documentation, but several forum
    //      threads are there to document this atrocity. That's why we have now a
    //      THREE-step process.

    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void LoadScene(string sceneName){
        StartCoroutine(LoadSceneCoroutine(sceneName));
    }

    public IEnumerator LoadSceneCoroutine(string sceneName){
        return LoadSceneCoroutine(sceneName, Color.black);
    }

    public IEnumerator LoadSceneCoroutine(string sceneName, Color color, bool loading = true) {
        Debug.Log($"LoadSceneCoroutine(sceneName=\"{sceneName}\") @ isLoading={_isLoading}");
        if(_isLoading) yield break;

        nextScene = sceneName;
        _isLoading = true;

        // first fade from nothing to black
        yield return StartCoroutine(ScreenFade(color, fadeDuration));

        // go to loading screen scene if non-direct
        // load async so it doesn't go to steamvr loading
        AsyncOperation operation = SceneManager.LoadSceneAsync(loading ? "Loading" : sceneName);
        operation.allowSceneActivation = false;

        while(!operation.isDone){
            if(operation.progress >= 0.9f){
                if(!loading) { // clear these flags because we are basically done, loading screen-less
                    nextScene = "";
                    _isLoading = false;
                }
                operation.allowSceneActivation = true;
                break;
            }
            yield return null;
        }
    }

    public IEnumerator PerformLoading(){
        Debug.Log($"PerformLoading(with {nextScene})");

        // fade back to nothing
        yield return StartCoroutine(ScreenFade(transparent_black, fadeDuration));

        Debug.Log("Faded back, loading the world.");

        // attempt to load the next world
        AsyncOperation operation = SceneManager.LoadSceneAsync(nextScene);
        operation.allowSceneActivation = false; // wait to be gracefully transitioned

        while(!operation.isDone){
            if(operation.progress >= 0.9f){  // magic number per documentation
                // fade back to black
                yield return StartCoroutine(ScreenFade(Color.black, fadeDuration));
                
                // cleanup
                nextScene = null;
                _isLoading = false;

                // allow loading
                operation.allowSceneActivation = true;
                break;
            }
            yield return null;
        }
    }

    public IEnumerator FinalizeLoading(){
        // fade back to nothing
        yield return StartCoroutine(ScreenFade(transparent_black, fadeDuration));

        // cleanup
        nextScene = null;
        _isLoading = false;

        // this is called on every scene load (or should be), so disable the pause flag
        paused = false;
    }

    // screen fade setter
    public float fadeDuration = 1.0f;

    public IEnumerator ScreenFade(Color start, Color end, float duration){
        Debug.Log($"Screen Fade ({start} -> {end} in {duration})");

        screenBlend.passMaterial.SetColor("_Color", start);

        float time = 0.0f;
        while(time <= duration){
            time += Time.deltaTime;
            screenBlend.passMaterial.SetColor("_Color", Color.Lerp(start, end, Mathf.Clamp01(time / duration)));

            yield return null;
        }

        screenBlend.passMaterial.SetColor("_Color", end);
    }

    public IEnumerator ScreenFade(Color end, float duration){
        Color start = screenBlend.passMaterial.GetColor("_Color");
        return ScreenFade(start, end, duration);
    }

    // scenario result
    public Nullable<Scenario.Result> lastScenarioResult = null;
}
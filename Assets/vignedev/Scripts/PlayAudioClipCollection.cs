using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayAudioClipCollection : MonoBehaviour{
    public AudioClipCollection collection;
    private AudioSource audioSource;

    void Awake(){
        audioSource = GetComponent<AudioSource>();
    }

    public void Play(){
        audioSource.clip = collection.clips[Random.Range(0, collection.clips.Length)];
        audioSource.Play();
    }

    public void PlayOneShot(){
        audioSource.PlayOneShot(collection.clips[Random.Range(0, collection.clips.Length)]);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using EasyButtons;
using UnityEngine.Events;

public class PauseMenuUIBinding : MonoBehaviour{
    public UnityEvent onResume;

    Scenario _scenario;
    Scenario scenario {
        get {
            if(_scenario) return _scenario;
            return _scenario = FindObjectOfType<Scenario>();
        }
    }

    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void TogglePauseState() {
        SetPauseState(!GameManager.instance.paused);
    }

    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void SetPauseState(bool state) {
        GameManager.instance.paused = state;
        if(!state) onResume.Invoke();
    }
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void RestartScenario() {
        GameManager.instance.LoadScene(SceneManager.GetActiveScene().name);
    }
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void BackToMainMenu() {
        scenario.EndScenario(Scenario.EndReason.GiveUp);
    }
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void ResetPlayerTransform() {
        GameManager.instance.player.ResetPosition();
    }
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void QuitScenario(bool wasSuicide){
        scenario.EndScenario(wasSuicide ? Scenario.EndReason.Death : Scenario.EndReason.Leave);
    }
}

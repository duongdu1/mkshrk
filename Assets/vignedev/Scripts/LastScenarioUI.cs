using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LastScenarioUI : MonoBehaviour{
    [SerializeField] TMP_Text name_txt;
    [SerializeField] TMP_Text goal_txt;
    [SerializeField] TMP_Text duration_txt;
    [SerializeField] TMP_Text alive_txt;
    [SerializeField] TMP_Text cause_txt;

    [SerializeField] TMP_Text cold_txt;
    [SerializeField] TMP_Text food_txt;
    [SerializeField] TMP_Text water_txt;
    [SerializeField] TMP_Text resources_txt;
    [SerializeField] TMP_Text score_txt;

    [SerializeField] GameObject item_template;

    void Start(){
        if(GameManager.instance.lastScenarioResult == null){
            gameObject.SetActive(false);
            return;
        }

        var result = GameManager.instance.lastScenarioResult.Value;
        name_txt.text = result.name;
        goal_txt.text = result.goal;
        duration_txt.text = result.duration.ToString("0.00");
        alive_txt.text = result.alive ? "yes" : "no";
        cause_txt.text = result.alive ? "(exited safely)" : $"({result.deathCause})";

        cold_txt.text = result.properties[ItemProperty.Cold].ToString("0.0");
        food_txt.text = result.properties[ItemProperty.Food].ToString("0.0");
        water_txt.text = result.properties[ItemProperty.Water].ToString("0.0");
        resources_txt.text = result.properties[ItemProperty.Resources].ToString("0.0");

        score_txt.text = result.score.ToString("0.0");

        foreach(var key in result.items){
            CreateItem(key.Key, key.Value);
        }

        if(result.items.Count == 0 && result.flags.ContainsKey("backpack") && result.flags["backpack"] == false){
            CreateItem("Left without backpack.", -1);
        }
    }

    void CreateItem(string name, int count){
        GameObject copy = Instantiate(item_template, item_template.transform.position, item_template.transform.rotation);
        copy.transform.SetParent(item_template.transform.parent, true);
        copy.transform.localScale = Vector3.one;

        TMP_Text[] kids = copy.GetComponentsInChildren<TMP_Text>();
        foreach(var kid in kids){
            if(kid.gameObject.name == "label") kid.text = name;
            else if(kid.gameObject.name == "value") kid.text = count >= 0 ? $"x{count}" : "";
        }

        copy.SetActive(true);
    }
}

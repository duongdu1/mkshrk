using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioNeedle : MonoBehaviour{
    [SerializeField] Radio radio;
    public Vector3 offset;

    private Vector3 initial;

    void Awake() {
        initial = transform.localPosition;
    }

    void FixedUpdate(){
        // from -1 to 1, where 0 is the center
        float side = (radio.frequencyLimit.y - radio.frequencyLimit.x) / 2.0f;
        float center = radio.frequencyLimit.x + side;
        float relative = (radio.currentFrequency - center) / side;

        transform.localPosition = initial + relative * offset;
    }

    void OnDrawGizmosSelected() {
        Gizmos.matrix = Matrix4x4.Translate(transform.position) * Matrix4x4.Rotate(transform.rotation);

        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(Vector3.zero,  offset);
        Gizmos.color = Color.red;
        Gizmos.DrawRay(Vector3.zero, -offset);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightIntensityByDistance : MonoBehaviour{
    [SerializeField] AnimationCurve curve = AnimationCurve.Constant(0.0f, 1.0f, 1.0f);
    [SerializeField] LayerMask mask;
    new Light light;

    float desiredIntensity = 0.0f;

    public float maxIntensity = 2.0f;
    void Awake() {
        if(!light) light = GetComponent<Light>();
    }

    void Update(){
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, light.range, mask,QueryTriggerInteraction.Ignore)){
            desiredIntensity = curve.Evaluate(hit.distance / light.range) * maxIntensity;
        }else{
            desiredIntensity = maxIntensity;
        }

        light.intensity = Mathf.Lerp(light.intensity, desiredIntensity, Time.deltaTime * 10.0f);
    }
}

using UnityEngine;
using System.Collections.Generic;

class GeneralDictionary : MonoBehaviour{
    public Dictionary<string, object> dictionary = new Dictionary<string, object>();

    public void Set(string key, object value){
        dictionary[key] = value;
    }
    public object Get(string key){
        return dictionary.GetValueOrDefault(key, null);
    }
    public bool Has(string key){
        return dictionary.ContainsKey(key);
    }
    public void Remove(string key){
        dictionary.Remove(key);
    }

    public object this[string key] {
        get { return dictionary[key]; }
        set { dictionary[key] = value; }
    }

    static public GeneralDictionary GetOrObtain(GameObject target) {
        var result = target.GetComponent<GeneralDictionary>();
        if(result) return result;
        return target.AddComponent<GeneralDictionary>();
    }
}
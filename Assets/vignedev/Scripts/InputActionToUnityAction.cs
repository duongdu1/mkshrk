using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class InputActionToUnityAction : MonoBehaviour{
    [SerializeField] InputActionReference activationAction;
    public UnityEvent started;
    public UnityEvent performed;
    public UnityEvent canceled;

    void OnEnable() {
        activationAction.action.started += StartedAction;
        activationAction.action.performed += PerformedAction;
        activationAction.action.canceled += CanceledAction;
    }
    void OnDisable() {
        activationAction.action.started -= StartedAction;
        activationAction.action.performed -= PerformedAction;
        activationAction.action.canceled -= CanceledAction;
    }
    void StartedAction(InputAction.CallbackContext ctx) { started.Invoke(); }
    void PerformedAction(InputAction.CallbackContext ctx) { performed.Invoke(); }
    void CanceledAction(InputAction.CallbackContext ctx) { canceled.Invoke(); }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterContainerFillController : MonoBehaviour
{
    [SerializeField] Material material;
    [SerializeField, Range(0.0f, 1.0f)]
    private float _fillRate;
    public float fillRate {
        get { return _fillRate; }
        set { _fillRate = value; UpdateLevel(); }
    }

    void Awake(){
        UpdateLevel();
    }

    void UpdateLevel(){
        material.SetFloat("_FillRate", _fillRate);
    }
}

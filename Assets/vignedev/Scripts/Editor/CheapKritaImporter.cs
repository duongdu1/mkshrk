using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AssetImporters;
using System.IO.Compression;
using System.IO;
using System;

// It is called a *cheap* Krita importer because:
// - it is made to avoid paying extra for any work
// - and that comes with the fact that it just works
//   by extracting mergedimage.png from the kra file
//   and is extremely simple, similiar to my Blender
//   hack

[ScriptedImporter(0, "kra")]
public class CheapKritaImporter : ScriptedImporter{
    const string MERGED_IMAGE_FILENAME = "mergedimage.png";
    const int BUFFER_SIZE = 1024 * 64;

    [SerializeField] bool overrideFormat = false;
    [SerializeField] TextureFormat format = TextureFormat.RGBA32;
    [SerializeField] bool linear = false;
    [SerializeField] bool compress = true;
    [SerializeField] TextureCompressionQuality compressionQuality = TextureCompressionQuality.Normal;
    [SerializeField] FilterMode filterMode = FilterMode.Bilinear;

    public override void OnImportAsset(AssetImportContext ctx) {
        try{
            using(ZipArchive archive = ZipFile.OpenRead(ctx.assetPath)){
                foreach(ZipArchiveEntry entry in archive.Entries){
                    if(entry.Name != MERGED_IMAGE_FILENAME) continue;

                    byte[] buffer = new byte[BUFFER_SIZE];
                    using(Stream stream = entry.Open()){
                        using(MemoryStream mem = new MemoryStream()){
                            // help i thought this was c#, not c
                            int read = 0; // i'm getting progtest flashbacks
                            while((read = stream.Read(buffer, 0, BUFFER_SIZE)) > 0)
                                mem.Write(buffer, 0, read);

                            Texture2D tex = new Texture2D(2, 2);
                            if(ImageConversion.LoadImage(tex, mem.ToArray())){
                                if(overrideFormat){
                                    Texture2D tmp = new Texture2D(tex.width, tex.height, compress ? TextureFormat.RGBA32 : format, 12, linear);
                                    tmp.filterMode = filterMode;
                                    tmp.SetPixels(tex.GetPixels());
                                    tmp.Apply(true); DestroyImmediate(tex);
                                    if(compress) EditorUtility.CompressTexture(tmp, format, compressionQuality);
                                    ctx.AddObjectToAsset("texture", tmp);
                                    ctx.SetMainObject(tmp);
                                }else{
                                    tex.filterMode = filterMode;
                                    ctx.AddObjectToAsset("texture", tex);
                                    ctx.SetMainObject(tex);
                                }
                            }else{
                                ctx.LogImportError("ImageConversion.LoadImage failed");
                            }
                        }
                    }
                }
            }
        }catch(Exception ex){
            ctx.LogImportError($"Caught exception: {ex.ToString()}");
        }
    }
}
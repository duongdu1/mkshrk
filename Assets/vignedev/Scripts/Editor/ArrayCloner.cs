using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class ArrayCloner : EditorWindow {
    const string UNDO_NAME = "Clone";

    private Vector3 relativeOffset;
    private Vector3 absoluteOffset;

    [Range(0, 1000)]
    private int count;

    private void OnGUI() {
        EditorGUILayout.BeginVertical();
        relativeOffset = EditorGUILayout.Vector3Field("Relative", relativeOffset);
        absoluteOffset = EditorGUILayout.Vector3Field("Absolute", absoluteOffset);
        count = EditorGUILayout.IntField("Count", count);
        EditorGUILayout.EndVertical();

        if(Selection.gameObjects.Length == 1){
            if(!Selection.gameObjects[0].GetComponent<Renderer>()) EditorGUILayout.LabelField("Doesn't have renderer, can't compute bounds");
            else if(GUILayout.Button("Clone")) PerformCloning();
        }else{
            EditorGUILayout.LabelField("Select only one object.");
        }
    }

    private void PerformCloning(){
        int undoId = Undo.GetCurrentGroup();

        GameObject target = Selection.gameObjects[0]; // has to exist
        Vector3 size = target.GetComponent<Renderer>().bounds.size;

        List<GameObject> copies = new List<GameObject>() {
            target
        };

        Vector3 perObjectOffset = absoluteOffset + Vector3.Scale(size, relativeOffset);
        for(int i = 1; i <= count; i++){
            GameObject clone = EditorUtils.Duplicate(target);
            Undo.RegisterCreatedObjectUndo(clone, UNDO_NAME);
            clone.transform.position = target.transform.position;
            clone.transform.rotation = target.transform.rotation;
            clone.transform.localScale = target.transform.localScale;

            clone.transform.position += perObjectOffset * i;
            clone.transform.SetParent(target.transform.parent, true);
            copies.Add(clone);
        }

        Selection.objects = copies.ToArray();
        Undo.CollapseUndoOperations(undoId);
    }

    [MenuItem("Tools/Array Cloner")]
    private static void OpenWindow(){
        GetWindow<ArrayCloner>(false, "Array Cloner", true);
    }

    void OnInspectorUpdate(){
        this.Repaint();
    }
}
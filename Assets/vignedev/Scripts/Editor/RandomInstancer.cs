using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

public class TemplateScriptableObject : ScriptableObject {
    
}

[InitializeOnLoad]
public class RandomInstancer : EditorWindow {
    void OnFocus() {
        SceneView.duringSceneGui -= this.OnSceneGUI;
        SceneView.duringSceneGui += this.OnSceneGUI;
    }

    void OnDestroy() {
        SceneView.duringSceneGui -= this.OnSceneGUI;
    }

    private static BoxBoundsHandle handles = new BoxBoundsHandle();
    void OnSceneGUI(SceneView sceneView) {
        handles.center = boxCenter;
        handles.size = boxSize;
        
        EditorGUI.BeginChangeCheck();
		handles.DrawHandle();

		if (EditorGUI.EndChangeCheck()){
            boxCenter = handles.center;
            boxSize = handles.size;

            Repaint();
        }
    }

    [MenuItem("Tools/Random Instancer")]
    private static void OpenWindow(){
        GetWindow<RandomInstancer>(false, "Random Instancer", true);
    }

    [SerializeField] List<GameObject> templates = new List<GameObject>();
    [SerializeField] Vector3 boxCenter = Vector3.zero;
    [SerializeField] Vector3 boxSize = Vector3.one;
    [SerializeField] [Range(0, 1000)] int count = 0;

    private void CreateGUI(){
        SerializedObject serializedObject = new SerializedObject(this);
        
        EditorUtils.ExposeField(serializedObject, rootVisualElement, "templates");
        EditorUtils.ExposeField(serializedObject, rootVisualElement, "count");
        EditorUtils.ExposeField(serializedObject, rootVisualElement, "boxCenter");
        EditorUtils.ExposeField(serializedObject, rootVisualElement, "boxSize");

        rootVisualElement.Add(new Button(PerformSpawning) { text = "Spawn"});
        rootVisualElement.Bind(serializedObject);
    }

    void PerformSpawning(){
        List<GameObject> copies = new List<GameObject>();
        int undoId = Undo.GetCurrentGroup();

        for(int i = 0; i < count; ++i){
            GameObject clone = EditorUtils.Duplicate(templates[Random.Range(0, templates.Count)]);
            Undo.RegisterCreatedObjectUndo(clone, "Random Instancer");
            clone.transform.position = new Vector3(
                boxCenter.x + Random.Range(-boxSize.x, boxSize.x) / 2.0f,
                boxCenter.y + Random.Range(-boxSize.y, boxSize.y) / 2.0f,
                boxCenter.z + Random.Range(-boxSize.z, boxSize.z) / 2.0f
            );
            copies.Add(clone);
        }

        Selection.objects = copies.ToArray();
        Undo.CollapseUndoOperations(undoId);
    }
}

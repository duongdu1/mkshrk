using System;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[InitializeOnLoad]
public class ScenePhysics : EditorWindow {
    // Name of the undo action
    const string UNDO_NAME = "Editor Physics";

    // Rigidbodies that were not selected and were temporarily disabled
    static List<Rigidbody> disabledBodies = new List<Rigidbody>();
    // Rigidbodies that were selected and are physically simulated
    static Dictionary<int, Rigidbody> selectedBodies = new Dictionary<int, Rigidbody>();
    // List of temporary rigidbodies
    static List<Rigidbody> tempRigidbodies = new List<Rigidbody>();
    // Mesh Colliders that are non-convex that need to be convex temporarily
    static List<MeshCollider> fixedMeshColliders = new List<MeshCollider>();
 
    // Adds a temporary Rigidbody component to selected items, that gets deleted afterwards.
    // If it does already has one, then no action is performed.
    static bool addRigidbodyToSelected = false;
    static bool useFixedTimestep = false;
    static bool useCustomTimestep = false;
    static float customTimestep = 0.0001f;
    static bool resetVelocity = true;

    private void OnGUI(){
        // Basically we are considered "running" if we are simulating by script
        // This doesn't account for other scripts messing with this.
        bool isRunning = Physics.simulationMode == SimulationMode.Script;

        // Hey, it's a button. Wow!
        if (GUILayout.Button(isRunning ? "Stop Physics" : "Run Physics")){
            if(!isRunning) StartPhysicsLoop();
            else EndPhysicsLoop();
        }

        // Hide the toggle, because I can't see any "disable this toggle onegaishimasu"
        if(!isRunning){
            resetVelocity = GUILayout.Toggle(resetVelocity, "Reset Velocity");
            useFixedTimestep = GUILayout.Toggle(useFixedTimestep, "Use Fixed Timestep");
            if(useFixedTimestep){
                useCustomTimestep = GUILayout.Toggle(useCustomTimestep, "Use Custom Timestep");
                if(useCustomTimestep) customTimestep = EditorGUILayout.FloatField("Custom Timestep", customTimestep);
            }
            addRigidbodyToSelected = GUILayout.Toggle(
                addRigidbodyToSelected,
                "Add Rigidbody to selected if necessary"
            );
        }
    }

    private void StartPhysicsLoop(){
        // Clean slate
        tempRigidbodies.Clear();
        selectedBodies.Clear();
        disabledBodies.Clear();
        fixedMeshColliders.Clear(); 

        // list of "affected gameobjects" 
        List<GameObject> affected = new List<GameObject>();

        // to cojoin undo acions
        // int undoId = Undo.GetCurrentGroup();

        // filter through the selection for items
        foreach(GameObject gobject in Selection.gameObjects){
            Rigidbody body = gobject.GetComponent<Rigidbody>();

            // store itself and its location
            // Undo.RegisterCompleteObjectUndo(gobject, UNDO_NAME);
            // Undo.RegisterCompleteObjectUndo(gobject.transform, UNDO_NAME);

            // if there is no rigidbody and a temporary one is requested
            if(body == null && addRigidbodyToSelected){
                body = gobject.AddComponent<Rigidbody>();
                tempRigidbodies.Add(body);
            }
            if(body == null || body.isKinematic) continue;
            selectedBodies[body.GetInstanceID()] = body;

            // sift through the meshcolliders that need editing, since they need to be convex
            MeshCollider collider = body.GetComponent<MeshCollider>();
            if(collider != null && collider.convex == false){
                // Undo.RegisterCompleteObjectUndo(collider, UNDO_NAME);
                fixedMeshColliders.Add(collider);
                collider.convex = true;
            }
        }

        // no bodies, nothing has changed (i think)
        if(selectedBodies.Count == 0) {
            // Undo.CollapseUndoOperations(undoId);
            return;
        }

        if(resetVelocity){
            foreach(var keypiar in selectedBodies){
                keypiar.Value.velocity = Vector3.zero;
            }
        }

        // disable every other non-selected rigidbody
        Rigidbody[] bodies = FindObjectsByType<Rigidbody>(FindObjectsInactive.Exclude, FindObjectsSortMode.None);
        foreach(Rigidbody body in bodies){
            if(!body.isKinematic && !selectedBodies.ContainsKey(body.GetInstanceID())){
                // Undo.RegisterCompleteObjectUndo(body, UNDO_NAME);
                body.isKinematic = true;
                disabledBodies.Add(body);
            }
        }

        // collapse all the events to single undo
        // Undo.CollapseUndoOperations(undoId);

        // add the approapriate callbacks
        EditorApplication.update += Update;
        Physics.simulationMode = SimulationMode.Script;
    }

    private void EndPhysicsLoop(){
        // re-enable the non-selected rigidbodies
        foreach(Rigidbody body in disabledBodies)
            body.isKinematic = false;

        // remove the temporary rigidbodies
        foreach(Rigidbody body in tempRigidbodies)
            DestroyImmediate(body);

        // and fix back the mesh colliders to their former glory
        foreach(MeshCollider collider in fixedMeshColliders)
            collider.convex = false;

        // and to clean up the ram
        fixedMeshColliders.Clear();
        disabledBodies.Clear();
        selectedBodies.Clear();
        tempRigidbodies.Clear();

        // and to clean up the callbacks
        EditorApplication.update -= Update;
        Physics.simulationMode = SimulationMode.FixedUpdate;
    }

    private static void Update() {
        // simulate, let's goo
        Physics.Simulate(useFixedTimestep ? (useCustomTimestep ? customTimestep : Time.fixedDeltaTime) : Time.deltaTime);
    }

    [MenuItem("Tools/Editor Physics")]
    private static void OpenWindow(){
        GetWindow<ScenePhysics>(false, "Editor Physics", true);
    }
}
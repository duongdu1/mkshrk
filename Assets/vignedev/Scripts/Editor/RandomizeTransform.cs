using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class RandomizeTransform : EditorWindow {
    const string UNDO_NAME = "Randomize Transform";

    private Bounds positionBounds;
    private Bounds eulerBounds;
    private Bounds scaleBounds;

    private void OnGUI() {
        EditorGUILayout.BeginHorizontal();
        positionBounds.min = EditorGUILayout.Vector3Field("Position Min", positionBounds.min);
        positionBounds.max = EditorGUILayout.Vector3Field("Position Max", positionBounds.max);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        eulerBounds.min = EditorGUILayout.Vector3Field("Euler Min", eulerBounds.min);
        eulerBounds.max = EditorGUILayout.Vector3Field("Euler Max", eulerBounds.max);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        scaleBounds.min = EditorGUILayout.Vector3Field("Scale Min", scaleBounds.min);
        scaleBounds.max = EditorGUILayout.Vector3Field("Scale Max", scaleBounds.max);
        EditorGUILayout.EndHorizontal();
        
        if(GUILayout.Button("Randomize")) Randomize();
    }

    private void Randomize(){
        int undoId = Undo.GetCurrentGroup();
        foreach(GameObject obj in Selection.gameObjects){
            Undo.RecordObject(obj.transform, UNDO_NAME);
            float randomPos = Random.Range(0.0f, 1.0f);
            float randomRot = Random.Range(0.0f, 1.0f);
            float randomSca = Random.Range(0.0f, 1.0f);

            obj.transform.position += Vector3.Lerp(positionBounds.min, positionBounds.max, randomPos);
            obj.transform.eulerAngles += Vector3.Lerp(eulerBounds.min, eulerBounds.max, randomRot);
            obj.transform.localScale += Vector3.Lerp(scaleBounds.min, scaleBounds.max, randomSca);
        }
        Undo.CollapseUndoOperations(undoId);
    }

    [MenuItem("Tools/Randomize Transform")]
    private static void OpenWindow(){
        GetWindow<RandomizeTransform>(false, "Randomize Transform", true);
    }
}
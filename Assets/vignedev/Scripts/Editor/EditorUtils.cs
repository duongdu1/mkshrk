using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

public class EditorUtils {
    // https://discussions.unity.com/t/how-do-i-properly-duplicate-an-object-in-a-editor-script/28744/3
    // and also custom optimalization/tweaks
    public static GameObject Duplicate (GameObject source){
        Object prefabRoot = PrefabUtility.GetCorrespondingObjectFromSource(source);
        return (GameObject) PrefabUtility.InstantiatePrefab (prefabRoot ?? source); 
    }

    public static void ExposeField(SerializedObject obj, VisualElement root, string fieldName){
        SerializedProperty property = obj.FindProperty(fieldName);
        PropertyField field = new PropertyField(property);
        root.Add(field);
    }
}
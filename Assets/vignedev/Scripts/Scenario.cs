using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using System.IO;

public class Scenario : MonoBehaviour{
    [Serializable]
    public struct Result {
        public string name;
        public string goal;
        public float duration;
        public bool alive;
        public string deathCause;

        public Dictionary<string, bool> flags;
        public Dictionary<string, float> values;
        public Dictionary<string, int> items;
        public Dictionary<ItemProperty, float> properties;

        public float score;

        public override string ToString(){
            string flagsStr = "";
            foreach(var item in flags) flagsStr += $"    {item.Key}: {item.Value}\n";

            string valuesStr = "";
            foreach(var item in values) valuesStr += $"    {item.Key}: {item.Value}\n";

            string itemsStr = "";
            foreach(var item in items) itemsStr += $"    {item.Key}: {item.Value}\n";

            string propsStr = "";
            foreach(var item in properties) propsStr += $"    {Enum.GetName(typeof(ItemProperty), item.Key)}: {item.Value}\n";

            return $@"
name: '{name}'
goal: '{goal}'
duration: {duration.ToString()}
alive: {alive}
deathCause: '{deathCause}'
score: '{score}'

flags[{flags.Count}]
{flagsStr}
values[{values.Count}]
{valuesStr}
items[{items.Count}]
{itemsStr}
properties[{properties.Count}]
{propsStr}
            ";
        }
    }

    [SerializeField] string scenarioName;
    [SerializeField] string scenarioGoal;
    private float startTime;

    [Serializable]
    public enum EndReason { Death, Leave, GiveUp };

    void Start() {
        // fade in from potentional darkness - should happen on WorldLoader
        /* StartCoroutine(GameManager.instance.ScreenFade(
            GameManager.transparent_black,
            GameManager.instance.fadeDuration
        )); */
        startTime = Time.time;
    }

    // Unity doesn't expose EndScenario(reason) to UnityEvents for some reason
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void PlayerDeath (){ EndScenario(EndReason.Death ); }
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void PlayerDeath (string cause){ EndScenario(EndReason.Death, cause); }
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void PlayerLeave (){ EndScenario(EndReason.Leave ); }
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void PlayerGiveUp(){ EndScenario(EndReason.GiveUp); }

    public void EndScenario(EndReason reason, string cause = ""){
        StartCoroutine(PrepareEnding(reason, cause));
    }

    IEnumerator PrepareEnding(EndReason reason, string cause = "") {
        // calculate the result
        GameManager.instance.lastScenarioResult = (reason != EndReason.GiveUp) ? PrepareResult(reason, cause) : null; // nullify if the player gives up (main menu)

        // log the last scenario into a temp folder
        try{
            string filepath = Path.Join(Path.GetTempPath(), $"scenario_{DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff")}.txt");
            Debug.Log(filepath);
            File.WriteAllText(
                filepath,
                GameManager.instance.lastScenarioResult.ToString()
            );
        }catch(Exception err){
            Debug.LogError(err);
        }
        // leave
        yield return GameManager.instance.LoadSceneCoroutine(
            "MenuBeta",
            reason == EndReason.Death ? Color.red : Color.black,
            false
        );
    }

    Result PrepareResult(EndReason reason, string cause = ""){
        Result result = new Result() {
            name = scenarioName,
            goal = scenarioGoal,
            duration = Time.time - startTime,
            alive = reason != EndReason.Death,
            deathCause = cause,

            flags = new Dictionary<string, bool>(),
            values = new Dictionary<string, float>(),
            items = new Dictionary<string, int>(),
            properties = new Dictionary<ItemProperty, float>(),
        };

        foreach(ItemProperty key in Enum.GetValues(typeof(ItemProperty))){
            result.properties[key] = 0;
        }

        SumBackpackValues(ref result);
        CalculateResult(ref result);
        return result;
    }

    public virtual void CalculateResult(ref Result result){

    }

    void SumBackpackValues(ref Result result){
        Kaban kaban = Kaban.current;
        if(!kaban) {
            Debug.LogError("Kaban not found in the world.");
            return;
        }

        if(kaban.gameObject.activeSelf){
            Debug.Log("Kaban was left on the ground, not taken with the user.");
            result.flags["backpack"] = false;
            return;
        }
        result.flags["backpack"] = true;

        foreach(var item in kaban.stored){
            ItemData data = item.data;
            if(!data) {
                Debug.LogError($"{item.gameObject.name} has no item data!", item.gameObject);
                continue;
            }

            if(!result.items.ContainsKey(data.name)) result.items[data.name] = 1;
            else result.items[data.name] += 1;

            foreach(var prop in data.properties){
                result.properties[prop.type] += prop.value;
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class WorldLoader : MonoBehaviour{
    public enum Stage {
        None,
        Performing,
        Finalizing
    }
    [SerializeField] Stage stage = Stage.None;

    void Start(){
        if(stage == Stage.Performing) StartCoroutine(GameManager.instance.PerformLoading());
        if(stage == Stage.Finalizing) StartCoroutine(GameManager.instance.FinalizeLoading());
    }

    public void LoadScene(string scene){ // basically acts as a proxy
        GameManager.instance.LoadScene(scene);
    }
}

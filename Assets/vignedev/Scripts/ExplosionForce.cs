using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionForce : MonoBehaviour{
    public float radius;
    public float strength;
    public float upwards;
    public LayerMask layerMask;

    public void Explode(){
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layerMask);
        foreach(Collider c in colliders){
            if(!c.attachedRigidbody) continue;

            float distance = Vector3.Distance(c.transform.position, transform.position) / radius;
            c.attachedRigidbody.AddExplosionForce(strength * distance, transform.position, radius, upwards, ForceMode.Impulse);
        }
    }
}

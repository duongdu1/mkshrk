using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class PhysicalButton : MonoBehaviour{
    public bool triggerOnce = true;
    public bool triggered = false;

    private bool wasDepressedOnce = false;
    private bool lastFrameWasActive = false;

    public float distance = 0.1f;
    private Vector3 original;

    public UnityEvent onClick;
    public UnityEvent onExit;

    new Rigidbody rigidbody;

    void Awake(){
        if(!rigidbody) rigidbody = GetComponent<Rigidbody>();
        original = transform.position;
    }

    void FixedUpdate() {
        if(rigidbody.IsSleeping()) return;

        float current = Vector3.Distance(transform.position, original);
        if(current >= distance){
            if(!(triggerOnce && triggered) && !lastFrameWasActive){
                triggered = wasDepressedOnce = lastFrameWasActive = true;
                onClick.Invoke();
            }
        }else if(lastFrameWasActive && wasDepressedOnce){
            lastFrameWasActive = false;
            onExit.Invoke();
        }
    }

    void OnDrawGizmosSelected(){
        Gizmos.DrawWireSphere(original, distance);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class UITextFromPlayerPrefs : MonoBehaviour{
    [SerializeField] string key;
    [SerializeField] string fallback;
    TMP_Text text;

    void Awake(){
        text = GetComponent<TMP_Text>();
    }

    void Start(){
        text.text = PlayerPrefs.GetString(key, fallback);
    }
}

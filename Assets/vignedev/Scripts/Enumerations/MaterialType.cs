using System;

[Serializable]
public enum MaterialType {
    Default,
    Wood,
    Snow,
    Wire,
    Metal,
    ShallowWater
}
using System;

enum ItemType {
    Book,
    Branch,
    Pot,
    Ramen,
    FoodCan,
    Salt,
    Lighter
}
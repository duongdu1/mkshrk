using System;

[Serializable]
public enum ItemProperty {
    Cold,
    Food,
    Water,
    Resources
}
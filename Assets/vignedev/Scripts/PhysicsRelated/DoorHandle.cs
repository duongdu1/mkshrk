using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(HingeMonitor))]
public class DoorHandle : MonoBehaviour{
    [SerializeField] new Rigidbody rigidbody;
    [SerializeField] Rigidbody door;
    [SerializeField] HingeJoint doorHinge;

    public bool allowStateChange = true;
    public void SetAllowStateChange(bool state){ allowStateChange = state; }

    [SerializeField] bool isPulled = false;
    public float threshold = 0.5f;

    private float lastAngle = 0.0f;
    private Quaternion restRotation;

    public UnityEvent OnDoorClose;
    public UnityEvent OnDoorOpen;

    void Awake() {
        if(!rigidbody) rigidbody = GetComponent<Rigidbody>();
        if(!door) door = GetComponentInParent<Rigidbody>();
        if(!doorHinge) doorHinge = GetComponentInParent<HingeJoint>();

        restRotation = transform.localRotation;
        lastAngle = doorHinge.angle;
    }

    void FixedUpdate(){
        if(isPulled && door.isKinematic && allowStateChange){
            door.isKinematic = false;
            OnDoorOpen.Invoke();
        }else if(!isPulled && !door.isKinematic){
            if(
                Mathf.Abs(doorHinge.angle) <= threshold || // current rotation is in closing angle
                ( lastAngle >=  threshold && doorHinge.angle <=  threshold ) || 
                ( lastAngle <= -threshold && doorHinge.angle >= -threshold ) // or crossed it
            ){
                door.isKinematic = true;
                transform.localRotation = restRotation; // doorHinge.angle = 0.0f;
                OnDoorClose.Invoke();
            }
        }

        lastAngle = doorHinge.angle;
    }

    public void SetPullState(bool state){
        isPulled = state;
    }
}

using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[RequireComponent(typeof(Rigidbody))]
public class Grabbable : MonoBehaviour {
    public ItemData data; // objects that are later evaluated (score-wise) should read this
    // if the grabbable does not have data, it should be ignored during evaluation

    private Vector3 stashedOffset;
    private HandType stashHand;

    private Rigidbody _rigidbody;
    private HashSet<HandGrabController> _hands = new HashSet<HandGrabController>();
    private Transform _originalParent = null;
    private Collider[] _colliders = new Collider[0];
    private GameObject[] _renderers = new GameObject[0]; // for outline applying
    private bool _isStashed;

    public int handCount { get { return _hands.Count; }}
    public new Rigidbody rigidbody { get { return _rigidbody; }}
    public Transform originalParent { get { return _originalParent; }}
    public Collider[] colliders { get { return _colliders; }}
    public GameObject[] renderers { get { return _renderers; }}
    public bool isStashed { get { return _isStashed; }}
    
    [Tooltip("If set the true, upon grab the object will be temporarily adopted by the player's transform and on release back to their original parent.")]
    public bool allowAdoption = true;

    [Tooltip("Allow the object to be grabbed (won't affect if it is already being held).")]
    public bool allowGrab = true;

    [Tooltip("Allow the item to be stashed in a kaban")]
    public bool allowStash = true;

    [Tooltip("Torque strength of the hand will be multiplied by this number upon grab")]
    public float torqueScale = 1.0f;

    [Tooltip("Weight to be added when inserted into kaban.")]
    public float weight = 1.0f;

#region Callbacks
    /// <summary>Called once the object is released</summary>
    /// <param name="hand">Hand that has released this object</param>
    public virtual void OnHandRelease(HandGrabController hand) {}
    /// <summary>Called once the object is grbbed</summary>
    /// <param name="hand">Hand that has grbbed this object</param>
    public virtual void OnHandGrab(HandGrabController hand) {}
#endregion

#region Internal Functions
    /// <summary>Internal function, handles the common logic of adoption/hand tracking</summary>
    /// <param name="hand">Hand in context</param>
    /// <param name="newParent">If adoption is allowed, temporarily give this grabbable to this transform</param>
    internal void _OnHandGrab(HandGrabController hand, Transform newParent){
        _hands.Add(hand);

        if(allowAdoption && transform.parent != newParent){
            _originalParent = transform.parent;
            transform.SetParent(newParent, true);
        }

        SetOutline(true);
        OnHandGrab(hand);
    }

    /// <summary>Internal function, handles the common logic of adoption/hand tracking</summary>
    /// <param name="hand">Hand in context</param>
    internal void _OnHandRelease(HandGrabController hand){
        _hands.Remove(hand);

        if(handCount == 0){
            if(allowAdoption && _originalParent != transform.parent) transform.SetParent(_originalParent, true);
            SetOutline(false);
        }

        OnHandRelease(hand);
    }

    /// <summary>Internal function, is called when the item is stored into Kaban. It is called before it is set as inactive.</summary>
    /// <param name="newParent">New parent that it will belong to. In this case, it should be the Kaban GameObject.</param>
    /// <param name="hand">Hand that stored it, stored to apply proper offset when destashing</param>
    internal void _OnStash(Transform newParent, HandType hand) {
        if(!allowStash) return;
        _originalParent = transform.parent;
        transform.SetParent(newParent, true);
        stashedOffset = transform.localPosition;
        _isStashed = true;
        stashHand = hand;
    }

    /// <summary>Internal function, is called when the item is requested from Kaban</summary>
    /// <param name="hand">Hand that is destashing it, to apply proper offset</param>
    internal void _OnDestash(HandType hand){
        transform.SetParent(_originalParent, true);
        _isStashed = false;

        if(hand == HandType.None) return; // ignore
        // TODO: this spazzes out and has invalid locations
        // if(hand != stashHand){ // not the same hand, invert X position
        //     stashedOffset.x *= -1f;
        //     transform.localPosition = stashedOffset;
        // }
    }

    /// <summary>Sets the outline to approapriate layer</summary>
    /// <param name="enabled">State of the outline</param>
    internal void SetOutline(bool enabled){
        int layer = enabled ? 10 : 7; // refer to project settings
        foreach(GameObject renderer in renderers) renderer.layer = layer;
    }
#endregion

#region Editor Related
    public void OnDrawGizmosSelected(){
        Gizmos.matrix = transform.localToWorldMatrix;

        Gizmos.color = Color.red;
        Gizmos.DrawRay(Vector3.zero, Vector3.right * 0.05f);

        Gizmos.color = Color.green;
        Gizmos.DrawRay(Vector3.zero, Vector3.up * 0.05f);

        Gizmos.color = Color.blue;
        Gizmos.DrawRay(Vector3.zero, Vector3.forward * 0.05f);

        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(_rigidbody.centerOfMass, 0.01f);
    }
#endregion

    void OnValidate(){
        Awake();
    }

    public void Awake(){
        _rigidbody = GetComponent<Rigidbody>();
        _colliders = GetComponentsInChildren<Collider>();
        _renderers = GetComponentsInChildren<Renderer>().Select(x => x.gameObject).ToArray();/*.Concat(
            GetComponentsInParent<Renderer>().Select(x => x.gameObject).ToArray()
        ).ToArray();*/
    }

    /// <summary>Forcefully releases itself from all hands</summary>
    public void ForceRelease(){
        foreach(HandGrabController hand in _hands.ToList()) hand.ReleaseGrab();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Grabbable)), CanEditMultipleObjects]
public class GrabbableEditor : Editor {
    override public void OnInspectorGUI(){
		DrawDefaultInspector();
		Grabbable grab = (Grabbable)target;
        GUILayout.Label($"Hand Count: {grab.handCount}");
        GUILayout.Label($"Original parent: {(grab.originalParent ? grab.originalParent.name : "none")}");

        GUILayout.Label($"{grab.colliders.Length} colliders, {grab.renderers.Length} renderers");
	}
}
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SoundOnCollision : MonoBehaviour{
    public SoundPool pool;
    public void OneShot(Vector3 position, AudioClip[] pool, float volume = 1.0f){
        AudioManager.instance.PlayRandomClip(pool, position, AudioManager.MixerOutput.Impact, volume);
    }

    public float softImpactThreshold = 0.5f;
    public float hardImpactThreshold = 2.0f;
    public bool scaleByMagnitude = true;
    public float volumeMagnitudeScale = 0.2f;

    private new Rigidbody rigidbody;
    private bool isColliding = false;
    private Vector3 last_check_velocity = Vector3.zero;

    void OnCollisionEnter(Collision col){ isColliding = true; }
    void OnCollisionExit(Collision col){ isColliding = false; }
    void Awake(){ rigidbody = GetComponent<Rigidbody>(); }

    void FixedUpdate(){
        if(!isColliding || rigidbody.IsSleeping()){ 
            return;
        }

        float magnitude = Vector3.Distance(last_check_velocity, rigidbody.velocity);
        if(magnitude >= softImpactThreshold){
            float volume = magnitude * volumeMagnitudeScale;
            if(volume <= 0.05f) return;
            if(magnitude >= hardImpactThreshold) OneShot(transform.position, pool.hard, scaleByMagnitude ? volume : 1.0f);
            else OneShot(transform.position, pool.soft, scaleByMagnitude ? volume : 1.0f);
        }
        // WorldSpaceIMGUI.Text(transform.position, magnitude.ToString("0.000"), gameObject.GetInstanceID().ToString());

        last_check_velocity = rigidbody.velocity;
    }
}

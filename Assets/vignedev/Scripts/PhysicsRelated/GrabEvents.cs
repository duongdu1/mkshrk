using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GrabEvents : Grabbable{
    public UnityEvent onHandGrab;
    public UnityEvent onHandRelease;

    // this is to allow the checkbox to be shown
    void Start() { }
    public override void OnHandGrab(HandGrabController hand){ if(enabled) onHandGrab.Invoke(); }
    public override void OnHandRelease(HandGrabController hand){ if(enabled) onHandRelease.Invoke(); }
}

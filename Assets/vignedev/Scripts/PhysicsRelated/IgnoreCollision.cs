using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollision : MonoBehaviour{
    Collider[] colliders;

    [SerializeField] Collider[] target;

    void Awake(){
        colliders = GetComponentsInChildren<Collider>();
        foreach(Collider c in colliders)
            foreach(Collider t in target)
                Physics.IgnoreCollision(c, t, true);
    }
}

using UnityEngine;
using TMPro;

public class Radio : Grabbable {
    [Header("Frequency Settings")]
    public float currentFrequency = 1000.0f;
    public Vector2 frequencyLimit = new Vector2(100f, 2000f);

    public float targetFrequency = 543.2f;
    public float leniency = 100.0f;
    public AnimationCurve leniencyFalloff = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

    [Header("Fake Emitter Settings")]
    public Transform emitter;
    public float maxDistance = 100.0f;
    public AnimationCurve distanceDropoff = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 0.0f);

    [Header("Control Speed")]
    public float turnSpeed = 1.0f;

    [Header("Resources")]
    [SerializeField] TMP_Text frequencyText;
    [SerializeField] AudioSource noiseSource;
    [SerializeField] AudioSource loopSource;
    [SerializeField] AudioClip toggleSound;

    [Header("Internals")]
    [SerializeField] bool isPlaying = false;

    // these AS are looping, only changing their volumes
    new void Awake() {
        base.Awake();
        if(!noiseSource || !loopSource){
            Debug.LogError("Missing AudioSources!", gameObject);
            enabled = false;
            return;
        }

        noiseSource.loop = true;
        loopSource.loop = true;

        UpdateVolumes();
    }

    void OnEnable(){
        SetPlayingState(isPlaying);
    }

    // keep track of the first hand
    HandGrabController firstHand;
    public override void OnHandGrab(HandGrabController hand) {
        if(!firstHand || handCount == 1) firstHand = hand;
    }
    public override void OnHandRelease(HandGrabController hand) {
        if(firstHand == hand || handCount == 0) firstHand = null;
    }

    // only call when it is being held
    private bool primaryLastFrame = false;
    void FixedUpdate(){
        UpdateVolumes();

        if(firstHand != null){
            currentFrequency = Mathf.Clamp(
                currentFrequency + firstHand.joystick.x * Time.deltaTime * turnSpeed,
                frequencyLimit.x, frequencyLimit.y
            );
            if(frequencyText) frequencyText.text = string.Format("{0:0.00} Hz", currentFrequency);
        }
        if(firstHand != null) {
            if(!primaryLastFrame && firstHand.primary) TogglePlayback();
            primaryLastFrame = firstHand.primary;
        }else{
            primaryLastFrame = false;
        }
    }

    // update volumes based on the distance from freq
    void UpdateVolumes() {
        float frequencyCorrectness = leniencyFalloff.Evaluate(Mathf.Clamp01(-(Mathf.Abs(currentFrequency - targetFrequency) / leniency) + 1.0f));
        float dropoff = distanceDropoff.Evaluate(Mathf.Clamp01(Vector3.Distance(emitter.position, transform.position) / maxDistance));
        
        // Debug.Log($"{frequencyCorrectness} {dropoff}");

        float finalClarity = frequencyCorrectness * dropoff;
        noiseSource.volume = 1.0f - (loopSource.volume = finalClarity);
    }

    [EasyButtons.Button]
    void TogglePlayback() {
        if(toggleSound) AudioManager.instance.PlayClipAtPoint(toggleSound, transform.position);
        SetPlayingState(!isPlaying);
    }

    void SetPlayingState(bool state){
        if(!state){
            noiseSource.Stop();
            loopSource.Stop();
        }else{
            noiseSource.Play();
            loopSource.Play();
        }
        isPlaying = state;
    }
}
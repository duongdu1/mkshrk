using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(HingeJoint), typeof(Rigidbody))]
public class HingeMonitor : MonoBehaviour{
    HingeJoint joint;
    new Rigidbody rigidbody;

    public float minAngle;
    public float maxAngle;
    private bool lastFrameInRange = false;

    public UnityEvent onRangeEnter;
    public UnityEvent onRangeExit;

    void Awake(){
        joint = GetComponent<HingeJoint>();
        rigidbody = GetComponent<Rigidbody>();
    }

    void Start() {
        float angle = joint.angle;
        lastFrameInRange = angle >= minAngle && angle <= maxAngle;
    }

    void FixedUpdate(){
        if(rigidbody.IsSleeping()) return;

        float angle = joint.angle;
        bool inRange = angle >= minAngle && angle <= maxAngle;
        if(!lastFrameInRange && inRange){
            onRangeEnter.Invoke();
        }else if(lastFrameInRange && !inRange){
            onRangeExit.Invoke();
        }
        lastFrameInRange = inRange;
    }
}

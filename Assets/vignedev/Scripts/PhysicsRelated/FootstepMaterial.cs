using System;
using UnityEngine;

class FootstepMaterial : MonoBehaviour {
    [Serializable] public struct MaterialAssociation {
        public MaterialType type;   
        public AudioClipCollection clips;
    }

    [SerializeField] MaterialType _type;
    public MaterialType type => _type;
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidbodyExpose : MonoBehaviour{
    new Rigidbody rigidbody;

    void OnEnable(){
        if(!rigidbody) rigidbody = GetComponent<Rigidbody>();
    }

    void Awake() {
        if(!rigidbody) rigidbody = GetComponent<Rigidbody>();
    }

    public void ResetVelocity(){
        SetVelocity(Vector3.zero);
    }

    public void SetVelocity(Vector3 newVelocity){
        rigidbody.velocity = newVelocity;
    }
}

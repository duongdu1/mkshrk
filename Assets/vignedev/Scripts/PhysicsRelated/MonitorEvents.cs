using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MonitorEvents : MonoBehaviour{
    public UnityEvent onMonitorEnter;
    public UnityEvent onMonitorExit;

    void OnMonitorEnter(){
        onMonitorEnter.Invoke();
    }
    void OnMonitorExit(){
        onMonitorExit.Invoke();
    }
}

using UnityEngine;

class KabanPickupGesture : HandGrabGesture {
    Kaban kaban;
    [SerializeField] KabanGrabGesture gesture;

    void Awake(){
        kaban = GetComponentInParent<Kaban>();

        if(!gesture) {
            gesture = GameManager.instance.player.gameObject.GetComponentInChildren<KabanGrabGesture>();
            if(!gesture){
                Debug.LogError("KabanGrabGesture is not assigned!", this);
                enabled = false;
            }
        }else{
            if(gesture.assignedKaban && gesture.assignedKaban != kaban){
                Debug.LogError("Multiple Kabans in scene?", this);
                enabled = false;
            }else{
                gesture.assignedKaban = kaban;
            }
        }
    }

    public override void OnHandGrab(HandGrabController hand){
        hand.ForgetGrabbable(gesture.assignedKaban);
        hand.ForgetGesture(this);
        kaban.ForceRelease();
        kaban.ResetHands();
        kaban.gameObject.SetActive(false);
    }
}
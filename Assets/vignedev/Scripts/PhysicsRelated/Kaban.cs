using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

public class Kaban : Grabbable {
    [SerializeField] HashSet<Grabbable> _grabbables = new HashSet<Grabbable>();
    [SerializeField] List<Grabbable> _stored = new List<Grabbable>();
    [SerializeField] float stashingPeriod = 0.1f;
    [SerializeField] HandGrabController currentHand;

    public HashSet<Grabbable> grabbables { get { return _grabbables; } }
    public List<Grabbable> stored { get { return _stored; } }

    public Vector3 center;
    public float baseWeight = 1.0f;
    public float maxHandDistance = 1.0f;
    public float weightLimit = 60f;
    public float resetHeight = -50f;

    [Tooltip("Pushes out objects that are out of the weight limit")]
    [SerializeField] float pushoutForce = 100.0f;

    private float _stashedWeight = 0.0f;
    public float stashedWeight => _stashedWeight;

    public AudioClip onStashAudio;
    public AudioClip onDestashAudio;

    new void Awake(){
        base.Awake(); // oh great, awake is called as a message only...
        allowStash = false;
        allowAdoption = false;
    }

    void OnEnable(){
        StartCoroutine(PeriodicalStasher());
    }

    void OnTriggerEnter(Collider other){
        if(other.isTrigger) return;

        Grabbable grabbable = other.GetComponentInParent<Grabbable>();
        if(grabbable && grabbable.allowStash) _grabbables.Add(grabbable);

        if(currentHand == null){
            HandGrabController hand = other.GetComponentInParent<HandGrabController>();
            if(hand){
                currentHand = hand;
                currentHand.kaban = this;
            }
        }
    }

    void OnTriggerExit(Collider other){
        if(other.isTrigger) return;

        Grabbable grabbable = other.GetComponentInParent<Grabbable>();
        if(grabbable) _grabbables.Remove(grabbable);

        if(currentHand != null){
            HandGrabController hand = other.GetComponentInParent<HandGrabController>();
            if(currentHand == hand){
                currentHand.kaban = null;
                currentHand = null;
            }
        }
    }

    void Update(){
        if(
            GameManager.instance.player == null ||
            Vector3.Distance(GameManager.instance.player.transform.position, transform.position
        ) >= 1.0f) return;

        WorldSpaceIMGUI.Text(transform, $"{stashedWeight.ToString("0.00")}/{weightLimit.ToString("0.00")}");
        // WorldSpaceIMGUI.Text(transform, $"\n\ndist={}");

        int index; float depth;
        if(_stored.Count != 0 && GetHandStoredIndex(out index, out depth)){
            Grabbable item = _stored[index];
            WorldSpaceIMGUI.Text(transform, $"\n{item.data?.name ?? "Unknown"}");
        }else{
            // WorldSpaceIMGUI.Text(transform, $"\n\n<handless>");
        }
    }

    void FixedUpdate(){
        if(transform.position.y <= resetHeight) {
            Debug.LogWarning("Kaban got out of bounds, restoring it...");
            rigidbody.MovePosition(GameManager.instance.player.transform.position);
        }
    }

    bool GetHandProgress(out float progress){
        if(currentHand){
            float distance = (center - transform.InverseTransformVector(currentHand.transform.position)).y;
            
            // remap to proper values (and clamp them)
            float normal = Mathf.InverseLerp(0.0f, maxHandDistance, distance);
            progress = 1.0f - Mathf.Lerp(0.0f, 1.0f, normal);
            // progress would be normalized depth from top 

            return true;
        }
        progress = 0.0f;
        return false;
    }

    bool GetHandStoredIndex(out int index, out float depth){
        float progress;
        if(GetHandProgress(out progress)){
            int itemIndex = (int)((_stored.Count - 1) * progress);
            itemIndex = Math.Max(Math.Min(itemIndex, _stored.Count - 1), 0);

            index = itemIndex;
            depth = progress;
            return true;  
        }

        index = -1;
        depth = progress;
        return false;
    }

    public void RetrieveItem(){
        int index;
        if(GetHandStoredIndex(out index, out _))
            RetrieveItem(index);
    }

    public void RetrieveItem(int index){
        if(!currentHand) return;
        if(_stored.Count == 0) return;

        Grabbable item = _stored[index];
        _stored.RemoveAt(index);
        _stashedWeight -= item.weight;

        item._OnDestash(currentHand.handType);
        item.gameObject.SetActive(true);
        currentHand.AttemptGrab(item);

        AudioManager.instance.PlayClipAtPoint(onDestashAudio, transform.position);
    }

    public void ResetHands(){
        if(currentHand) currentHand.kaban = null;
        currentHand = null;
    }

    IEnumerator PeriodicalStasher(){
        while(true){
            foreach(Grabbable grabbable in _grabbables.ToArray()){
                if(!grabbable.allowStash){ // just for sanity, should never happen
                    _grabbables.Remove(grabbable);
                    continue;
                }
                
                // check if weight/volume limit would be overshot
                if(grabbable.weight + _stashedWeight > weightLimit){
                    grabbable.rigidbody.AddForce(transform.up * pushoutForce * grabbable.weight, ForceMode.Force);
                    continue;
                }

                if(grabbable.handCount == 0){ // is released, at this point all should be reset
                    _grabbables.Remove(grabbable);

                    _stored.Add(grabbable);
                    _stashedWeight += grabbable.weight;
                    grabbable._OnStash(transform, currentHand ? currentHand.handType : HandType.None);
                    grabbable.gameObject.SetActive(false);

                    AudioManager.instance.PlayClipAtPoint(onStashAudio, transform.position);
                }
            }
            yield return new WaitForSecondsRealtime(stashingPeriod);
        }
    }

    new void OnDrawGizmosSelected(){
        base.OnDrawGizmosSelected();

        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireSphere(center, 0.01f);
        Gizmos.DrawLine(center, center - Vector3.up * maxHandDistance);
        Gizmos.DrawWireSphere(center - Vector3.up * maxHandDistance, 0.01f);
    }

    private static Kaban _instance;
    public static Kaban current { get {
        if(_instance == null) _instance = GameObject.FindGameObjectWithTag("Kaban")?.GetComponent<Kaban>();
        if(_instance) return _instance;
        return _instance = FindObjectOfType<Kaban>();
    } }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Kaban))]
public class KabanEditor : Editor {
    override public void OnInspectorGUI(){
		DrawDefaultInspector();
		Kaban kaban = (Kaban)target;
        
        GUILayout.Label($"Weight: {kaban.stashedWeight}/{kaban.weightLimit}");
        GUILayout.Label("Grabbables:");
        foreach(Grabbable g in kaban.grabbables)
            GUILayout.Label($"{g.gameObject.name}");
	}
}
#endif
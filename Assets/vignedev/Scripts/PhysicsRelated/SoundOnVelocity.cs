using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(SoundOnVelocity))]
class SoundOnVelocityEditor : Editor {
    override public void OnInspectorGUI(){
		DrawDefaultInspector();
		SoundOnVelocity grab = (SoundOnVelocity)target;
        GUILayout.Label($"Magnitude: {grab.magnitude}");
	}
}
#endif

[RequireComponent(typeof(AudioSource), typeof(Rigidbody))]
public class SoundOnVelocity : MonoBehaviour{
    public enum MonitoredProperty {
        Velocity, Angular
    }

    private AudioSource source;
    private new Rigidbody rigidbody;

    public float minVelocity = 0.1f;
    public float maxVelocity = 1f;
    public MonitoredProperty prop;
    public AnimationCurve volumeRemap = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);
    public float targetVolume = 0.0f;

    [HideInInspector] public float magnitude = 0.0f;

    void Awake(){
        source = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate(){
        if(!rigidbody.IsSleeping()){
            magnitude = (prop == MonitoredProperty.Velocity ? rigidbody.velocity : rigidbody.angularVelocity).magnitude;
            if(magnitude < minVelocity){
                source.Pause();
            }else if(!source.isPlaying){ // (&& magnitude >= minVelocity)
                targetVolume = 0.0f; // is updated below, this is to prevent residual volumes
                if(!source.isPlaying) source.Play();
            }

            if(source.isPlaying) targetVolume = volumeRemap.Evaluate((magnitude - minVelocity) * maxVelocity);
        }

        source.volume = Mathf.Lerp(source.volume, targetVolume, 0.2f);
    }
}

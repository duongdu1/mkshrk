using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivateOnImpact : MonoBehaviour{
    public float threshold = 1.0f;

    public UnityEvent action;
    public bool triggerOnce = true;
    public bool wasTriggered = false;

    [SerializeField] private bool printDebug = false;
    
    [Header("Normal Matching")]
    public bool matchNormal = false;
    public Vector3 normal = Vector3.up;
    public float angleLeniency = 45f;

    [Header("Filter by Body")]
    public bool filterBodies = false;
    public List<Rigidbody> permittedBodies = new List<Rigidbody>();

    void OnCollisionEnter(Collision col){
        if(triggerOnce && wasTriggered) return;
        float impulse = col.impulse.magnitude / Time.fixedDeltaTime;
        if(printDebug) Debug.Log(impulse, this);

        if(filterBodies && !permittedBodies.Contains(col.rigidbody)) return;

        if(impulse >= threshold){
            if(matchNormal && col.contactCount >= 1){
                ContactPoint point = col.GetContact(0);
                if(Vector3.Angle(normal, point.normal) <= angleLeniency) Invoke();
            }else{
                Invoke();
            }
        }
    }

    private void Invoke(){
        action.Invoke();
        wasTriggered = true;
    }
}

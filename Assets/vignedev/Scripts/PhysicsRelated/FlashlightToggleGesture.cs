using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightToggleGesture : HandGrabGesture {
    public bool hasFlashlight = true;
    [SerializeField] GameObject model;
    [SerializeField] GameObject flashLight;

    void Start(){
        UpdateModelVisibility();
    }

    public void UpdateModelVisibility(){
        model.SetActive(hasFlashlight);
    }

    public override void OnHandGrab(HandGrabController hand) {
        flashLight.SetActive(!flashLight.activeSelf);
    }
}

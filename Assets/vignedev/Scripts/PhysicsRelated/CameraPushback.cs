using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class CameraPushback : MonoBehaviour{
    new SphereCollider collider;
    PlayerController player;

    [SerializeField] float radius = 0.1f;
    [SerializeField] LayerMask layerMask;

    [SerializeField] Collider[] colliders = new Collider[32];

    void Awake(){
        collider = GetComponent<SphereCollider>();
        collider.center = Vector3.zero;
        collider.radius = radius;
        player = GetComponentInParent<PlayerController>();

        collider.center = Vector3.zero;
        collider.radius = radius;
    }

    // this time Update is important to have correct state each frame
    void Update(){
        int found = Physics.OverlapSphereNonAlloc(transform.position, radius, colliders, layerMask, QueryTriggerInteraction.Ignore);
        for(int i = 0; i < found; ++i){
            var context = colliders[i];
            if(context == collider) continue;

            // for each collision, find out how much we are overlapping and the normal of overlap
            Vector3 direction; float distance;
            // vig: Unity reports Terrain as the collider, however ComputePenetration ignores trees?
            if(Physics.ComputePenetration(
                collider, transform.position, transform.rotation,
                context, context.transform.position, context.transform.rotation,
                out direction, out distance
            )){
                Vector3 offset = direction * distance;
                offset.y = 0; // flatten the vector, assume we aren't like hitting the roof or floor

                player.controller.Move(offset);
                player.controller.center -= offset; // push the player back to maintain the same offset
                // so in theory we moved only the camera back, faking the collision in a way
            }
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
        Gizmos.DrawLine(transform.position, transform.parent.position);
        Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.1f);
        Gizmos.DrawSphere(transform.position, radius);
    }
}

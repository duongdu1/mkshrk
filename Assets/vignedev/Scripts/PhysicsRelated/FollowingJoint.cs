using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]
public class FollowingJoint : MonoBehaviour{
    [Serializable]
    public struct JointDriveSerializable {
        public float positionSpring;
        public float positionDamper;
        public float maximumForce;
    }

    ConfigurableJoint joint;
    new Rigidbody rigidbody;
    JointDrive positionDrive;
    JointDrive angularDrive;

    [Tooltip("The target it should follow")]
    public Rigidbody target;

    [Tooltip("The distance it is allowed to deviate before snap")]
    public float maxDistance = 0.5f;

    [Tooltip("Whether the FixedUpdate loop should keep updating the values as below. Otherwise it will be only set on startup.")]
    public bool liveUpdates = false;

    public JointDriveSerializable positionDriveSettings = new JointDriveSerializable() {
        positionSpring = 3_000f,
        positionDamper = 0.0f,
        maximumForce = Mathf.Infinity
    };
    public JointDriveSerializable angularDriveSettings = new JointDriveSerializable() {
        positionSpring = 3_000f,
        positionDamper = 0.0f,
        maximumForce = Mathf.Infinity
    };
    public float positionMassScale = 1.0f;
    public float angularMassScale = 1.0f;
    public RotationDriveMode rotationDriveMode = RotationDriveMode.Slerp;

    private HandGrabController grabController;

    void Awake() {
        joint = GetComponent<ConfigurableJoint>();
        rigidbody = GetComponent<Rigidbody>();
        grabController = GetComponent<HandGrabController>();

        if(!target){
            Debug.LogError("No target set, disabling.", this);
            enabled = false;
        }

        UpdateJoint();
    }

    public void UpdateJoint(){
        positionDrive.positionSpring = positionDriveSettings.positionSpring;
        positionDrive.positionDamper = positionDriveSettings.positionDamper;
        positionDrive.maximumForce = positionDriveSettings.maximumForce * positionMassScale; 

        angularDrive.positionSpring = angularDriveSettings.positionSpring;
        angularDrive.positionDamper = angularDriveSettings.positionDamper;
        angularDrive.maximumForce = angularDriveSettings.maximumForce * angularMassScale;

        joint.angularXDrive = joint.angularYZDrive = joint.slerpDrive = angularDrive;
        joint.xDrive = joint.yDrive = joint.zDrive = positionDrive;

        joint.rotationDriveMode = rotationDriveMode;

        joint.connectedBody = target;
    }

    void FixedUpdate(){
        if(liveUpdates) UpdateJoint();

        float distance = Vector3.Distance(target.position, rigidbody.position);
        if(distance >= maxDistance){
            if(grabController) grabController.ReleaseGrab();
            rigidbody.MovePosition(target.position);
            rigidbody.MoveRotation(target.rotation);
            rigidbody.velocity = Vector3.zero;
        }
    }
}

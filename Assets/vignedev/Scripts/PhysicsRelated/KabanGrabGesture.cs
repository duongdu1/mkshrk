using UnityEngine;
using UnityEditor;

class KabanGrabGesture : HandGrabGesture {
    public Kaban assignedKaban;
    public HandType handLimit = HandType.None;
    
    public Vector3 grabHandlePosition;
    public Quaternion grabHandleRotation = Quaternion.identity;

    void Awake() {
        if(!assignedKaban) assignedKaban = Kaban.current;
        if(!assignedKaban){
            Debug.LogWarning("No kaban has been assigned, is it non-existant?", this);
            gameObject.SetActive(false);
        }
    }

    public override void OnHandGrab(HandGrabController hand){
        if(assignedKaban.gameObject.activeSelf) return; // kaban is not hidden
        if(handLimit != HandType.None && handLimit != hand.handType) return;
        
        assignedKaban.ResetHands();
        assignedKaban.gameObject.SetActive(true);
        assignedKaban.rigidbody.position = assignedKaban.transform.position = hand.transform.position + hand.transform.TransformVector(grabHandlePosition);
        assignedKaban.rigidbody.rotation = assignedKaban.transform.rotation = hand.transform.rotation * grabHandleRotation; // TODO: maybe ignore rotation?
        hand.AttemptGrab(assignedKaban);
        hand.ForgetGesture(this);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(KabanGrabGesture))]
class KabanGrabGestureEditor : Editor {
    protected virtual void OnSceneGUI(){
        KabanGrabGesture gesture = (KabanGrabGesture)target;

        if(!gesture.assignedKaban) return;
        

        EditorGUI.BeginChangeCheck();
        Handles.matrix = gesture.assignedKaban.transform.localToWorldMatrix;
        gesture.grabHandlePosition = Handles.PositionHandle(gesture.grabHandlePosition, gesture.grabHandleRotation);
        gesture.grabHandleRotation = Handles.RotationHandle(gesture.grabHandleRotation, gesture.grabHandlePosition);
        if (EditorGUI.EndChangeCheck()){
            Undo.RecordObject(target, "Update Kaban Grab Position");
        }
    }
}
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(MonitorDistance))]
class MonitorDistanceEditor : Editor{
    public override void OnInspectorGUI(){
        base.DrawDefaultInspector();

        MonitorDistance dist = ((MonitorDistance)target);
        float distance = Vector3.Distance(dist.initialLocalPosition, dist.transform.localPosition);
        GUILayout.Label($"{distance}");
        GUILayout.Label($"distance >= minDistance = {distance >= dist.minDistance}");
        GUILayout.Label($"distance <= maxDistance = {distance <= dist.maxDistance}");
    }
}
#endif
public class MonitorDistance : MonoBehaviour{
    private Vector3 _initialLocalPosition;
    public Vector3 initialLocalPosition => _initialLocalPosition;

    public float minDistance = 0.0f;
    public float maxDistance = 0.1f;
    private bool lastFrameInRange = false;

    public UnityEvent onRangeEnter;
    public UnityEvent onRangeExit;

    void Awake(){
        UpdateInitial();
    }

    void Start() {
        float distance = Vector3.Distance(_initialLocalPosition, transform.localPosition);
        lastFrameInRange = distance >= minDistance && distance <= maxDistance;
    }

    public void UpdateInitial(){
        _initialLocalPosition = transform.localPosition;
    }

    void FixedUpdate(){
        float distance = Vector3.Distance(_initialLocalPosition, transform.localPosition);
        bool inRange = distance >= minDistance && distance <= maxDistance;
        if(!lastFrameInRange && inRange){
            onRangeEnter.Invoke();
        }else if(lastFrameInRange && !inRange){
            onRangeExit.Invoke();
        }
        lastFrameInRange = inRange;
    }
}

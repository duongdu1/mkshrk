using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(ConfigurableJointHelper))]
class ConfigurableJointHelperEditor : Editor {
    protected virtual void OnSceneGUI(){
        ConfigurableJointHelper helper = (ConfigurableJointHelper)target;
        ConfigurableJoint joint = helper.joint;

        if(!joint) return;

        Rigidbody body = joint.connectedBody;
        Transform transform = joint.transform;

        Vector3 world_anchor = transform.TransformPoint(joint.anchor);
        Vector3 world_connectedAnchor = body.transform.TransformPoint(joint.connectedAnchor);

        Handles.DrawWireCube(world_anchor, Vector3.one * 0.01f);

        Handles.color = Color.red;
        Handles.DrawWireCube(world_connectedAnchor, Vector3.one * 0.01f);

        EditorGUI.BeginChangeCheck();
        Vector3 newAnchor = transform.InverseTransformPoint(Handles.PositionHandle(world_anchor, transform.rotation));
        Vector3 newConnectedAnchor = body.transform.InverseTransformPoint(Handles.PositionHandle(world_connectedAnchor, transform.rotation));

        Handles.color = Color.yellow;
        float newLimit = Handles.RadiusHandle(transform.rotation, world_connectedAnchor, joint.linearLimit.limit, false);

        if (EditorGUI.EndChangeCheck()){
            Undo.RecordObject(joint, "Change Joint Settings");

            joint.anchor = newAnchor;
            joint.connectedAnchor = newConnectedAnchor;

            SoftJointLimit limit = joint.linearLimit;
            limit.limit = newLimit;
            joint.linearLimit = limit;
        }

        Vector3 offset = Vector3.zero;
        Vector3 direction = Vector3.zero;

        if(joint.xMotion == ConfigurableJointMotion.Limited){
            offset = transform.right * joint.linearLimit.limit;
            direction = Vector3.right;
            Handles.color = Handles.xAxisColor;

            Vector3 start = world_connectedAnchor - offset, end = world_connectedAnchor + offset;
            Handles.DrawLine(start, end, 3);
        }

        if(joint.yMotion == ConfigurableJointMotion.Limited){
            offset = transform.up * joint.linearLimit.limit;
            direction = Vector3.up;
            Handles.color = Handles.yAxisColor;
            
            Vector3 start = world_connectedAnchor - offset, end = world_connectedAnchor + offset;
            Handles.DrawLine(start, end, 3);
        }

        if(joint.zMotion == ConfigurableJointMotion.Limited){
            offset = transform.forward * joint.linearLimit.limit;
            direction = Vector3.forward;
            Handles.color = Handles.zAxisColor;
            
            Vector3 start = world_connectedAnchor - offset, end = world_connectedAnchor + offset;
            Handles.DrawLine(start, end, 3);
        }        
    }
}
#endif

[RequireComponent(typeof(ConfigurableJoint))]
public class ConfigurableJointHelper : MonoBehaviour {
    public ConfigurableJoint joint;
}
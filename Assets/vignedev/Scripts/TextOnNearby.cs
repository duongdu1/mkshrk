using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextOnNearby : MonoBehaviour{
    new Camera camera;
    PlayerController player;

    public Collider relativeToCollider;
    public float distance = 1.0f;
    public string text;

    public bool useCamera;

    void Start(){
        player = GameManager.instance.player;
        camera = Camera.main;
    }

    void Update(){
        Vector3 target = useCamera ? camera.transform.position : player.transform.position;
        Vector3 closestPos = relativeToCollider ? relativeToCollider.ClosestPoint(target) : transform.position;
        if(Vector3.Distance(target, closestPos) <= distance) {
            WorldSpaceIMGUI.Text(closestPos, text);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeKillEvent : MonoBehaviour{
    public float range = 1.0f;
    public string reason = "Minefield";

    public void Trigger(){
        if(Vector3.Distance(GameManager.instance.player.transform.position, transform.position) <= range)
            GameManager.instance.player.Kill(reason);
    }

    void OnDrawGizmosSelected(){
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(Vector3.zero, range);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GenerateMeshCollidersFromList : MonoBehaviour{
    public List<Mesh> meshes;
    public bool isTrigger;

    public LayerMask include;
    public LayerMask exclude;

    [EasyButtons.Button]
    void GenerateColliders(){
        MeshCollider[] colliders = GetComponents<MeshCollider>();
        foreach(MeshCollider c in colliders) DestroyImmediate(c);

        foreach(Mesh mesh in meshes){
            MeshCollider collider = gameObject.AddComponent<MeshCollider>();
            collider.convex = true;
            collider.sharedMesh = mesh;
            collider.isTrigger = isTrigger;

            collider.includeLayers = include;
            collider.excludeLayers = exclude;
        }
    }
}

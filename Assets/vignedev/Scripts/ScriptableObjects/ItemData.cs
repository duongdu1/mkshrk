using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Scriptable Objects/Item", order = 1)]
public class ItemData : ScriptableObject {
    [Serializable]
    public struct Property {
        public ItemProperty type;
        public float value;
    }
    public new string name;
    public List<Property> properties = new List<Property>();
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Scriptable Objects/Sound Pool", order = 1)]
public class SoundPool : ScriptableObject {
    public AudioClip[] soft;
    public AudioClip[] hard;
    public AudioClip[] drag;
}

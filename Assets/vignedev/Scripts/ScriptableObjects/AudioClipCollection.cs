using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Scriptable Objects/Audio Clip Collection", order = 1)]
public class AudioClipCollection : ScriptableObject {
    public AudioClip[] clips;
}

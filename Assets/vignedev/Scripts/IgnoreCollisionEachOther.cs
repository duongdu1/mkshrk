using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollisionEachOther : MonoBehaviour{
    Collider[] colliders;

    void Awake(){
        colliders = GetComponentsInChildren<Collider>();
        foreach(Collider c in colliders)
            foreach(Collider t in colliders)
                Physics.IgnoreCollision(c, t, true);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
    using UnityEditor.IMGUI.Controls;
    [CustomEditor(typeof(ReflectionProbeHelper))]
    class ReflectionProbeHelperEditor : Editor {
        private BoxBoundsHandle handles = new BoxBoundsHandle();

        protected virtual void OnSceneGUI(){
            ReflectionProbeHelper helper = (ReflectionProbeHelper)target;
            ReflectionProbe probe = helper.probe;
            handles.center = probe.center + probe.transform.position;
            handles.size = probe.size - Vector3.one * probe.blendDistance * 2;

            EditorGUI.BeginChangeCheck();
            handles.DrawHandle();
            if (EditorGUI.EndChangeCheck()){
                Undo.RecordObject(probe, "Probe Boundary Changed");

                probe.center = handles.center - probe.transform.position;
                probe.size = handles.size + Vector3.one * probe.blendDistance * 2;
            }
        }
    }
#endif

[RequireComponent(typeof(ReflectionProbe))]
public class ReflectionProbeHelper : MonoBehaviour {
    ReflectionProbe _probe;
    public ReflectionProbe probe => _probe;

    void OnValidate(){
        _probe = GetComponent<ReflectionProbe>();
    }
}
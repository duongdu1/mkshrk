using UnityEngine;

public class DummyScenario : Scenario {
    public override void CalculateResult(ref Result result){
        result.score = result.alive ? (
            1.0f * result.properties[ItemProperty.Cold] +
            1.0f * result.properties[ItemProperty.Food] +
            1.0f * result.properties[ItemProperty.Water] +
            1.0f * result.properties[ItemProperty.Resources]
        ) / ((1.0f + result.duration)) : 0;
        base.CalculateResult(ref result);
    }
}
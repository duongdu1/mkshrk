using UnityEngine;
using System.Collections.Generic;

public class StoreScenario : Scenario {
    public Dictionary<ItemProperty, float> additions = new Dictionary<ItemProperty, float>() {
        {ItemProperty.Cold, 0},
        {ItemProperty.Food, 0},
        {ItemProperty.Resources, 0},
        {ItemProperty.Water, 0}
    };

    public int brokenWindows = 0;
    public bool _climbedUnderFence = false;
    public bool _hasProppedShutterDoor = false;
    public bool _hasReachedBasement = false;
    public bool climbedUnderFence {
        get { return _climbedUnderFence; }
        set {_climbedUnderFence = value; }
    }
    public bool hasProppedShutterDoor {
        get { return _hasProppedShutterDoor; }
        set {_hasProppedShutterDoor = value; }
    }
    public bool hasReachedBasement {
        get { return _hasReachedBasement; }
        set {_hasReachedBasement = value; }
    }

    public void IncrementBrokenWindows(){
        ++brokenWindows;
    }

    public override void CalculateResult(ref Result result){
        additions[ItemProperty.Resources] = brokenWindows * 10f;
        if(climbedUnderFence) additions[ItemProperty.Resources] += 30f;
        if(hasProppedShutterDoor) additions[ItemProperty.Resources] += 20f;
        if(hasReachedBasement) additions[ItemProperty.Resources] += 100f;

        result.score = result.alive ? (
            2.0f   * (result.properties[ItemProperty.Cold]      += additions[ItemProperty.Cold     ]) +
            3.0f   * (result.properties[ItemProperty.Food]      += additions[ItemProperty.Food     ]) +
            5.0f  * (result.properties[ItemProperty.Water]      += additions[ItemProperty.Water    ]) +
            100.0f * (result.properties[ItemProperty.Resources] += additions[ItemProperty.Resources])
        ) / (1.0f + result.duration/6000.0f) : 0;
        base.CalculateResult(ref result);
    }
}
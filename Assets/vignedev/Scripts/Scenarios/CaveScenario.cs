using UnityEngine;
using System.Collections.Generic;

public class CaveScenario : Scenario {
    public WaterContainerFillController waterContainer;
    public float literVolume = 50.0f;

    // reuse this because laziness and panicness, but hey, it will work
    public List<ItemData> endings = new List<ItemData>(); 

    public Dictionary<ItemProperty, float> additions = new Dictionary<ItemProperty, float>() {
        {ItemProperty.Cold, 0},
        {ItemProperty.Food, 0},
        {ItemProperty.Resources, 0},
        {ItemProperty.Water, 0}
    };

    public override void CalculateResult(ref Result result){
        result.score = result.alive ? (
            1.0f   * (result.properties[ItemProperty.Cold]      += additions[ItemProperty.Cold     ]) +
            1.0f   * (result.properties[ItemProperty.Food]      += additions[ItemProperty.Food     ]) +
            10.0f  * (result.properties[ItemProperty.Water]     += additions[ItemProperty.Water    ] + waterContainer.fillRate * literVolume * 1_000) +
            100.0f * (result.properties[ItemProperty.Resources] += additions[ItemProperty.Resources])
        ) / (1.0f + result.duration/3000.0f) : 0;
        base.CalculateResult(ref result);
    }

    public void ExitWithValuesInIndex(int index){
        foreach(var field in endings[index].properties){
            additions[field.type] += field.value;
        }
        base.PlayerLeave();
    }
}
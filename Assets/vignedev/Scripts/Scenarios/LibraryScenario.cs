using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

#if UNITY_EDITOR
    using UnityEditor;
    using UnityEditor.IMGUI.Controls;
    using EasyButtons.Editor;

    [CustomEditor(typeof(LibraryScenario)), CanEditMultipleObjects]
    public class LibraryScenarioEditor : Editor {
        private BoxBoundsHandle[] handles;
        private ButtonsDrawer drawer;

        void OnEnable() {
            drawer = new ButtonsDrawer(target);
        }

        protected virtual void OnSceneGUI() {
            LibraryScenario helper = (LibraryScenario)target;

            // Handles.matrix = helper.transform.localToWorldMatrix;

            if(handles == null || handles.Length != helper.zones.Count){
                handles = new BoxBoundsHandle[helper.zones.Count];
                for(int i = 0; i < handles.Length; ++i) handles[i] = new BoxBoundsHandle();
            }

            for(int i = 0; i < handles.Length; ++i){
                handles[i].center = helper.zones[i].bounds.center;
                handles[i].size = helper.zones[i].bounds.size;
                handles[i].SetColor(helper.zones[i].color);
            }

            EditorGUI.BeginChangeCheck();
            foreach(var handle in handles) handle.DrawHandle();
            
            if(EditorGUI.EndChangeCheck()){
                Undo.RecordObjects(new UnityEngine.Object[] { helper }, "Updated temperature zones");
                for(int i = 0; i < handles.Length; ++i){
                    helper.zones[i].bounds.center = handles[i].center;
                    helper.zones[i].bounds.size = handles[i].size;
                }
            }
        }

        override public void OnInspectorGUI(){
            DrawDefaultInspector();
            drawer.DrawButtons(targets);
        }
    }
#endif

public class LibraryScenario : Scenario {
    [Serializable] public class TemperatureZone {
        public Color color = Color.red;
        public Bounds bounds;
        public float rate;
        public int priority = 0;
    }

    [SerializeField] float heartbeatInterval;

    private float initialBodyTemperature = 0.0f;    
    public float bodyTemperature = 37.4f;
    public float hypothermia = 35.0f;

    public float decreaseRate = 0.1f;

    [SerializeField] TMP_Text watchFace;
    public string format = "{0} °C";

    public override void CalculateResult(ref Result result){
        result.score = result.alive ? (
            3f * result.properties[ItemProperty.Cold] +
            1.5f * result.properties[ItemProperty.Food] +
            1.2f * result.properties[ItemProperty.Water] +
            2.0f * result.properties[ItemProperty.Resources]
        ) / ((1.0f + result.duration) * (1.0f + Mathf.Abs(initialBodyTemperature - bodyTemperature))) : 0;
        // abs just to be sure, in case we want to warm the player up somehow
    }

    void OnEnable() {
        initialBodyTemperature = bodyTemperature;
        StartCoroutine(UpdateTemperature());
    }

    // dont call every frame, perform like every heart beat
    public List<TemperatureZone> zones = new List<TemperatureZone>();
    IEnumerator UpdateTemperature() {
        while(true){
            TemperatureZone zone = null;
            Vector3 position = GameManager.instance.player.GetWorldPosition();
            foreach(var z in zones){
                if(z.bounds.Contains(position)){
                    if(zone == null || zone.priority < z.priority)
                        zone = z;
                }
            }

            bodyTemperature -= zone == null ? decreaseRate : zone.rate;
            watchFace.text = String.Format(format, bodyTemperature);
            yield return new WaitForSeconds(heartbeatInterval);

            if(bodyTemperature < hypothermia){
                yield return new WaitForSeconds(5.0f); // few seconds before death
                EndScenario(EndReason.Death, "Hypothermia");
            }
        }
    }
}
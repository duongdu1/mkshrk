using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPoolOnDemand : MonoBehaviour{
    public AudioClip overrideClip; // TODO: separate to a separate class
    public SoundPool pool;
    public float volume = 1.0f;

    public void PlaySoft(){
        if(overrideClip) AudioManager.instance.PlayClipAtPoint(overrideClip, transform.position, volume);
        else AudioManager.instance.PlayRandomClip(pool.soft, transform.position, volume);
    }

    public void PlayHard(){
        if(overrideClip) AudioManager.instance.PlayClipAtPoint(overrideClip, transform.position, volume);
        else AudioManager.instance.PlayRandomClip(pool.hard, transform.position, volume);
    }

    public void PlayDrag(){
        if(overrideClip) AudioManager.instance.PlayClipAtPoint(overrideClip, transform.position, volume);
        else AudioManager.instance.PlayRandomClip(pool.drag, transform.position, volume);
    }

    public void PlayOverride(float volume){
        AudioManager.instance.PlayClipAtPoint(overrideClip, transform.position, volume);
    }

    public void PlayOverride(AudioClip clip){
        overrideClip = clip;
        AudioManager.instance.PlayClipAtPoint(overrideClip, transform.position, volume);
    }

    public void PlayOverrideVelocityScale(float volume){
        AudioManager.instance.PlayClipAtPoint(overrideClip, transform.position, volume * GetComponent<Rigidbody>().velocity.magnitude / 10.0f);
    }
}

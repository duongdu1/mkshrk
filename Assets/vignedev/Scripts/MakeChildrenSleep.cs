using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeChildrenSleep : MonoBehaviour{
    void Start(){
        Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();
        foreach(Rigidbody body in bodies){
            body.Sleep();
        }
    }
}

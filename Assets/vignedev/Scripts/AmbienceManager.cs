using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
    using UnityEditor.IMGUI.Controls;

    [CustomEditor(typeof(AmbienceManager))]
    public class AmbienceManagerEditor : Editor {
        private BoxBoundsHandle[] handles;

        protected virtual void OnSceneGUI() {
            AmbienceManager helper = (AmbienceManager)target;

            // areas will be in world space
            // Handles.matrix = helper.transform.localToWorldMatrix;

            if(handles == null || handles.Length != helper.areas.Count){
                handles = new BoxBoundsHandle[helper.areas.Count];
                for(int i = 0; i < handles.Length; ++i) handles[i] = new BoxBoundsHandle();
            }

            for(int i = 0; i < handles.Length; ++i){
                handles[i].center = helper.areas[i].bounds.center;
                handles[i].size = helper.areas[i].bounds.size;
                handles[i].SetColor(helper.areas[i].color);
            }

            EditorGUI.BeginChangeCheck();
            foreach(var handle in handles) handle.DrawHandle();
            
            if(EditorGUI.EndChangeCheck()){
                Undo.RecordObjects(new Object[] { helper }, "Updated ambience areas");
                for(int i = 0; i < handles.Length; ++i){
                    helper.areas[i].bounds.center = handles[i].center;
                    helper.areas[i].bounds.size = handles[i].size;
                }
            }
        }

        override public void OnInspectorGUI(){
            DrawDefaultInspector();
            AmbienceManager helper = (AmbienceManager)target;

            GUILayout.Label($"Current Volume: {helper.volume}");
        }
    }
#endif

public class AmbienceManager : MonoBehaviour{
    [SerializeField] AudioListener listener;
    AudioManager manager;

    [System.Serializable]
    public class BoxDescription {
        public Bounds bounds;
        public float volume;
        public int index;
        public int priority;
        public Color color;
    }

    public List<AudioClip> clips = new List<AudioClip>();
    private List<AudioSource> sources = new List<AudioSource>();

    public List<BoxDescription> areas = new List<BoxDescription>();
    [Range(0.0f, 1.0f)] public float defaultVolume = 1.0f;
    public float lerpTime = 1.0f;
    public float updateInterval = 0.2f;

    private float _volume = 1.0f;
    public float volume => _volume;
    private int currentIndex = 0;

    void OnEnable() {
        manager = AudioManager.instance;
        StartCoroutine(LimitedUpdate());
    }

    void Awake(){
        if(listener == null) listener = FindObjectOfType<AudioListener>();
    }

    void Start(){
        foreach(AudioClip clip in clips){
            AudioSource source = gameObject.AddComponent<AudioSource>();
            source.playOnAwake = false;
            source.loop = true;
            source.clip = clip;
            source.outputAudioMixerGroup = manager.ambienceMixer;
            source.spatialBlend = 0.0f;
            source.volume = 0.0f;
            sources.Add(source);
        }
    }

    IEnumerator LimitedUpdate(){
        while(true){
            UpdateBasedOnLocation(listener.transform.position);
            yield return new WaitForSecondsRealtime(updateInterval);
        }
    }

    // updates the audiosources and such so it matches to what worldSpace is at
    public void UpdateBasedOnLocation(Vector3 worldSpace){
        BoxDescription area = null;
        foreach(var area_it in areas)
            if(area_it.bounds.Contains(worldSpace) && (area == null || area_it.priority > area.priority)){
                 area = area_it;
            }
        
        _volume = area?.volume ?? defaultVolume;
        currentIndex = area?.index ?? 0;
    }

    void FixedUpdate(){
        for(int i = 0; i < sources.Count; ++i){
            var source = sources[i];
            source.volume = Mathf.Lerp(source.volume, currentIndex == i ? volume : 0.0f, lerpTime * Time.fixedDeltaTime);
            if(source.volume <= 0.01f) source.Stop();
            else if(!source.isPlaying) source.Play();
        }
    }

    void OnDrawGizmosSelected(){
        for(int i = 0; i < areas.Count; ++i){
            Gizmos.color = new Color(areas[i].color.r, areas[i].color.g, areas[i].color.b, 0.6f);
            Gizmos.DrawCube(areas[i].bounds.center, areas[i].bounds.size);
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.Rendering;

[System.Obsolete]
public class ScreenFade : MonoBehaviour {
    public enum Direction {
        ZeroToOne,
        OneToZero
    }
    public bool fadeOnStart;
    public Color color;
    public Direction direction;
    public UnityEvent onFadeFinish;
    public float speed = 1.0f;

    [SerializeField] Material target;
    [SerializeField] public bool isFading = false;

    void Start(){
        if(fadeOnStart) BeginFade();
    }

    public void Reset(){
        color.a = direction == Direction.ZeroToOne ? 0 : 1;
        target.SetColor("_Color", color);
    }

    public void BeginFade(){
        if(!target){
            var loader = Addressables.LoadAssetAsync<Material>("Assets/vignedev/Shaders/BlendOverScreen.mat");
            target = loader.WaitForCompletion();
        }

        Reset();

        isFading = true;
    }

    void Update(){
        if(!isFading) return;
        
        switch(direction){
            case Direction.ZeroToOne:
                color.a += Time.deltaTime * speed;
                break;
            case Direction.OneToZero:
                color.a -= Time.deltaTime * speed;
                break;
        }

        color.a = Mathf.Clamp01(color.a);
        target.SetColor("_Color", color);

        if(color.a <= 0.0f || color.a >= 1.0f){
            onFadeFinish.Invoke();
            isFading = false;
        }
    }
}

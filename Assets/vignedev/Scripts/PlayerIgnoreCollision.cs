using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIgnoreCollision : MonoBehaviour{
    void Awake(){
        Collider[] colliders = GetComponents<Collider>();
        foreach(Collider c in colliders) Physics.IgnoreCollision(c, GameManager.instance.player.controller, true);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class InstantiateFromPointCloudBpy : MonoBehaviour{
    [SerializeField, Multiline]
    string input_data;

    [SerializeField]
    List<GameObject> objects = new List<GameObject>();

#if UNITY_EDITOR
    [EasyButtons.Button]
    void Generate(){
        string[] lines = input_data.Split("\n");
        foreach(string line in lines){
            if(line == "") continue;

            string[] values = line.Split(" ");
            Vector3 position = new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]));
            Vector3 euler    = new Vector3(float.Parse(values[3]), float.Parse(values[4]), float.Parse(values[5]));
            Vector3 scale    = new Vector3(float.Parse(values[6]), float.Parse(values[7]), float.Parse(values[8]));

            // Debug.Log($"{position} {euler} {scale}");
            // GameObject obj = Instantiate(objects[Random.Range(0, objects.Count)]);
            GameObject obj = PrefabUtility.InstantiatePrefab(objects[Random.Range(0, objects.Count)]) as GameObject;
            obj.transform.position = position;
            obj.transform.eulerAngles = euler * Mathf.Rad2Deg;
            obj.transform.localScale = scale;
        }
    }
#endif
}

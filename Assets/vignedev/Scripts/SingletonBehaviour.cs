using UnityEngine;
using System;

public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour{
    private static T _instance;
    public static T instance {
        get {
            if(_instance == null) _instance = FindObjectOfType<T>();
            if(_instance == null) {
                _instance = new GameObject(typeof(T).FullName).AddComponent<T>();
                _instance.SendMessage("Initialize", SendMessageOptions.DontRequireReceiver);
                DontDestroyOnLoad(_instance);
            }
            return _instance;
        }
    }

    private void Awake() {
        if(_instance == null){
            instance.SendMessage("Initialize", SendMessageOptions.DontRequireReceiver);
            DontDestroyOnLoad(_instance);
        }else if(_instance != null && _instance != this) {
            Debug.LogWarning($"Multiple instances of {typeof(T).FullName}. Destroying myself.", this.gameObject);
            Destroy(this);
        }
    }
}
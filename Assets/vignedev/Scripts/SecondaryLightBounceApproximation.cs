using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class SecondaryLightBounceApproximation : MonoBehaviour{
    new Light light;

    public float hitOffset = 0.0f;
    public float strengthFactor = 0.0f;
    public float normalOffset = 0.0f;
    public LayerMask mask;

    [SerializeField] Light secondaryLight;

    void Awake() {
        light = GetComponent<Light>();

        secondaryLight = new GameObject("SecondaryLight").AddComponent<Light>();
        secondaryLight.transform.SetParent(transform);
        secondaryLight.transform.localPosition = Vector3.zero;
        secondaryLight.transform.localRotation = Quaternion.identity;

        secondaryLight.type = LightType.Point;
    }

    void UpdateLight(float distance, Vector3 normal){
        secondaryLight.color = light.color;
        secondaryLight.colorTemperature = light.colorTemperature;
        secondaryLight.useColorTemperature = light.useColorTemperature;
        secondaryLight.intensity = light.intensity * (1.0f - (distance / light.range)) * strengthFactor;

        secondaryLight.transform.localPosition = Vector3.forward * distance * hitOffset + secondaryLight.transform.InverseTransformVector(normal) * normalOffset;
    }

    void Update(){
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, light.range, mask)){
            UpdateLight(hit.distance, hit.normal);
            Debug.DrawLine(hit.point, hit.point + hit.normal, Color.red, 0.1f);
        }else{
            UpdateLight(light.range, Vector3.zero);
        }
    }
}

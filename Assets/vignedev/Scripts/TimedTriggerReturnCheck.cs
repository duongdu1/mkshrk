using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimedTriggerReturnCheck : MonoBehaviour{
    public UnityEvent action;
    public List<Collider> colliders = new List<Collider>();
    
    public bool canTrigger = false;
    public bool hasTriggered = false;

    public bool secondStageFlag = false;
    private bool secondStageFlagTriggered = false;

    public float time = 1.0f;

    public void SetCanTrigger(bool state){
        canTrigger = state;
    }

    IEnumerator EnableTimer(){
        yield return new WaitForSecondsRealtime(time);
        secondStageFlag = true;
    }

    void OnTriggerStay(Collider col){
        if(canTrigger && !hasTriggered && colliders.Contains(col)){
            hasTriggered = true;
            StartCoroutine(EnableTimer());
        }

        if(hasTriggered && secondStageFlag && !secondStageFlagTriggered && colliders.Contains(col)){
            action.Invoke();
            enabled = false;
            secondStageFlagTriggered = true;
        }
    }
}

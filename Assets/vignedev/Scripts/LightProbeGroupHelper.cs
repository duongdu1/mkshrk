using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
    using UnityEditor.IMGUI.Controls;

    [CustomEditor(typeof(LightProbeGroupHelper)), CanEditMultipleObjects]
    public class LightProbeGroupHelperEditor : Editor {
        private BoxBoundsHandle[] handles;

        protected virtual void OnSceneGUI() {
            LightProbeGroupHelper helper = (LightProbeGroupHelper)target;

            Handles.matrix = helper.transform.localToWorldMatrix;

            if(handles == null || handles.Length != helper.boxes.Count){
                handles = new BoxBoundsHandle[helper.boxes.Count];
                for(int i = 0; i < handles.Length; ++i) handles[i] = new BoxBoundsHandle();
            }

            for(int i = 0; i < handles.Length; ++i){
                handles[i].center = helper.boxes[i].bounds.center;
                handles[i].size = helper.boxes[i].bounds.size;
            }

            EditorGUI.BeginChangeCheck();
            foreach(var handle in handles) handle.DrawHandle();
            
            if(EditorGUI.EndChangeCheck()){
                Undo.RecordObjects(new Object[] { helper, helper.group }, "Updated light probe boxes");
                for(int i = 0; i < handles.Length; ++i){
                    helper.boxes[i].bounds.center = handles[i].center;
                    helper.boxes[i].bounds.size = handles[i].size;
                }

                helper.RecalculateProbes();
            }
        }

        override public void OnInspectorGUI(){
            DrawDefaultInspector();
            LightProbeGroupHelper helper = (LightProbeGroupHelper)target;
            
            GUILayout.Label($"Probes: {helper.group.probePositions.Length}");
        }
    }
#endif

[RequireComponent(typeof(LightProbeGroup))]
public class LightProbeGroupHelper : MonoBehaviour{
    [System.Serializable]
    public class BoxDescription {
        public Bounds bounds;
        public Vector3Int resolution;
        public float wallOffset = 0.01f;
    }

    public List<BoxDescription> boxes = new List<BoxDescription>();

    public Color color;
    public bool drawFaces = false;

    private LightProbeGroup _group;
    public LightProbeGroup group => _group;

    void OnValidate(){
        _group = GetComponent<LightProbeGroup>();
        RecalculateProbes();
    }

    void Awake(){
        _group = GetComponent<LightProbeGroup>();

        // RecalculateProbes();
    }

    public void RecalculateProbes(){
        #if UNITY_EDITOR
        List<Vector3> probes = new List<Vector3>();
        for(int i = 0; i < boxes.Count; ++i){
            var box = boxes[i];
            if(box.resolution.x <= 0 || box.resolution.y <= 0 || box.resolution.z <= 0){
                Debug.LogError($"Box {i} has invalid resolution");
                continue;
            }

            for(int x = 0; x < box.resolution.x; ++x){
                for(int y = 0; y < box.resolution.y; ++y){
                    for(int z = 0; z < box.resolution.z; ++z){
                        Vector3 relative = new Vector3(
                            ((float)x / (float)(box.resolution.x - 1.0f)) * (box.bounds.size.x - box.wallOffset * 2.0f) + (box.wallOffset),
                            ((float)y / (float)(box.resolution.y - 1.0f)) * (box.bounds.size.y - box.wallOffset * 2.0f) + (box.wallOffset),
                            ((float)z / (float)(box.resolution.z - 1.0f)) * (box.bounds.size.z - box.wallOffset * 2.0f) + (box.wallOffset)
                        );

                        probes.Add(
                            transform.TransformPoint(
                                box.bounds.center - box.bounds.size / 2.0f + relative
                            ) - transform.position
                        );
                    }
                }
            }
        }

        group.probePositions = probes.ToArray();
        #else
            Debug.LogWarning("RecalculateProbes was called outside of editor, why is that?");
        #endif
    }

    void OnDrawGizmosSelected(){
        if(drawFaces){
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = color;
            for(int i = 0; i < boxes.Count; ++i)
                Gizmos.DrawCube(boxes[i].bounds.center, boxes[i].bounds.size);
        }
        
    }
}
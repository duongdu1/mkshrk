using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class BucketWaterLevelController : MonoBehaviour{
    [SerializeField, Range(0.0f, 1.0f)]
    private float _fillRate;

    public float fillRate {
        get { return _fillRate; }
        set { _fillRate = value; UpdateLevel(); }
    }

    [SerializeField]
    private Transform levelMesh;

    public Vector3 zeroPosition = Vector3.zero;
    public Vector3 onePosition = Vector3.zero;

    public Vector3 zeroScale = Vector3.one;
    public Vector3 oneScale = Vector3.one;

    public float zeroMass = 0.5f;
    public float oneMass = 5f;

    [SerializeField] private Rigidbody body;

    void Awake(){
        UpdateLevel();
    }

    public void UpdateLevel(){
        if(body) body.mass = Mathf.Lerp(zeroMass, oneMass, _fillRate);
        levelMesh.localPosition = Vector3.Lerp(zeroPosition, onePosition, _fillRate);
        levelMesh.localScale = Vector3.Lerp(zeroScale, oneScale, _fillRate);
    }

    [EasyButtons.Button]
    void FillBucket(){
        fillRate = 1.0f;
    }
}

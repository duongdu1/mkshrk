using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HingeJoint))]
public class WaterPump : MonoBehaviour{
    public Transform raycastOrigin;
    public LayerMask raycastMask;

    HingeJoint joint;

    public float flowRate = 0.0f;
    public float desiredFlowRate = 0.0f;
    public float flowReduction = 0.1f;

    public float pumpValue = 0.1f;
    public float pumpAngle = 5.0f;
    public float pumpDecay = 1.0f;

    public new ParticleSystem particleSystem;
    public int particleCount = 100;

    public bool debug = false;

    void Awake(){
        if(!joint) joint = GetComponent<HingeJoint>();
    }

    void Update(){
        if(debug) WorldSpaceIMGUI.Text(transform, joint.angle.ToString());
        if(desiredFlowRate >= 0) desiredFlowRate = Mathf.Clamp01(desiredFlowRate - Time.deltaTime);

        flowRate = Mathf.Lerp(flowRate, desiredFlowRate, pumpDecay * Time.deltaTime);

        if(flowRate >= Mathf.Epsilon){
            particleSystem.Emit((int)(particleCount * flowRate * Time.deltaTime));

            RaycastHit hit;
            if(Physics.Raycast(raycastOrigin.position, Vector3.down, out hit, 5f, raycastMask)){
                BucketWaterLevelController waterbucket = hit.collider.gameObject.GetComponentInParent<BucketWaterLevelController>();
                if(!waterbucket){
                    Debug.LogWarning("No water bucket component found", hit.collider.gameObject);
                    return;
                }
                waterbucket.fillRate = Mathf.Clamp01(waterbucket.fillRate + flowRate * flowReduction * Time.deltaTime);
            }
        }
    }

    float last_frame = 0.0f;
    void FixedUpdate(){
        float diff = joint.angle - last_frame;
        if(diff > pumpAngle){
            desiredFlowRate = Mathf.Clamp01(desiredFlowRate + pumpValue);
        }

        last_frame = joint.angle;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

[RequireComponent(typeof(BucketWaterLevelController))]
public class ClipWaterLevelOnAngle : MonoBehaviour{
    BucketWaterLevelController controller;
    new Rigidbody rigidbody;

    public Transform raycastSource;
    public LayerMask layerMask;
    public float radius = 0.1f;
    public AnimationCurve tippingCurve = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 0.0f);
    public float bucketInContainer = 0.1f;

    public ParticleSystem system;
    public int particleCount = 100;

    public bool debug = false;

    void Awake(){
        controller = GetComponent<BucketWaterLevelController>();
        rigidbody = GetComponent<Rigidbody>();
    }

    ParticleSystem.EmitParams emitParam = new ParticleSystem.EmitParams();
    void FixedUpdate(){
        // simplification - use the Up of object in world space to determine angle
        float rawTip = transform.up.y;
        float tipness = Mathf.Clamp01(1.0f - rawTip);

        float newMax = tippingCurve.Evaluate(tipness);
        if(debug) WorldSpaceIMGUI.Text(transform, $"rawTip={rawTip}\ntipness={tipness}\nnewMax={newMax}");
        
        // something is flowing out
        if(newMax < controller.fillRate){
            float delta = controller.fillRate - newMax;
            //  Debug.Log($"newMax={newMax} delta={delta}");
            RaycastHit hit;
            if(Physics.Raycast(raycastSource.position, Vector3.down, out hit, 20f, layerMask)){
                Debug.Log("Found container collider");
                WaterContainerFillController cc = hit.collider.GetComponentInParent<WaterContainerFillController>();
                if(cc) {
                    cc.fillRate = Mathf.Clamp01(cc.fillRate + delta * bucketInContainer);
                    Debug.Log("Found container component");
                }
            }

            emitParam.velocity = rigidbody.velocity * 2.0f;
            system.Emit(emitParam, (int)(particleCount * delta));
        }

        controller.fillRate = Mathf.Min(controller.fillRate, newMax);
    }
}

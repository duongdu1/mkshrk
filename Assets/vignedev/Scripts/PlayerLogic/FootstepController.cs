using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(PlayerController))]
public class FootstepController : MonoBehaviour{
    PlayerController player;
    [SerializeField] Transform source;
    [SerializeField, Range(0.0f, 1.0f)] float feetInterval;
    [SerializeField, Range(0.0f, 1000.0f)] float stepDistance;

    [SerializeField] float threshold;
    [SerializeField] LayerMask layerMask;
    [SerializeField] List<FootstepMaterial.MaterialAssociation> materials = new List<FootstepMaterial.MaterialAssociation>();

    Dictionary<MaterialType, AudioClipCollection> clips = new Dictionary<MaterialType, AudioClipCollection>();

    void OnValidate(){
        player = GetComponent<PlayerController>();
    }

    void Awake(){
        player = GetComponent<PlayerController>();

        foreach(var material in materials)
            clips[material.type] = material.clips;
    }

    void OnEnable(){
        StartCoroutine(FeetChecker());
    }

    void OnDrawGizmosSelected(){
        Gizmos.matrix = transform.localToWorldMatrix;

        Vector3 origin = player.transform.TransformPoint(player.controller.center) - player.transform.up * (player.controller.height / 2.0f);
        Gizmos.DrawLine(origin, origin - player.transform.up * threshold);
    }

    void PlayFootstep(Collider col){
        FootstepMaterial material = col.GetComponent<FootstepMaterial>();
        MaterialType type = material ? material.type : MaterialType.Default;

        Vector3 copy = source.transform.localPosition;
        copy.x = player.controller.center.x;
        copy.z = player.controller.center.z;
        source.transform.localPosition = copy;

        var selectedClips = clips[type];
        if(selectedClips == null) selectedClips = clips[MaterialType.Default];
        AudioManager.instance.PlayRandomClip(selectedClips.clips, source.transform.position, AudioManager.MixerOutput.Footstep);
        player.walkedDistance = 0.0f;
    }

    void RaycastFeet(){
        RaycastHit hit;
        if(
            Physics.Raycast(
                player.transform.TransformPoint(player.controller.center) - player.transform.up * (player.controller.height / 2.0f),
                -player.transform.up, out hit, threshold, layerMask, QueryTriggerInteraction.Ignore
            )
        ){
            PlayFootstep(hit.collider);
        }
    }

    IEnumerator FeetChecker(){
        while(!player.controller) yield return null;

        bool last_feet_check = player.controller.isGrounded;
        while(true){
            if(player.walkedDistance >= stepDistance) RaycastFeet();

            last_feet_check = player.controller.isGrounded;
            yield return new WaitForSeconds(feetInterval);
        }
    }
}

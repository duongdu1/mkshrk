using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class HandRigController : MonoBehaviour{
    [Serializable]
    class FingerAction {
        public InputActionReference action;

        public Transform baseSegment; // the first segment
        [NonSerialized] public Quaternion baseSegmentOriginal;
        public Vector3 baseEuler;
        [NonSerialized] public Quaternion baseRotationAxis;

        public Transform secondSegment; // the middle segment
        [NonSerialized] public Quaternion secondSegmentOriginal;
        public Vector3 secondEuler;
        [NonSerialized] public Quaternion secondRotationAxis;

        public Transform thirdSegment;  // the last segment
        [NonSerialized] public Quaternion thirdSegmentOriginal;
        public Vector3 thirdEuler;
        [NonSerialized] public Quaternion thirdRotationAxis;

        // Saves the original rotation of the finger segments
        public void SaveOriginals(){
            baseSegmentOriginal = baseSegment.localRotation;
            secondSegmentOriginal = secondSegment.localRotation;
            thirdSegmentOriginal = thirdSegment.localRotation;
        }

        // Updates the quaternions from the eulers
        public void UpdateQuaternions(){
            baseRotationAxis = baseSegmentOriginal * Quaternion.Euler(baseEuler);
            secondRotationAxis = secondSegmentOriginal * Quaternion.Euler(secondEuler);
            thirdRotationAxis = thirdSegmentOriginal * Quaternion.Euler(thirdEuler);
        }

        // Curls the finger accordingly to the Action provided values
        public void UpdateCurl() {
            float factor = action.ReadOrDefault<float>(0.0f);
            UpdateCurl(factor);
        }

        public void UpdateCurl(AnimationCurve curve){
            float factor = curve.Evaluate(action.ReadOrDefault<float>(0.0f));
            UpdateCurl(factor);
        }

        // Curls the finger accordingly to the provided value
        public void UpdateCurl(float factor){
            baseSegment.localRotation = Quaternion.Slerp(baseSegmentOriginal, baseRotationAxis, factor);
            secondSegment.localRotation = Quaternion.Slerp(secondSegmentOriginal, secondRotationAxis, factor);
            thirdSegment.localRotation = Quaternion.Slerp(thirdSegmentOriginal, thirdRotationAxis, factor);
        }

        // Draw the gizmo
        public void DrawGizmoBones(){
            if(!(baseSegment && secondSegment && thirdSegment)) return;
            Gizmos.DrawLine(baseSegment.transform.position, secondSegment.transform.position);
            Gizmos.DrawSphere(baseSegment.transform.position, 0.005f);
            Gizmos.DrawLine(secondSegment.transform.position, thirdSegment.transform.position);
            Gizmos.DrawSphere(secondSegment.transform.position, 0.005f);
            Gizmos.DrawLine(thirdSegment.transform.position, thirdSegment.transform.GetChild(0).position);
            Gizmos.DrawSphere(thirdSegment.transform.position, 0.005f);
            Gizmos.DrawSphere(thirdSegment.transform.GetChild(0).position, 0.005f);
        }
    }

    [Tooltip("Calculates the Euler into Quaternions in realtime.")]
    public bool calibrationMode = false;

    [Tooltip("Curl is based on the input reference.")]
    public bool curlUsingInputReference = true;

    [Range(0.0f, 1.0f)]
    public float curlValue = 0.0f;

    [Tooltip("Mapping function of the curlValue")]
    public AnimationCurve remappingCurve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

    [SerializeField] FingerAction thumbFinger;
    [SerializeField] FingerAction indexFinger;
    [SerializeField] FingerAction middleFinger;
    [SerializeField] FingerAction ringFinger;
    [SerializeField] FingerAction pinkieFinger;

    // Truly just for convenience and cleaner code reading
    FingerAction[] actions;

    void Start(){
        actions = new FingerAction[] {
            thumbFinger,
            indexFinger,
            middleFinger,
            ringFinger,
            pinkieFinger
        };

        foreach(FingerAction finger in actions){
            finger.SaveOriginals();
            finger.UpdateQuaternions();
        }
    }

    void FixedUpdate(){
        foreach(FingerAction finger in actions){
            if(calibrationMode) finger.UpdateQuaternions();
            if(curlUsingInputReference) finger.UpdateCurl(remappingCurve);
            else finger.UpdateCurl(remappingCurve.Evaluate(curlValue));
        }
    }

    void OnDrawGizmos(){
        thumbFinger.DrawGizmoBones();
        indexFinger.DrawGizmoBones();
        middleFinger.DrawGizmoBones();
        ringFinger.DrawGizmoBones();
        pinkieFinger.DrawGizmoBones();
    }
}
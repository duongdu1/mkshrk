using UnityEngine;

public class HandGrabGesture : MonoBehaviour {
    public virtual void OnHandGrab(HandGrabController hand) {}
}
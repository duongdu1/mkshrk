using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR.OpenXR;
using EasyButtons;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour {
    CharacterController _controller;
    public CharacterController controller => _controller;
    public float heightCalibration = 0.0f;

    Transform camOffset;
    Camera hmdCamera;
    [SerializeField] InputActionReference movementStick; // TODO: make this ambiguous to right/left (switchable)
    Vector2 desiredVector; 
    public float acceleration = 1.0f;
    public float speed = 2.0f;
    public float walkoffDistance = 1.0f;
    
    [Range(0.0f, 1.0f)] public float downwardsOffset = 0.0f;
    public AnimationCurve angleSpeedModifier = AnimationCurve.Constant(0.0f, 1.0f, 1.0f);

    // statistic just for other scripts to use if they so desire
    // can be set and get however it needs to be
    public float walkedDistance = 0.0f;

    private bool isMock = false;
    private Vector3 initialPosition;

    void OnValidate() {
        if(!hmdCamera) hmdCamera = GetComponentInChildren<Camera>();
        if(!_controller) _controller = GetComponent<CharacterController>();
    }

    void Awake() {
        if(!hmdCamera) hmdCamera = GetComponentInChildren<Camera>();
        if(!_controller) _controller = GetComponent<CharacterController>();
        _controller.Move(-transform.up * 20.0f);

        camOffset = hmdCamera.transform.parent; // TODO: is CameraOffset necessary? (TODO: how does it work??)
        initialPosition = transform.position;

#if UNITY_EDITOR_LINUX
        isMock = true;
#else
        isMock = OpenXRRuntime.name == "Unity Mock Runtime";
#endif
    }

    bool isReady = false; // it is a variable, in case there's a better way to determine ready state in the future
    void Start(){
        UpdateControllerCenter(); // update the character controller
        _controller.enabled = false;
        transform.position -= (new Vector3(_controller.center.x, 0.0f, _controller.center.z));
        _controller.enabled = isReady = true;

        heightCalibration = PlayerPrefs.GetFloat("HeightCalibration", 0.0f);
        transform.position += Vector3.up * heightCalibration;
    }

    // Process input events here, perform actual movement in FixedUpdate
    void Update() {
        // await stabilization signal
        if(!isReady) return;

        desiredVector = Vector2.ClampMagnitude(Vector2.Lerp(
            desiredVector,
            isMock ? GetMockMovementOffset() : movementStick.ReadOrDefault<Vector2>(Vector2.zero),
            acceleration * Time.deltaTime
        ), 1.0f);
    }

    Vector3 lastVelocity = Vector3.zero; 
    void FixedUpdate() {
        // await stabilization signal
        if(!isReady) return;

        // relative vector where to move the controller
        Vector3 relative = Vector3.zero;

        // if the controller is touched
        if(_controller.isGrounded && Vector2.SqrMagnitude(desiredVector) > 0.01f){
            // update the player collision model
            UpdateControllerCenter();

            // direction where the camera is looking from top-down
            Vector3 forward = hmdCamera.transform.forward;
            forward.y = 0; forward.Normalize();
            Vector3 right = hmdCamera.transform.right;
            right.y = 0; right.Normalize();
            
            // correct world-space vector of the movement
            RaycastHit currentGround; float speedMultiplier = 1.0f;
            Vector3 relativeVector = forward * desiredVector.y + right * desiredVector.x;
            if(GetGround(out currentGround)){
                Vector3 topDownVector = new Vector3(relativeVector.x, 0, relativeVector.z).normalized;
                if(Vector3.Angle(topDownVector, currentGround.normal) > 90.0f) // on slope
                    speedMultiplier = angleSpeedModifier.Evaluate(Vector3.Angle(transform.up, currentGround.normal) / controller.slopeLimit);
                else // goes downwards
                    speedMultiplier = 1.0f;
                // Debug.Log($"{Vector3.Angle(transform.up, currentGround.normal) / controller.slopeLimit}, {speedMultiplier}");
            }

            relative = relativeVector * speed * speedMultiplier;
            walkedDistance += relative.magnitude + (1.0f + relative.y * 2.0f);

            // if the move managed to get the player airborn, remember the velocity
            bool nowGrounded = _controller.SimpleMove(relative);
            if(!nowGrounded) {
                lastVelocity = relative;
                RaycastHit ground;
                if(GetGround(out ground)) controller.Move(transform.up * -ground.distance * 2.0f);
            }
        }else if(!_controller.isGrounded){
            _controller.SimpleMove(lastVelocity);
            walkedDistance = 0.0f;
            /* RaycastHit ground;
            if(GetGround(out ground)) controller.Move(transform.up * -ground.distance); */
        }else{ // isGrounded && untouchedController && check distance
            if(Vector2.Distance(
                new Vector2(hmdCamera.transform.position.x, hmdCamera.transform.position.z),
                new Vector2(transform.position.x + _controller.center.x, transform.position.z + _controller.center.z)
            ) >= walkoffDistance){ // player has walked too far, apply a SimpleMove
                UpdateControllerCenter();
                _controller.SimpleMove(Vector3.zero); // move as zero, assume that the player didn't just jump out of the window
                lastVelocity = Vector3.zero;
            }
            walkedDistance = 0.0f;
        }
    }

    // updates the CharacterController's center and height to camera
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    void UpdateControllerCenter(){
        Vector3 camera_relative_to_player = transform.InverseTransformPoint(hmdCamera.transform.position); //hmdCamera.transform.position - transform.position;

        _controller.height = camera_relative_to_player.y + heightCalibration;
        _controller.center = new Vector3(
            camera_relative_to_player.x,
            (_controller.height - heightCalibration) / 2.0f,
            camera_relative_to_player.z
        );
    }

    // vig: For debugging purposes
    [Button(Mode = ButtonMode.EnabledInPlayMode)]
    public void ResetPosition() {
        transform.position = initialPosition;
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Vector3 bottomOrigin = transform.position + controller.center - (transform.up * (controller.height / 2.0f - controller.radius));
        Gizmos.DrawWireSphere(bottomOrigin, controller.radius);
        Gizmos.DrawWireSphere(transform.position + controller.center + (transform.up * (controller.height / 2.0f - controller.radius)), controller.radius);
        
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(bottomOrigin - (transform.up * controller.radius * ((downwardsOffset * 2f))), controller.radius * (1.0f - downwardsOffset));

        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(bottomOrigin + transform.right * controller.radius  , transform.up * (controller.height - controller.radius * 2.0f));
        Gizmos.DrawRay(bottomOrigin - transform.right * controller.radius  , transform.up * (controller.height - controller.radius * 2.0f));
        Gizmos.DrawRay(bottomOrigin + transform.forward * controller.radius, transform.up * (controller.height - controller.radius * 2.0f));
        Gizmos.DrawRay(bottomOrigin - transform.forward * controller.radius, transform.up * (controller.height - controller.radius * 2.0f));

        Gizmos.color = Color.red;
        Gizmos.matrix = Matrix4x4.Translate(transform.position) * Matrix4x4.Scale(new Vector3(1.0f, 0.001f, 1.0f));
        Gizmos.DrawWireSphere(Vector3.zero, 0.5f);
        Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.1f);
        Gizmos.DrawSphere(Vector3.zero, 0.5f);
    }

    Vector2 GetMockMovementOffset(){
        if(Gamepad.current != null) return Gamepad.current.leftStick.value;

        return new Vector2(
            - Keyboard.current.aKey.value + Keyboard.current.dKey.value,
            + Keyboard.current.wKey.value - Keyboard.current.sKey.value
        );
    }

    // Returns world position of the character's center
    public Vector3 GetWorldPosition(){
        if(!_controller) _controller = GetComponent<CharacterController>(); // ensure so race conditions does not occur (gets called SOMEHOW before Awake)
        return transform.TransformPoint(controller.center);
    }

    bool GetGround(out RaycastHit hit){
        Vector3 bottomOrigin = transform.position + controller.center - (transform.up * (controller.height / 2.0f - controller.radius));
        float radius = controller.radius * (1.0f - downwardsOffset);
        return Physics.Raycast(bottomOrigin - (transform.up * controller.radius), -transform.up, out hit, downwardsOffset);
        /* if(Physics.Raycast(bottomOrigin - (transform.up * controller.radius), -transform.up, out hit, downwardsOffset)){
            Debug.Log("Snap down!");
            controller.Move(transform.up * -5.0f);
            Debug.DrawLine(bottomOrigin, hit.point, Color.magenta, 0.2f);
        }*/
    }

    public void Kill(string reason = "Suicide"){
        Scenario scenario = FindObjectOfType<Scenario>();
        if(scenario){
            scenario.EndScenario(Scenario.EndReason.Death, reason);
            return;
        }

        Debug.LogWarning("No scenario found to kill the player with, returning to main menu.");
        GameManager.instance.LoadScene("MenuBeta");
    }
}
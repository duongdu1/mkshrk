using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;
using System;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine.InputSystem.XR;

[RequireComponent(typeof(HandRigController), typeof(Rigidbody), typeof(FollowingJoint))]
public class HandGrabController : MonoBehaviour{
    private CharacterController player_controller;
    private HandRigController controller;
    private new Rigidbody rigidbody;
    private Collider[] handColliders;
    private FollowingJoint follower;
    [HideInInspector] public Kaban kaban; // TODO: move this kaban somewhere else??

    // last detected gesture and/or grabbables
    private Dictionary<HandGrabGesture, int> _gestures = new Dictionary<HandGrabGesture, int>();
    private HashSet<Grabbable> _grabbables = new HashSet<Grabbable>();
    private Grabbable _nearest;
    
    // joint & current are both not-null if they grabbed something
    private FixedJoint _joint;
    private Grabbable _current;

    // getters for the above
    public Dictionary<HandGrabGesture, int> gestures => _gestures;
    public HandGrabGesture lastGesture => _gestures.Count == 0 ? null : _gestures.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
    public HashSet<Grabbable> grabbables => _grabbables;
    public Grabbable nearest => _nearest;
    public FixedJoint joint => _joint;
    public Grabbable current => _current;

    // what determines the grab action
    [SerializeField] InputActionReference grabAction;
    [SerializeField] InputActionReference joystickAction;
    [SerializeField] InputActionReference triggerAction;
    [SerializeField] InputActionReference primaryAction;
    [SerializeField] InputActionReference secondaryAction;

    // time to let the physics simmer down
    public float physicsCooldown = 0.05f; 

    [SerializeField] public HandType handType;

    // values passed directly to ConfigurableJoint
    [Header("General Joint settings")]
    public float breakForce = 100f;
    public float breakTorque = Mathf.Infinity;
    public float massScale = 1.0f;
    public float connectedMassScale = 1.0f;
    public bool enablePreprocessing = false;

    [Header("Tunables")]
    public bool releaseOnStartup = true;
    public float massMultiplier = 1.25f;
    public Vector3 detectionOffset;
    public float distanceThreshold = 0.01f;

#region Internal or Editor related
    [RuntimeInitializeOnLoadMethod]
    void ClearEvents(){
        grabAction.action.started -= AttemptGrab;
        grabAction.action.canceled -= ReleaseGrab;
        SceneManager.sceneUnloaded -= OnSceneUnload;
    }
    void OnSceneUnload(Scene scene){ ClearEvents(); }

    void Awake(){
        player_controller = GetComponentInParent<CharacterController>();
        controller = GetComponent<HandRigController>();
        rigidbody = GetComponent<Rigidbody>();
        follower = GetComponent<FollowingJoint>();
        handColliders = GetComponentsInChildren<Collider>(false)
            .Where(x => x.gameObject != this.gameObject) // because of our grab trigger
            .ToArray();
    }

    void OnDrawGizmosSelected(){
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube(detectionOffset, Vector3.one * 0.01f);
    }
#endregion

#region Main Grabbing Logic
    /// <summary>Attempts to grab the Grabbable</summary>
    /// <param name="context">Grabbable in context</param>
    /// <returns>If grab was successful. If `false`, assume no changes occured.</returns>
    public bool AttemptGrab(Grabbable context){
        if(_joint || _current) return false; // what? how come we have a joint here? (ala releasegrab wasnt called)
        // if(grabbables.Count == 0) return false;   // nothing to grab on
        if(context.isStashed){
            _grabbables.Remove(context);
            return false;
        }
        
        controller.curlUsingInputReference = false;
        controller.curlValue = 1.0f;

        _current = context;
        _current.SetOutline(false);

        _joint = gameObject.AddComponent<FixedJoint>();
        _joint.breakForce = breakForce;
        _joint.breakTorque = breakTorque;
        _joint.connectedBody = _current.rigidbody;
        _joint.massScale = massScale;
        _joint.connectedMassScale = connectedMassScale;
        _joint.enableCollision = false;
        _joint.enablePreprocessing = enablePreprocessing;

        // call handgrab on it -> handles handcount and parenting
        _current._OnHandGrab(this, player_controller.transform);
        
        follower.angularMassScale = _current.torqueScale;
        follower.UpdateJoint();

        // make its colliders ignore the hand for now
        SetHandCollision(_current.colliders, false);

        return true;
    }

    /// <summary>Releases the current grabbable</summary>
    public void ReleaseGrab(){
        controller.curlUsingInputReference = true;

        // if release was called with only a single ConfigurableJoint left, then it is no longer held by any hands
        if(_current){
            // reparents and calls back what is necessary
            _current._OnHandRelease(this);

            // remove ignore flag for player only if we are the sole fixed joint
            SetHandCollision(_current.colliders, true, _current.handCount == 0);

            // disable hand collision for a bit for intuitive throwing
            StartCoroutine(TemporaryDisableCollision(_current.colliders));
            if(_joint) DestroyImmediate(_joint);

            if(kaban){ // was in a bag => inserted into the bag
                _grabbables.Remove(_current);
            }
        }

        follower.angularMassScale = 1.0f;
        follower.UpdateJoint();

        _current = null;
        _joint = null;
    }
    
    /// <summary>From the list of Grabbables select the closest</summary>
    /// <returns>Closest Grabbable to the hand</returns>
    Grabbable FindNearestGrabbable(){
        if(_grabbables.Count == 0) return null;

        try{
            return _grabbables
                .OrderBy(x => Vector3.Magnitude(x.rigidbody.ClosestPointOnBounds(transform.position + transform.InverseTransformPoint(detectionOffset))))
                .FirstOrDefault();
        }catch(Exception){
            return null;
        }
    }
#endregion

#region Unity Triggers
    UnityEngine.InputSystem.XR.Haptics.SendHapticImpulseCommand vibration = UnityEngine.InputSystem.XR.Haptics.SendHapticImpulseCommand.Create(0, 0.5f, 0.05f);
    void OnTriggerEnter(Collider trigger) {
        if(trigger.isTrigger) {
            HandGrabGesture gesture = trigger.GetComponentInParent<HandGrabGesture>();
            if(gesture){
                if(!_gestures.ContainsKey(gesture)) {
                    (handType == HandType.Left ? XRController.leftHand : XRController.rightHand)?.ExecuteCommand(ref vibration);
                    _gestures[gesture] = 0;
                }
                ++_gestures[gesture];
            }
        }

        Grabbable actual = trigger.GetComponentInParent<Grabbable>();
        if(actual && actual.allowGrab) _grabbables.Add(actual);
    }

    void OnTriggerExit(Collider trigger){
        if(trigger.isTrigger) {
            HandGrabGesture gesture = trigger.GetComponentInParent<HandGrabGesture>();
            if(gesture)
                if(_gestures.ContainsKey(gesture) && --_gestures[gesture] == 0)
                    _gestures.Remove(gesture);
        }

        Grabbable actual = trigger.GetComponentInParent<Grabbable>();
        if(actual) _grabbables.Remove(actual);
    }

    // workaround to have the hashset in proper state
    public void ForgetGrabbable(Grabbable grabbable){
        _grabbables.Remove(grabbable);
    }
    public void ForgetGesture(HandGrabGesture gesture){
        _gestures.Remove(gesture);
    }
#endregion 

#region Timed Enumerators
    IEnumerator TemporaryDisableCollision(Collider[] colliders){
        if(physicsCooldown == 0.0f) yield break;

        SetHandCollision(colliders, false);
        yield return new WaitForSeconds(physicsCooldown);
        SetHandCollision(colliders, true);

        yield return null;
    }

    IEnumerator PeriodicalGrabChecker(){
        while(true){
            if(!_current){
                Grabbable newNearest = FindNearestGrabbable();
                if(_nearest) _nearest.SetOutline(false);
                if(newNearest) newNearest.SetOutline(true);
                
                _nearest = newNearest;
            }
            yield return new WaitForSecondsRealtime(0.1f);
        }
    }
#endregion

#region Hand Collision Functions
    void SetHandCollision(IEnumerable<Collider> colliders, bool enable, bool player_as_well = true){
        foreach(Collider target in colliders){
            foreach(Collider collider in handColliders)
                Physics.IgnoreCollision(target, collider, !enable);
            if(player_as_well)
                Physics.IgnoreCollision(player_controller, target, !enable);
        }
    }
#endregion

#region Input Callbacks
    void AttemptGrab(InputAction.CallbackContext context){
        // if the hand is located inside a trigger that has the HandGrabGesture.cs
        if(lastGesture){
            lastGesture.OnHandGrab(this);
        }else{
            if(kaban) kaban.RetrieveItem();
            else if(_nearest) AttemptGrab(_nearest);
        }
    }

    void ReleaseGrab(InputAction.CallbackContext context){
        ReleaseGrab();
    }
#endregion

#region Exposing additional inputs
    // These getters are to be used in Grabbables, as they can access the hands and thus could
    // read these inputs for further customization
    public Vector2 joystick { get { return joystickAction.ReadOrDefault<Vector2>(Vector2.zero); } }
    public float trigger { get { return triggerAction.ReadOrDefault<float>(0.0f); } } 
    public bool primary { get { return primaryAction.ReadOrDefault<float>(0.0f) > 0.5f; }}
    public bool secondary { get { return secondaryAction.ReadOrDefault<float>(0.0f) > 0.5f; }}
#endregion

    void Start(){
        if(releaseOnStartup) transform.SetParent(null, true);
    }

    void OnEnable() {
        grabAction.action.started += AttemptGrab;
        grabAction.action.canceled += ReleaseGrab;
        SceneManager.sceneUnloaded += OnSceneUnload;

        StartCoroutine(PeriodicalGrabChecker());
    }

    void OnDisable() {
        grabAction.action.started -= AttemptGrab;
        grabAction.action.canceled -= ReleaseGrab;
        SceneManager.sceneUnloaded -= OnSceneUnload;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(HandGrabController)), CanEditMultipleObjects]
class HandGrabControllerEditor : Editor {
    override public void OnInspectorGUI(){
		DrawDefaultInspector();
		HandGrabController grab = (HandGrabController)target;

        GUILayout.Label("Grabbables:");
        foreach(Grabbable g in grab.grabbables)
            GUILayout.Label($"{g.gameObject.name}");

        GUILayout.Label("Gestures:");
        foreach(var kp in grab.gestures)
            GUILayout.Label($"{kp.Key.gameObject.name}: {kp.Value}");
        // GUILayout.Label($"{grab.colliders.Length} colliders, {grab.renderers.Length} renderers");
	}
}
#endif
# `mkshrk`

Bachelor thesis of Duong Duc Hoang

It is possible to download a compiled version over the release tab for Windows x64, found [here](https://gitlab.fit.cvut.cz/duongdu1/mkshrk/-/releases). For this game, a OpenXR compatible headset and runtime has to be installed on the system.

The thesis documentation can be found [here](https://gitlab.fit.cvut.cz/duongdu1/mkshrk/-/blob/master/Thesis/ctufit-thesis.pdf).

